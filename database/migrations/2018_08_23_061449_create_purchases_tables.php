<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases_header', function (Blueprint $table) {
            $table->increments('id');
            $table->date('purchase_date');
            $table->string('total_amount')->default(0);
            $table->string('invoice_number');
            $table->string('stamping'); // Timbrado
            $table->integer('user_id');
            $table->boolean('canceled')->default('false');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('purchases_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_header_id');
            $table->integer('product_id');
            $table->string('quantity');
            $table->string('single_amount');
            $table->string('total_amount');
            $table->timestamps();

            $table->foreign('purchase_header_id')->references('id')->on('purchases_header');

            $table->engine = 'InnoDB';

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases_details', function(Blueprint $table){
            $table->dropForeign(['purchases_header_id', 'product_id']);
        });

        Schema::drop('purchases_header');

    }
}
