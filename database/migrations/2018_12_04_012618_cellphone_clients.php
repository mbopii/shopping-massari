<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CellphoneClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_clients', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->default(1);
            $table->string('description');
            $table->string('idnum');
            $table->string('telephone');
            $table->string('birthday')->nullable();
            $table->string('tax_name');
            $table->string('tax_code');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('addresses', function(Blueprint $table){
            $table->integer('client_id')->nullable();
        });

        Schema::table('orders_header', function(Blueprint $table){
            $table->integer('client_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('call_clients', function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });

        Schema::table('addresses', function(Blueprint $table){
            $table->dropColumn(['client_id']);
        });

        Schema::table('orders_header', function(Blueprint $table){
            $table->dropColumn(['client_id']);
        });

    }
}
