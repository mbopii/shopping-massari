<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function(Blueprint $table){
            $table->increments('id');
            $table->string("description");
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
        Schema::create('payments_method', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('wish_list', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->date('wish_list_date');
            $table->integer('product_id');
            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('orders_header', function(Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("address_id");
            $table->integer("status_id");
            $table->integer("payment_method_id");
            $table->date('purchase_date');
            $table->string('order_amount');
            $table->boolean('discount')->default('false');
            $table->string('discount_amount')->default(0);
            $table->string('pagando_id')->nullable();
            $table->integer("sell_channel")->defualt(1);
            $table->integer("delivery_cost");
            $table->boolean('aex_payment')->default('false');
            $table->string('aex_request_id')->nullable();
            $table->string('aex_service_type')->nullable();
            $table->string("aex_tracking_id")->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on("users");
            $table->foreign('address_id')->references('id')->on("addresses");
            $table->foreign('status_id')->references('id')->on("statuses");
            $table->foreign('payment_method_id')->references('id')->on('payments_method');

            $table->engine = "InnoDB";
        });

        Schema::create('orders_details', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_header_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('unit_price');
            $table->boolean('discount')->default('false');
            $table->string('total_discount')->default(0);
            $table->string('sub_total');


            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('order_header_id')->references('id')->on('orders_header');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create("complex_products_details", function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('involve_product_id');
            $table->integer("quantity")->default(1);

            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders_details', function(Blueprint $table){
            $table->dropForeign(['product_id', 'order_header_id']);
        });
        Schema::drop('orders_header', function(Blueprint $table){
            $table->dropForeign(['user_id', 'address_id', 'status_id', 'payment_method_id']);
        });
        Schema::drop('payments_method');
        Schema::drop('wish_list', function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });
        Schema::drop('statuses');
        Schema::drop('complex_products_details');

    }
}
