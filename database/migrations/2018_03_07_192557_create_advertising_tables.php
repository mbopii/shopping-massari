<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_sections', function (Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->string('code');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('advertising', function (Blueprint $table){
            $table->increments('id');
            $table->integer('section_id');
            $table->string('tittle');
            $table->string('description')->nullable();
            $table->string('url')->nullable();
            $table->boolean('active')->default('false');
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('advertising_sections');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advertising', function (Blueprint $table){
            $table->dropForeign('section_id');
        });
        Schema::drop('advertising_sections');
    }
}
