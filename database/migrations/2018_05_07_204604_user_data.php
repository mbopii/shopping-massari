<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('departments', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('cities', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->integer('department_id');
            $table->string('aex_code')->nullable();
            $table->string('latlong')->nullable();
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('departments');

            $table->engine = 'InnoDB';
        });

        Schema::create('addresses', function(Blueprint $table){
            $table->increments('id');
            $table->string('address');
            $table->string("address2");
            $table->string('telephone')->nullable();
            $table->string('references')->nullable();
            $table->integer('city_id');
            $table->string('number');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('user_id')->references('id')->on('users');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses', function(Blueprint $table){
            $table->dropForeign('city_id');
        });
        Schema::drop('cities', function(Blueprint $table){
            $table->dropFroeign(['department_id']);
        });
        Schema::drop('departments');
    }
}
