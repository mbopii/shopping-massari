<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsCategoriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();


            $table->engine = 'InnoDB';
        });

        Schema::create('sub_categories', function (Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->integer('category_id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();


            $table->foreign('category_id')->references('id')->on('categories');
            $table->engine = 'InnoDB';
        });

        Schema::create('brands', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->engine = 'InnoDB';
        });

        Schema::create('products', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('price');
            $table->boolean('featured')->default('false');
            $table->integer('category_id');
            $table->integer('subcategory_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('quantity')->default(0);
            $table->boolean('discount')->default('false');
            $table->string('discount_percentage')->default('0');
            $table->string("discount_quantity")->default('0');
            $table->string('discount_percentage_2')->default('0');
            $table->string("discount_quantity_2")->default('0');
            $table->boolean('complex_product')->default('false');
            $table->integer('min_quantity')->default(5);
            $table->decimal("weight")->default(1);
            $table->integer("height")->default(10);
            $table->integer("width")->default(10);
            $table->integer("length")->default(10);
            $table->integer('views')->default(0);
            $table->boolean('active')->default(true);
            $table->timestamp('deleted_at');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('subcategory_id')->references('id')->on('sub_categories');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products', function(Blueprint $table){
            $table->dropForeign(['category_id', 'sub_category_id', 'brand_id']);
        });
        Schema::drop('sub_categories', function(Blueprint $table){
            $table->dropForeign(['category_id']);
        });
        Schema::drop('categories');
        Schema::drop('brands');
    }
}
