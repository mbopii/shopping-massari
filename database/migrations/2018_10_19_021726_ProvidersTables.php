<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvidersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('tax_name');
            $table->string('tax_code');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax_number')->nullable();
            $table->timestamp('deleted_at')->nullable();

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::table('purchases_header', function(Blueprint $table){
            $table->integer('provider_id');

            $table->foreign('provider_id')->references('id')->on('providers');
        });

        Schema::table('products', function(Blueprint $table){
            $table->string('bar_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases_header', function(Blueprint $table){
            $table->dropForeign(['provider_id']);
            $table->dropColumn(['provider_id']);
        });

        Schema::table('products', function(Blueprint $table){
            $table->dropColumn(['bar_code']);
        });

        Schema::drop("providers");
    }
}
