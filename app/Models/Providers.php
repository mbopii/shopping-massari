<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Providers extends Model
{

    use SoftDeletes;

    protected $table = 'providers';

    protected $fillable = ['description', 'tax_name', 'tax_code', 'email', 'address',
        'telephone', 'fax_number'];

}