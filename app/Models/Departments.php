<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{

    protected $table = 'departments';

    protected $fillable = ['description'];

    public function cities()
    {
        return $this->hasMany('App\Models\Cities', 'department_id');
    }
}