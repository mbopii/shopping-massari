<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    protected $fillable = ['description'];
    public $timestamps = ['deleted_at'];

    public function subcategories()
    {
        return $this->hasMany('App\Models\SubCategories', 'category_id');
    }

    public function Products()
    {
        return $this->hasMany('App\Models\Products');
    }

    public function productsCount()
    {
        return Products::where('category_id', $this->id)->count('*');
    }


}
