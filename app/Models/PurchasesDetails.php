<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchasesDetails extends Model
{

    protected $table = 'purchases_details';

    protected $fillable = ['purchase_header_id', 'product_id', 'quantity', 'single_amount', 'total_amount'];

    public function purchaseHeader()
    {
        return $this->belongsTo('App\Models\PurchasesHeader', 'purchase_header_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }



}