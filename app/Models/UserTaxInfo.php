<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTaxInfo extends Model
{
    use SoftDeletes;

    protected $table = 'user_tax_info';

    protected $fillable = ['user_id', 'tax_name', 'tax_code'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
