<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallClients extends Model
{
    use SoftDeletes;

    protected $table = 'call_clients';

    protected $fillable = ['user_id', 'description', 'idnum', 'telephone', 'birthday', 'tax_name', 'tax_code'];

    public $timestamps = ['deleted_at'];


    public function addresses()
    {
        return $this->hasMany('App\Models\Addresses', 'client_id');
    }

    public function purchases()
    {
        return $this->hasMany('App\Models\OrdersHeaders', 'client_id');
    }


}