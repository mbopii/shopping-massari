<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersHeaders extends Model
{
    protected $table = 'orders_header';

    protected $fillable = ['user_id', 'address_id', 'status_id', 'payment_method_id', 'purchase_date', 'order_amount',
        'discount', 'discount_amount', 'pagando_id', 'sell_channel', 'aex_request_id', 'aex_service_type',
        'aex_tracking_id', 'aex_collect', 'aex_collect_amount', 'delivery_cost', 'aex_payment', 'reception_time_from',
        'reception_time_to', 'client_id', 'reception_date'];

    public function details()
    {
        return $this->hasMany('App\Models\OrdersDetails', 'order_header_id');
    }

    public function paymentForm()
    {
        return $this->belongsTo('App\Models\PaymentForms', 'payment_method_id');
    }

    public function clients()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }

    public function statuses()
    {
        return $this->belongsTo('App\Models\Statuses', 'status_id');
    }

    public function addresses()
    {
        return $this->belongsTo('App\Models\Addresses', 'address_id')->withTrashed();
    }

    public function noClient()
    {
        return $this->belongsTo('App\Models\CallClients', 'client_id')->withTrashed();
    }
}