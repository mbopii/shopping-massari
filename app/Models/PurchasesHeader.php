<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchasesHeader extends Model
{

    protected $table = 'purchases_header';

    protected $fillable = ['purchase_date', 'total_amount', 'invoice_number', 'stamping', 'user_id', 'canceled', 'provider_id'];


    public function purchaseDetails()
    {
        return $this->hasMany('App\Models\PurchasesDetails', 'purchase_header_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function providers()
    {
        return $this->belongsTo('App\Models\Providers', 'provider_id');
    }

}