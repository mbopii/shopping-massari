<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategories extends Model
{
    use SoftDeletes;

    protected $table = 'sub_categories';
    public $timestamps = ['deleted_at'];

    protected $fillable = ['description', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Categories', 'category_id');
    }
}
