<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends Model
{
    use SoftDeletes;

    protected $table = 'addresses';

    protected $fillable = ['address', 'address2', 'telephone', 'references', 'city_id', 'number',
        'latitude', 'longitude', 'user_id', 'client_id'];

    public $timestamps = ['deleted_at'];

    public function cities()
    {
        return $this->belongsTo('App\Models\Cities', 'city_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }


}