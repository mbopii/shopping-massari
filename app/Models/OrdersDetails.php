<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersDetails extends Model
{
    protected $table = 'orders_details';

    protected $fillable = ['order_header_id', 'product_id', 'quantity', 'unit_price', 'sub_total', 'discount', 'total_discount'];

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id')->withTrashed();
    }
}