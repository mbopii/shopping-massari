<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    public $timestamps = ['deleted_at'];
    protected $fillable = ['category_id','name','price','description','subcategory_id', 'featured', 'brand_id',
        'quantity', 'discount', 'discount_percentage', 'discount_quantity', 'discount_percentage2', 'discount_quantity2', 'min_quantity', 'complex_product',
        'weight', 'height', 'width', 'length', 'views', 'bar_code', 'active'];

    public function categories()
    {
        return $this->belongsTo('App\Models\Categories','category_id');
    }

    public function subCategories()
    {
        return $this->belongsTo('App\Models\SubCategories','subcategory_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brands', 'brand_id');
    }

    public function onWishList($user_id)
    {
        if (WishList::where('user_id', $user_id)->where('product_id', $this->id)->first()){
            return true;
        }else{
            return false;
        }
    }

    public function complexItems()
    {
        return $this->hasMany('App\Models\ComplexProductsDetails', 'product_id');
    }

    public function validateProduct()
    {
        if ($this::where('deleted_at', '!=', null)->get()){
            return true;
        }else{
            return false;
        }
    }
}
