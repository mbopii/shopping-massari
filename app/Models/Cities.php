<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{

    protected $table = 'cities';

    protected $fillable = ['description', 'department_id', 'aex_code', 'latlong'];

    public function myDepartment()
    {
        return $this->belongsTo('App\Models\Departments', 'department_id');
    }
}