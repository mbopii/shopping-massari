<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

//use Illuminate\Http\Request;

class OrdersRequest extends Request
{
    /**
     * Determine if the user is authorize to make this request
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to this request
     * @return array
     */
    public function rules()
    {
        return [
            'address_id'   => 'required',
            'payment_method_id'      => 'required',
        ];
    }

    /**
     * Set custom messages for validator errors
     * @return array
     */
    public function messages()
    {
        return [
            'payment_method_id.required' => 'Debe elegir una forma de pago.',
            'address_id.required' => 'El campo dirección es obligatorio.',
        ];
    }
}