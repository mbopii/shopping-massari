<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required',
            'bar_code' => 'required',
            'product_image_1' => 'max:5000',
            'product_image_2' => 'max:5000',
            'product_image_3' => 'max:5000'

        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Debe agregar un nombre al producto',
            'price.required' => 'El producto debe tener un precio',
            'price.numeric' => 'El precio debe ser un valor numerico',
            'category_id.required' => 'Debe elegir una categoria',
            'bar_code.required' => 'Debe agregar un código de barras a su producto',
            'product_image_1.max' => 'El tamano maximo de imagen es de 5MB',
            'product_image_2.max' => 'El tamano maximo de imagen es de 5MB',
            'product_image_3.max' => 'El tamano maximo de imagen es de 5MB'

        ];
    }

}
