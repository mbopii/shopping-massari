<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

//use Illuminate\Http\Request;

class AdvertisingRequest extends Request
{
    /**
     * Determine if the user is authorize to make this request
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to this request
     * @return array
     */
    public function rules()
    {
        return [
            'tittle'   => 'required',
            'url'      => 'required|url',
            'section_id' => 'required',
        ];
    }

    /**
     * Set custom messages for validator errors
     * @return array
     */
    public function messages()
    {
        return [
            'tittle.required' => 'Debe asignar un titulo al banner.',
            'url.required' => 'Debe agregar una url para redireccionar al hacer click en el anuncio.',
            'url.url'   => "El formato de la url es invalido, debe tener este formato: 'http://test.com/prueba'",
            'section_id.required' => 'Debe elegir una seccion para el banner',
        ];
    }
}