<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

//use Illuminate\Http\Request;

class CategoriesRequest extends Request
{
    /**
     * Determine if the user is authorize to make this request
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to this request
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => 'required',
//            'category_id'      => 'required',
        ];
    }

    /**
     * Set custom messages for validator errors
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe escribir una descripcion para la subcategoria.',
//            'category_id.required' => 'Debe asignar una categoria'
        ];
    }
}