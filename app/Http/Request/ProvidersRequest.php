<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProvidersRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'tax_name' => 'required',
            'tax_code' => 'required',
            'email' => 'nullable|email'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe agregar un nombre al proveedor',
            'tax_name.required' => 'Debe agregar una razon social al proveedor',
            'tax_code.required' => 'Debe agregar un RUC al proveedor',
            'email.email' => 'Debe ingresar un formato válido para el correo electronico'
        ];
    }

}
