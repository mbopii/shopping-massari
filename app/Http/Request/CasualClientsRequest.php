<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

//use Illuminate\Http\Request;

class CasualClientsRequest extends Request
{
    /**
     * Determine if the user is authorize to make this request
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to this request
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => 'required',
            'idnum'      => 'required',
            'birthday'  => 'required|date',
            'address' => 'required'
        ];
    }

    /**
     * Set custom messages for validator errors
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe escribir una descripcion para la subcategoria.',
            'idnum.required' => 'Debe agergar un numero de cedula',
            'birthday.required' => 'Debe agergar una fecha de nacimiento',
            'birthday.date' => 'La fecha de nacimiento debe contener un formato de fecha valido',
            'address.required' => 'Debe agregar una dirección'
        ];
    }
}