<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use App\Models\WishList;

class WishListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Categories::all();
        $user = \Sentinel::getUser()->id;
        $wishListHeader = WishList::where('user_id', $user)->get();

        return view('front.store.wish', compact('wishListHeader', 'categories'));
    }

    public function add($product_id)
    {
        // Create a new WishList row

        if ($product = Products::find($product_id)) {
            if (!$wishPreview = WishList::where('user_id', \Sentinel::getUser()->id)
                ->where('product_id', $product_id)->first()) {
                $arrayToSave = [
                    'user_id' => \Sentinel::getUser()->id,
                    'product_id' => $product_id,
                    'wish_list_date' => date('Y-m-d')
                ];

                if ($wish = WishList::create($arrayToSave)) {
                    \Log::debug("WishListController | Product added");
                    return redirect()->back()->with('success', 'Producto agregado a la lista');
                } else {
                    \Log::warning("WishListController | Error creating a new wish list");
                    return redirect()->back()->with('error', 'Error al intentar crear una nueva lista');
                }
            } else {
                \Log::warning("WishListController | Duplicate product on list");
                return redirect()->back()->with('error', 'El producto ya existe en la lista');
            }
        } else {
            \Log::warning("WishListController | Product not found");
            return redirect()->back()->with('error', 'Producto no encontrado');
        }
    }


    public function remove($id)
    {
        if ($wishPreview = WishList::find($id)) {

            if (WishList::destroy($id)) {
                \Log::debug("WishListController | Product removed");
                return redirect()->back()->with('success', 'Producto Eliminado de la lista');
            } else {
                \Log::warning("WishListController | Error destroying a product");
                return redirect()->back()->with('error', 'Error al intentar eliminar producto');
            }
        } else {
            \Log::warning("WishListController | List not found");
            return redirect()->back()->with('error', 'Producto no encontado');
        }
    }

}