<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrdersRequest;
use App\Models\Addresses;
use App\Models\Categories;
use App\Models\OrdersDetails;
use App\Models\OrdersHeaders;
use App\Models\Products;
use GuzzleHttp\Client;

class OrdersController extends Controller
{
    public $pagando_public_key;
    public $pagando_private_key;

    public function __construct()
    {
        $this->middleware('auth');
        // Desarrollo
        //        $this->pagando_public_key = '6520dc4f48401f07235c0d9f2bdf7fcf';
        //       $this->pagando_private_key = 'a05e2e328696085351d6d67f2a71d500';

        // Produccion
        $this->pagando_public_key = '09e795b4e9414fe425dc14662c31f99e';
        $this->pagando_private_key = '1a9c44311c72bb5dd8b31e6ea4c8f0de';
    }

    public function store(OrdersRequest $request)
    {
        $input = $request->all();
        $cartContent = \Cart::content();
        $totalAmount = \Cart::subtotal(0, '', '');
        $user = \Sentinel::getUser();
        if ($input['address_id'] == 0) {
            return redirect()->back()->with('error', 'Debe elegir una dirección');
        }

        $orderHeaderArray = [
            'user_id' => $user->id,
            'address_id' => $input['address_id'],
            'status_id' => '1', // En proceso,
            'payment_method_id' => $input['payment_method_id'],
            'purchase_date' => date('Y-m-d'),
            "delivery_cost" => $input['hidden_delivery_cost'],
            'reception_time_from' => $input['reception_time_from'],
            'reception_time_to' => $input['reception_time_to'],
        ];
        $orderHeaderArray['discount'] = false;
        if (array_key_exists('flag', $input)) {
            $orderHeaderArray['order_amount'] = $totalAmount;
            $orderHeaderArray['discount'] = true;
            $orderHeaderArray['discount_amount'] = $totalAmount - $input['subtotal_discount'];
        } else {
            $orderHeaderArray['order_amount'] = $totalAmount;
        }

        if ($orderHeader = OrdersHeaders::create($orderHeaderArray)) {
            $package = [];
            foreach ($cartContent as $content) {
                $product = Products::find($content->id);
                $detailsArray = [
                    'order_header_id' => $orderHeader->id,
                    'product_id' => $content->id,
                    'unit_price' => $product->price,
                    'quantity' => $content->qty,
                    'sub_total' => ($content->qty * $content->price),
                ];
                if ($content->qty >= $content->model->discount_quantity && $content->qty < $content->model->discount_quantity2 && $content->model->discount_quantity != 0) {
                    $discount = $content->price * $content->qty * $content->model->discount_percentage / 100;
                    $detailsArray['discount'] = true;
                    $detailsArray['total_discount'] = $discount;
                } elseif ($content->qty >= $content->model->discount_quantity2 && $content->model->discount_quantity != 0) {
                    $discount = $content->price * $content->qty * $content->model->discount_percentage2 / 100;
                    $detailsArray['discount'] = true;
                    $detailsArray['total_discount'] = $discount;
                } else {
                    $detailsArray['discount'] = false;
                }

                OrdersDetails::create($detailsArray);
                if ($content->qty > 1) {
                    for ($i = 1; $i <= $content->qty; $i++) {
                        $package[] = [
                            "peso" => $product->weight, // KG
                            "largo" => $product->height, // CM
                            "alto" => $product->width, // CM
                            "ancho" => $product->length, // CM,
                            "valor" => $product->price,
                        ];
                    }
                } else {
                    $package[] = [
                        "peso" => $product->weight, // KG
                        "largo" => $product->height, // CM
                        "alto" => $product->width, // CM
                        "ancho" => $product->length, // CM,
                        "valor" => $product->price,
                    ];
                }
                \Log::warning("Package: " . json_encode($package));
            }

            // Si existe costo de delivery, solicitar servicio
            $address = Addresses::find($input['address_id']);
            if ($input['hidden_delivery_cost'] > 0 && $address->cities->aex_code) {
                $aexURL = env('AEX_SERVICE');
                $aexRequestArray = [
                    "clave_publica" => env('AEX_PUBLIC_KEY'),
                    "codigo_sesion" => 123,
                    "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
                ];

                try {
                    // Generamos Código Token para autenticación
                    $client = new Client(['base_uri' => $aexURL]);
                    $response = $client->request('POST', 'autorizacion-acceso/generar',
                        ['json' => $aexRequestArray, 'verify' => false]);
                    $aexAuthRequestJson = $response->getBody()->getContents();

                    $aexAuthRequest = json_decode($aexAuthRequestJson);
                    \Log::info("OrdersController | {$aexAuthRequestJson}");

                    if ($aexAuthRequest->codigo == 0) {
                        // Realizamos la solicitud
                        $aexGetServiceRequestArray = [
                            "clave_publica" => env("AEX_PUBLIC_KEY"),
                            "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                            "origen" => "PY1107",
                            "destino" => $address->cities->aex_code,
                            "codigo_operacion" => $orderHeader->id,
                            "paquetes" => $package,
                            "codigo_tipo_carga" => "P",
                        ];
                        \Log::warning("StoreController | Calculator request -> " . json_encode($aexGetServiceRequestArray));
                        // Solicitamos Servicio a AEX
                        $response = $client->request('POST', 'envios/solicitar_servicio', ['json' => $aexGetServiceRequestArray, 'verify' => false]);
                        $aexGetServiceResponseJson = $response->getBody()->getContents();
                        $aexGetServiceResponse = json_decode($aexGetServiceResponseJson);
                        \Log::info("OrdersController | {$aexGetServiceResponseJson}");
                        if ($aexGetServiceResponse->codigo == 0) {
                            // Guardamos los datos importantes para confirmar dependiendo del método de pago
                            $conditions = $aexGetServiceResponse->datos->condiciones;
                            foreach ($conditions as $condition) {
                                // Envio estandar puerta a puerta
                                if ($condition->id_tipo_servicio == 3) {
                                    \Log::info("OrdersController | Store AEX Data");
                                    $delivery = explode('.', $condition->costo_flete);
                                    $orderHeader->delivery_cost = $delivery[0];
                                    $orderHeader->aex_request_id = $aexGetServiceResponse->datos->id_solicitud;
                                    $orderHeader->aex_service_type = 3;
                                    // Pago en efectivo, solicitamos cobro del mismo
                                    if ($orderHeader->payment_method_id == 1) {
                                        foreach ($condition->adicionales as $service) {
                                            if ($service->id_adicional == 3) {
                                                $cobro = explode('.', $service->costo);
                                                $orderHeader->aex_collect_amount = $cobro[0];
                                                break;
                                            }
                                        }
                                    }
                                    $orderHeader->save();
                                    break;
                                }
                            }
                            // Confirmamos el pago solamente si es efectivo en esta funciono
                            if ($orderHeader->payment_method_id == 1) {
                                $aexConfirmServiceArray = [
                                    "clave_publica" => env("AEX_PUBLIC_KEY"),
                                    "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                                    "id_solicitud" => $aexGetServiceResponse->datos->id_solicitud,
                                    'id_tipo_servicio' => 3,
                                    'codigo_forma_pago' => 'D',
                                    // Direccion de retiro de mercaderías,
                                    "pickup" => [
                                        "codigo" => "2", // COdigo direccion
                                        "calle_principal" => "Valois Rivarola",
                                        "numero_casa" => '951',
                                        "calle_transversal_1" => "Tte. Francisco Cusmanich",
                                        "codigo_ciudad" => "PY0000",
                                        "telefono_movil" => '0972162339',
                                        'latitud' => '-25.279472',
                                        'longitud' => '-57.610626',
                                        'referencias' => '',
                                    ],
                                    // Direccion de entrega de mercaderías
                                    "entrega" => [
                                        "codigo" => $address->id, // COdigo direccion
                                        "calle_principal" => $address->address,
                                        "numero_casa" => $address->number,
                                        "calle_transversal_1" => $address->address2,
                                        "codigo_ciudad" => $orderHeader->addresses->cities->aex_code,
                                        "referencias" => $address->references,
                                        "disponible_desde" => $orderHeader->reception_time_from,
                                        "disponible_hasta" => $orderHeader->reception_time_to,
                                    ],
                                    "destinatario" => [
                                        "codigo" => $user->id,
                                        "tipo_documento" => "CIP",
                                        "numero_documento" => $user->idnum,
                                        "nombre" => $user->description,
                                        "email" => $user->email,
                                        "telefonos" => [
                                            [
                                                "numero" => $user->telephone,
                                            ],
                                        ],
                                    ],
                                ];

                                $aexConfirmServiceArray["adicionales"] = [3];

                                \Log::warning("StoreController | Confirmation request -> " . json_encode($aexConfirmServiceArray));
                                $response = $client->request('POST', 'envios/confirmar_servicio', ['json' => $aexConfirmServiceArray, 'verify' => false]);
                                $aexConfirmServiceResponseJson = $response->getBody()->getContents();
                                $aexConfirmServiceResponse = json_decode($aexConfirmServiceResponseJson);
                                \Log::info("OrdersController | {$aexConfirmServiceResponseJson}");
                                if ($aexConfirmServiceResponse->codigo == 0) {
                                    // Guardamos los datos importantes para confirmar dependiendo del método de pago
                                    $orderHeader->aex_tracking_id = $aexConfirmServiceResponse->datos->numero_guia;
                                    $orderHeader->save();
                                }
                            }

                        }
                    }
                } catch (\Exception $e) {
                    \Log::warning('Error while attempting to call pagando');
                    \Log::warning($e->getMessage());
                }

            } else {
                // Delivery Externo
                $orderHeader->delivery_cost = 30000;
                $orderHeader->save();
            }

            if ($orderHeader->payment_method_id == 2) { // Si elegimos pagar con PAGANDO
                $title = 'Compra Massari';
                $description = 'Compra Online';
                $totalAmount = $orderHeader->order_amount;
                if ($orderHeader->discount) {
                    $totalAmount = $totalAmount - $orderHeader->discount_amount;
                }
                if (!is_null($orderHeader->delivery_cost)) {
                    $totalAmount = $totalAmount + $orderHeader->delivery_cost;
                }
                $tokenString = $this->pagando_private_key;
                $tokenString .= $orderHeader->id;
                $tokenString .= $title;
                $tokenString .= $description;
                $tokenString .= $totalAmount;
                $tokenString .= "request";

                $generateTransactionURL = env('PAGANDO_GENERATE_TRANSACTION');
                $paymentURL = env('PAGANDO_SHOW_TRANSACTION');
                $pagandoArray = [
                    'client_transaction_id' => $orderHeader->id,
                    'title' => $title,
                    'description' => $description,
                    'total_amount' => $totalAmount,
                    'return_url' => route('orders.finish', $orderHeader->id),
                    'cancel_url' => route('orders.cancel', $orderHeader->id),
                    'public_key' => $this->pagando_public_key,
                    'token' => sha1($tokenString),
                ];

                $singleBuyJson = json_encode($pagandoArray, JSON_UNESCAPED_SLASHES);
                \Log::debug("OrdersController | {$singleBuyJson}");
                try {

                    $client = new Client();

                    $response = $client->post($generateTransactionURL, ['json' => $pagandoArray, 'verify' => false]);
                    $pagandoJsonResponse = $response->getBody()->getContents();

                    $pagandoResponse = json_decode($pagandoJsonResponse);
                    \Log::info("OrdersController | {$pagandoJsonResponse}");

                    if ($pagandoResponse->success == true) {
                        $orderHeader->pagando_id = $pagandoResponse->data->transaction_hash;
                        $orderHeader->update();
                        \Log::info("OrdersController | Redirect to Pagando WebSite");
                        header("Location: $paymentURL" . $pagandoResponse->data->transaction_hash);
                        exit();
                    } else {
                        \Log::warning("OrdersController | Something happend on the pagando endpoint");
                        dd("This is a test error");
                    }

                } catch (\Exception $e) {
                    \Log::warning('Error while attempting to call pagando');
                    \Log::warning($e->getMessage());
                    // Rollback de la transacción
                    // If the order was canceled, we need to update the stock for each product
                    foreach ($orderHeader->details as $detail) {
                        $product = Products::find($detail->product_id);
                        if ($product->complex_product) {
                            foreach ($product->complexItems as $item) {
                                $complexItem = Products::find($item->involve_product_id);
                                $complexItem->quantity = $complexItem->quantity + ($item->quantity * $detail->qty);
                                $complexItem->save();
                            }
                        } else {
                            // Update Stock
                            $product->quantity = $product->quantity + $detail->quantity;
                            $product->save();
                        }

                    }
                    $orderHeader->status_id = 2; // Orden Cancelada - Dejamos un histórico para reportes
                    $orderHeader->save();
                    return redirect()->back()->with('error', 'Error al conectarse a Pagando');
                }
            }
            \Cart::destroy();
            if ($input['payment_method_id'] == 3) {
                return redirect()->route('orders.pre.deposit', $orderHeader->id);
            }

            return redirect()->route('orders.finish', $orderHeader->id);
        } else {
            \Log::warning("OrdersController | Error attempint to create a new order");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar crear su orden, intente nuevamente');
        }
    }

    public function preFinishPurchase($id)
    {
        $categories = Categories::all();
        if ($orderHeader = OrdersHeaders::find($id)) {

            //Send Mail
            $data = ['orderHeader' => $orderHeader];
            $mailList = [
                '0' => ['email' => $orderHeader->clients->email, 'description' => $orderHeader->clients->description],
                '1' => ['email' => 'rojalogo@gmail.com', 'description' => 'Rodrigo Lopez'],
                '2' => ['email' => 'aminmathias@gmail.com', 'description' => 'Amin Daher'],
                '3' => ['email' => 'massaripy@gmail.com', 'description' => 'MassariPY'],
            ];

            \Mail::send('front.mails.confirm', $data, function ($message) use ($mailList) {
                foreach ($mailList as $list) {
                    $message->to($list['email'], $list['description'])
                        ->subject('Massari - Orden Completada');
                }

            });
            return view('front.store.deposit', compact('orderHeader', 'categories'));
        }

        return redirect()->route('index')->with('error', 'Orden no encontrada');

    }

    public function finishPurchase($id)
    {
        $categories = Categories::all();
        \Cart::destroy();
        if ($orderHeader = OrdersHeaders::find($id)) {
            if ($orderHeader->status_id != 1) {
                \Log::warning('OrdersController | Order already closed');
                return view('front.store.completed', compact('orderHeader', 'categories'));
            }
            if (!is_null($orderHeader->aex_request_id) && $orderHeader->payment_method_id == 2) {
                $aexURL = env('AEX_SERVICE');
                $aexRequestArray = [
                    "clave_publica" => env('AEX_PUBLIC_KEY'),
                    "codigo_sesion" => 123,
                    "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
                ];
                try {
                    $client = new Client(['base_uri' => $aexURL]);
                    $response = $client->request('POST', 'autorizacion-acceso/generar',
                        ['json' => $aexRequestArray, 'verify' => false]);
                    $aexAuthRequestJson = $response->getBody()->getContents();

                    $aexAuthRequest = json_decode($aexAuthRequestJson);
                    \Log::info("OrdersController | {$aexAuthRequestJson}");
                    $aexConfirmServiceArray = [
                        "clave_publica" => env("AEX_PUBLIC_KEY"),
                        "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                        "id_solicitud" => $orderHeader->aex_request_id,
                        'id_tipo_servicio' => $orderHeader->aex_service_type,
                        'codigo_forma_pago' => 'C',
                        // Direccion de retiro de mercaderías
                        "pickup" => [
                            "codigo" => "2", // COdigo direccion
                            "calle_principal" => "Valois Rivarola",
                            "numero_casa" => '951',
                            "calle_transversal_1" => "Tte. Francisco Cusmanich",
                            "codigo_ciudad" => "PY0000",
                            "telefono_movil" => '0972162339',
                            'latitud' => '-25.279472',
                            'longitud' => '-57.610626',
                            'referencias' => '',
                        ],
                        // Direccion de entrega de mercaderías
                        "entrega" => [
                            "codigo" => $orderHeader->address_id, // COdigo direccion
                            "calle_principal" => $orderHeader->addresses->address,
                            "numero_casa" => $orderHeader->addresses->number,
                            "calle_transversal_1" => $orderHeader->addresses->address2,
                            "codigo_ciudad" => $orderHeader->addresses->cities->aex_code,
                            'referencias' => $orderHeader->addresses->references,
                            "disponible_desde" => date("Y-m-d", time() + 172800) . " " . $orderHeader->reception_time_from,
                            "disponible_hasta" => date("Y-m-d", time() + 172800) . " " . $orderHeader->reception_time_to,
                        ],
                        "destinatario" => [
                            "codigo" => $orderHeader->clients->id,
                            "tipo_documento" => "CIP",
                            "numero_documento" => $orderHeader->clients->idnum,
                            "nombre" => $orderHeader->clients->description,
                            "email" => $orderHeader->clients->email,
                            "telefonos" => [
                                [
                                    "numero" => $orderHeader->clients->telephone,
                                ],
                            ],
                        ],
                    ];
                    $aexConfirmServiceArray["adicionales"] = [];
                    \Log::warning("StoreController | Confirmation request -> " . json_encode($aexConfirmServiceArray));
                    $response = $client->request('POST', 'envios/confirmar_servicio', ['json' => $aexConfirmServiceArray, 'verify' => false]);
                    $aexConfirmServiceResponseJson = $response->getBody()->getContents();
                    $aexConfirmServiceResponse = json_decode($aexConfirmServiceResponseJson);
                    \Log::info("OrdersController | {$aexConfirmServiceResponseJson}");
                    if ($aexConfirmServiceResponse->codigo == 0) {
                        // Guardamos los datos importantes para  confirmar dependiendo del método de pago
                        $orderHeader->aex_tracking_id = $aexConfirmServiceResponse->datos->numero_guia;
                        $orderHeader->status_id = 3;
                        $orderHeader->save();
                    }
                } catch (\Exception $e) {
                    \Log::warning('Error while attempting to call AEX');
                    \Log::warning($e->getMessage());
                }
            }

            if ($orderHeader->payment_method_id == 1) {
                $orderHeader->status_id = 3;
                $orderHeader->save();
            }

            // Descontamos Stock
            foreach ($orderHeader->details as $detail) {
                $product = Products::find($detail->product_id);
                if ($product->complex_product) {
                    foreach ($product->complexItems as $item) {
                        $complexItem = Products::find($item->involve_product_id);
                        $complexItem->quantity = $complexItem->quantity - ($item->quantity * $detail->qty);
                        $complexItem->save();
                    }
                } else {
                    // Update Stock
                    $product->quantity = $product->quantity - $detail->quantity;
                    $product->save();
                }

            }
            //Send Mail
            $data = ['orderHeader' => $orderHeader];
            $mailList = [
                '0' => ['email' => $orderHeader->clients->email, 'description' => $orderHeader->clients->description],
                '1' => ['email' => 'rojalogo@gmail.com', 'description' => 'Rodrigo Lopez'],
                '2' => ['email' => 'aminmathias@gmail.com', 'description' => 'Amin Daher'],
                '3' => ['email' => 'massaripy@gmail.com', 'description' => 'MassariPY'],
            ];

            \Mail::send('front.mails.confirm', $data, function ($message) use ($mailList) {
                foreach ($mailList as $list) {
                    $message->to($list['email'], $list['description'])
                        ->subject('Massari - Orden Completada');
                }

            });
            return view('front.store.completed', compact('orderHeader', 'categories'));
        } else {
            \Log::warning("OrdersController | Order {$id} not found");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }
    }

    public function cancelOrder($id)
    {
        \Cart::destroy();
        // If the order was canceled, we need to update the stock for each product
        $ordersDetails = OrdersDetails::where('order_header_id', $id)->get();
        if ($ordersDetails->isEmpty()) {
            return redirect()->route('store.index')->with('error', 'No existe esta factura');
        }

        $header = OrdersHeaders::find($id);
        $header->status_id = 2; // Orden Cancelada - Dejamos un histórico para reportes
        $header->save();
        $categories = Categories::all();

        return view('front.store.canceled', compact('categories'));

    }
}
