<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Addresses;
use App\Models\Departments;
use App\Models\Cities;
use App\Models\Role;
use App\Models\User;
use App\Models\UserTaxInfo;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('not_guest');
    }

    public function showRegisterPage()
    {
        return view('front.auth.register');
    }

    public function doFrontRegister(RegisterRequest $request)
    {
        $input = $request->all();

        if (User::where('email', $input['email'])->first()) {
            \Log::warning("RegisterController | Email already exist");
            return redirect()->back()->with('error', 'Ya existe una cuenta con esta direccion de mail');
        }

        if (!$input['password'] == $input['password_confirm']) {
            \Log::warning("Password doesnt match");
            return redirect()->back()->with('error', 'Las contraseñas no coinciden');
        }

        $credentials = [
            'email' => $input['email'],
            'password' => $input['password'],
            'description' => $input['description'],
            'idnum' => $input['idnum'],
            "telephone" => $input['telephone'],
            'tax_name' => 'Sin Nombre',
            'tax_code' => '4444444-4'
        ];

        try {
            if ($user = \Sentinel::registerAndActivate($credentials)) {
                // Attach client Role
                $expectedRole = Role::where('slug', '=', 'client')->first();
                $user->roles()->attach($expectedRole->id);
                $user->permissions = $expectedRole->permissions;

                if (!$user->save()) {
                    \Log::warning('RegisterController | Cant attach permissions', $input);
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error', 'Problemas al actualizar registro.');
                }


                \Sentinel::authenticate($credentials);

                if (array_key_exists('new_city_description', $input)){
                    if (!is_null($input['new_city_description'])){
                        \Log::debug("RegisterController | Add new City");
                        $department = Departments::find($input['department_id']);
                        $newCityArray = ['description' => $input['new_city_description'], 'department_id' => $department->id];
                        $city = Cities::create($newCityArray);
                    }
                    // Guardamos la direccion del cliente
                    if (array_key_exists('city_id', $input) && $input['city_id'] != 0) {
                        $city = Cities::find($input['city_id']);
                        $city_name = $city->description;
                        $addressArray = [
                            "address" => $input['address1'],
                            "address2" => $input['address2'],
                            "city_id" => $city->id,
                            "number" => $input['number'],
                            'user_id' => $user->id
                        ];
                        $address = new Addresses($addressArray);
                        $address->save();
                    }
                }

                return redirect()->intended()->with('success', 'Usuario creado exitosamente');
            }
        } catch (\Exception $e) {
            \Log::warning("RegisterController | Error attempting to create front user");
            \Log::warning("RegisterController | {$e->getMessage()}");
            return redirect()->back()->with('error', 'Ocurrio un error inesperado, intente nuevamente');
        }

    }

    public function activateFrontUser($id, $code)
    {
        $user = \Sentinel::findUserById($id);

        if (!$user) {
            \Log::warning("User not found, id: {$id}");
            return redirect('login.page')->withErrors('Usuario no encontrado');
        }
        if (!\Activation::complete($user, $code)) {
            \Log::warning("User not activated {$user->email}");
            return redirect('login.page')->withErrors('Codigo de activacion inválido o expirado');
        }

        \Log::debug("RegisterController | User activated!");
        return redirect()->to('/')->with('success', 'Cuenta de Usuario activada');
    }
}
