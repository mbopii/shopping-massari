<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Addresses;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\PaymentForms;
use App\Models\Products;
use App\Models\UserTaxInfo;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'index', 'show', 'searchInStore', 'addToCart', 'showCart', 'destroyAllCart',
            'destroyOneCartItem', 'filterByRules', 'filterByCategory', 'filterByBrand',
            'getAddress', 'buyNow'
        ]]);
    }

    public function index()
    {

        $categories = Categories::all();

        $products = Products::orderBy('name', 'asc')->where('active', '=', 'true')->paginate(15); // TODO cambiarr a random

        $arrayProducts = array();
        $i = 0;
        $j = 0;
        $flag = 0;

        foreach ($products as $p) {
            $arrayProducts['items'][$i][$j] = $p;
            if ($flag == 3) {
                $i++;
                $j = 0;
                $flag = 0;
                continue;
            }
            $flag++;
            $j++;
        }
        $arrayProducts['paginate'] = $products;

        $categoriesFilter = Categories::all();
        $brandsFilter = Brands::all();

        return view('front.store.index', compact('arrayProducts', 'categoriesFilter', 'brandsFilter', 'categories'));
    }

    public function show($id)
    {
        $categories = Categories::all();

        if ($product = Products::find($id)) {
            if ($product->active == false){
                \Log::warning("StoreController | Inactive Product");
                return redirect()->back()->with('error', 'Producto fuera de lista');
            }
            $product->views = $product->views + 1;
            $product->save();
            \Log::info("StoreController | Product {$product->name} / {$product->id}");
            return view('front.store.show', compact('product', 'categories'));
        }

        \Log::info("StoreController | Product {$id} not found");
        return redirect()->back()->with('error', 'Producto no encontrado');
    }

    public function searchInStore(Request $request)
    {
        $categories = Categories::all();
        $input = $request->all();

        $where = '';
        $where .= "deleted_at is NULL AND ";
        $where .= "name ILIKE '%{$input['search']}%' OR description ILIKE '%{$input['search']}%' AND ";

        $where = trim($where, ' ');
        $where = trim($where, 'AND');
        $where = trim($where, ' ');

        $products = Products::whereRaw($where)->where('active', '=', 'true')->paginate(15);

        if ($products->isEmpty()) {
            \Log::debug("StoreController | Search results = 0");
            return redirect()->route('store.index')->with('error', 'No se encontraron resultados para su busqueda');
        }

        $arrayProducts = array();
        $i = 0;
        $j = 0;
        $flag = 0;

        foreach ($products as $p) {
            $arrayProducts['items'][$i][$j] = $p;
            if ($flag == 3) {
                $i++;
                $j = 0;
                $flag = 0;
                continue;
            }
            $flag++;
            $j++;
        }
        $arrayProducts['paginate'] = $products;

        $categoriesFilter = Categories::all();
        $brandsFilter = Brands::all();
        $search = $input['search'];


        return view('front.store.search', compact('arrayProducts', 'categoriesFilter', 'brandsFilter', 'categories', 'search'));
    }

    public function addToCart($id, $count)
    {
        \Log::info("CartController | New Request - Product_id= {$id} -- Count: {$count}");
        if ($products = Products::find($id)) {
            if ($products->active){
                $cart = Cart::content();
                $flag = 0;
                \Log::warning($cart->isEmpty());
                if (!$cart->isEmpty()) {
                    foreach ($cart as $item) {
                        \Log::warning(json_encode($item));
                        if ($item->id == $id) {
                            // Update Row
                            Cart::update($item->rowId, $count);
                            $message = 'Producto Actualizado';
                            $error = false;
                            $flag = 1;
                        }
                    }
                    if ($flag == 0) {
                        \Log::info("New Product on existent cart");
                        if (Cart::add($id, $products->name, $count, $products->price)->associate('App\Models\Products') !== false) {
                            $message = 'Agregado al carrito';
                            $error = false;
                        } else {
                            \Log::error('error al agregar al carrito');
                            $message = 'No se pudo agregar al carrito';
                            $error = true;
                        }
                    }
                } else {
                    if (Cart::add($id, $products->name, $count, $products->price)->associate('App\Models\Products') !== false) {
                        $message = 'Agregado al carrito';
                        $error = false;
                    } else {
                        \Log::error('error al agregar al carrito');
                        $message = 'No se pudo agregar al carrito';
                        $error = true;
                    }
                }
            }else{
                $error = true;
                $message = 'Producto no encontrado';
            }
        } else {
            $error = true;
            $message = 'Producto no encontrado';
        }

        $response = [
            'error' => $error,
            'message' => $message,
            'subtotal' => Cart::subtotal(0, ',', '.')
        ];
        \Log::debug('Sending response. ' . json_encode($response));
        return response()->json($response);

    }

    public function showCart()
    {
        $categories = Categories::all();
        $cartItems = Cart::content();
        $discount = 0;
        $flag = false;
        foreach ($cartItems as $item) {
            if ($product = Products::find($item->id)) {
                if ($product->active){
                    if ($item->model->discount) {
                        if ($item->qty >= $item->model->discount_quantity && $item->model->discount_quantity > 0 && $item->qty < $item->model->discount_quantity2) {
                            \Log::info("Discount 1");
                            $flag = true;
                            $item->flag = true;
                            $item->discount = '1';
                            $discount += $item->price * $item->qty * $item->model->discount_percentage / 100;
                        } elseif ($item->qty >= $item->model->discount_quantity2 && $item->model->discount_quantity2 > 0) {
                            \Log::info("Discount 2");
                            $flag = true;
                            $item->flag = true;
                            $item->discount = '2';
                            $discount += $item->price * $item->qty * $item->model->discount_percentage2 / 100;
                        } else {
                            \Log::warning("No discount");
                            $flag = false;
                            $item->flag = false;
                            unset($item->discount);
                        }
                    }
                }else{
                    $cartItems = Cart::remove($item->rowId);
                }
            }else{
                $cartItems = Cart::remove($item->rowId);
            }

        }
        if ($flag) {
            $subTotal = Cart::subtotal(0, '', '');
            $subTotalDiscount = $subTotal - $discount;
        } else {
            $subTotal = Cart::subtotal(0, '', '');
            $subTotalDiscount = Cart::subtotal(0, '', '');
        }

        return view('front.store.cart', compact('cartItems', 'subTotalDiscount', 'subTotal', 'flag', 'categories'));
    }

    public function destroyAllCart()
    {
        \Log::debug("Destroy all cart");
        Cart::destroy();

        return redirect()->route('store.index')->with("success", 'Carrito Eliminado correctamente');
    }

    public function destroyOneCartItem($rowId)
    {
        \Log::debug("Destroy one item from cart");

        $cartItems = Cart::remove($rowId);
        return redirect()->back()->with('success', 'Item Eliminado Correctamente');

    }


    public function filterByRules($rule)
    {
        $categories = Categories::all();
        if (empty($rule) || is_null($rule)) {
            \Log::warning("StoreController | Missing Rule");
            return redirect()->back()->with('error', 'Debe haber un filtro');
        }
        \Log::info("StoreController | Case to find: {$rule}");
        switch ($rule) {
            case 'featured':
                $products = Products::orderBy('featured', 'desc')->where('active', '=', 'true')->paginate(15);
                break;
            case 'asc':
                $products = Products::orderBy('name', 'asc')->where('active', '=', 'true')->paginate(15);
                break;
            case 'desc':
                $products = Products::orderBy('name', 'desc')->where('active', '=', 'true')->paginate(15);
                break;
            case 'asc_price':
                $products = Products::orderBy('price', 'asc')->where('active', '=', 'true')->paginate(15);
                break;
            case 'desc_price':
                $products = Products::orderBy('price', 'desc')->where('active', '=', 'true')->paginate(15);
                break;
        }

        $arrayProducts = array();
        $i = 0;
        $j = 0;
        $flag = 0;

        foreach ($products as $p) {
            $arrayProducts['items'][$i][$j] = $p;
            if ($flag == 3) {
                $i++;
                $j = 0;
                $flag = 0;
                continue;
            }
            $flag++;
            $j++;
        }
        $arrayProducts['paginate'] = $products;

        $categoriesFilter = Categories::all();
        $brandsFilter = Brands::all();


        return view('front.store.index', compact('categoriesFilter', 'brandsFilter', 'arrayProducts', 'categories'));
    }

    public function filterByCategory($category_id)
    {
        $categories = Categories::all();
        if (empty($category_id) || is_null($category_id)) {
            \Log::warning("StoreController | Missing Category");
            return redirect()->back()->with('error', 'Debe elegir una categoria');
        }
        \Log::info("StoreController | Category to find: {$category_id}");
        if ($category = Categories::find($category_id)) {
            $products = Products::where('category_id', '=', $category_id)->where('active', '=', 'true')->OrderBy('id')->paginate(15);
            if ($products->isEmpty()) {
                \Log::warning("StoreController |Products for Category_id = {$category_id} not found");
                return redirect()->back()->with('error', 'Productos no encontrados');
            }
            $arrayProducts = array();
            $i = 0;
            $j = 0;
            $flag = 0;
            foreach ($products as $p) {

                $arrayProducts['items'][$i][$j] = $p;
                if ($flag == 3) {
                    $i++;
                    $j = 0;
                    $flag = 0;
                    continue;
                }
                $flag++;
                $j++;
            }
            $arrayProducts['paginate'] = $products;

            $categoriesFilter = Categories::all();
            $brandsFilter = Brands::all();


            return view('front.store.index', compact('arrayProducts', 'categoriesFilter', 'brandsFilter', 'categories'));
        } else {
            \Log::warning("StoreController | SubCategory not found");
            return redirect()->back()->with('error', 'Categoría no encontrada');
        }
    }

    public function filterByBrand($brand_id)
    {
        $categories = Categories::all();
        if (empty($brand_id) || is_null($brand_id)) {
            \Log::warning("StoreController | Missing Brand");
            return redirect()->back()->with('error', 'Debe elegir una marca');
        }
        \Log::info("StoreController | Brand to find: {$brand_id}");
        if ($category = Brands::find($brand_id)) {
            $products = Products::where('brand_id', '=', $brand_id)->where('active', '=', 'true')->OrderBy('id')->paginate(15);
            if ($products->isEmpty()) {
                \Log::warning("StoreController | Products for Brand = {$brand_id} not found");
                return redirect()->back()->with('error', 'Productos no encontrados');
            }
            $arrayProducts = array();
            $i = 0;
            $j = 0;
            $flag = 0;
            foreach ($products as $p) {

                $arrayProducts['items'][$i][$j] = $p;
                if ($flag == 3) {
                    $i++;
                    $j = 0;
                    $flag = 0;
                    continue;
                }
                $flag++;
                $j++;
            }
            $arrayProducts['paginate'] = $products;

            $categoriesFilter = Categories::all();
            $brandsFilter = Brands::all();

            return view('front.store.index', compact('arrayProducts', 'categoriesFilter', 'brandsFilter', 'categories'));
        } else {
            \Log::warning("StoreController | Brand not found");
            return redirect()->back()->with('error', 'Categoría no encontrada');
        }
    }

    public function checkout()
    {
        $categories = Categories::all();
        $paymentsForm = PaymentForms::all()->except([1, 4]);
        $cartItems = Cart::content();

        if ($cartItems->isEmpty()) {
            \Log::warning("StoreController | Missing Products on cart");
            return redirect()->route('store.index')->with('error', 'No tiene productos en su carrito');
        }

        $discount = 0;
        $flag = false;
        foreach ($cartItems as $item) {
            if ($product = Products::find($item->id)) {
                if ($product->active){
                    if ($item->qty >= $item->model->discount_quantity && $item->model->discount_quantity > 0 && $item->qty < $item->model->discount_quantity2) {
                        \Log::info("Discount 1");
                        $flag = true;
                        $item->flag = true;
                        $item->discount = '1';
                        $discount += $item->price * $item->qty * $item->model->discount_percentage / 100;
                    } elseif ($item->qty >= $item->model->discount_quantity2 && $item->model->discount_quantity2 > 0) {
                        \Log::info("Discount 2");
                        $flag = true;
                        $item->flag = true;
                        $item->discount = '2';
                        $discount += $item->price * $item->qty * $item->model->discount_percentage2 / 100;
                    } else {
                        \Log::warning("No discount");
                        $flag = false;
                        $item->flag = false;
                        unset($item->discount);
                    }
                }else{
                    $cartItems = Cart::remove($item->rowId);
                }

            }else{
                $cartItems = Cart::remove($item->rowId);
            }

        }


        if (is_null($cartItems)) {
            \Log::warning("StoreController | Missing Products on cart");
            return redirect()->route('store.index')->with('error', 'No tiene productos en su carrito');
        }


        if ($flag) {
            $subTotalDiscount = Cart::subtotal(0, '', '');
            $subTotal = $subTotalDiscount - $discount;
        } else {
            $subTotal = Cart::subtotal(0, '', '');
            $subTotalDiscount = Cart::subtotal(0, '', '');
        }


        if (!\Sentinel::getUser()) {
            return redirect()->route('login.page');
        }
        if (is_null(\Sentinel::getUser()->telephone)){
            \Log::warning("StoreController | Missing phone number");
            return redirect()->route('front.user.show', \Sentinel::getUser()->id)->with('error', 'Debe actualizar su numero de telefono');
        }
        $addresses = Addresses::where("user_id", \Sentinel::getUser()->id)->get();
        if ($addresses->isEmpty()) {
            \Log::warning("StoreController | Empty address for user");
            return redirect()->route('front.user.addresses', \Sentinel::getUser()->id)->with("error", 'Debe agregar una dirección para poder terminar su compra');
        }
        $i = 0;
        foreach ($addresses as $a) {
            $preview[$i]['id'] = $a->id;
            $preview[$i]['description'] = $a->address;
            $i++;
        }


        return view("front.store.checkout", compact("cartItems", "preview", 'paymentsForm',
            'subTotal', 'subTotalDiscount', 'flag', 'categories'));
    }

    public function getAddress($id)
    {
        $addresses = Addresses::find($id);
        $arrayAddress['error_code'] = false;
        $arrayAddress['id'] = $addresses->id;
        $arrayAddress['address'] = $addresses->address;
        $arrayAddress['city'] = $addresses->cities->description;
        $arrayAddress['number'] = $addresses->number;
        $arrayAddress['references'] = $addresses->references;
        $arrayAddress['tax_name'] = $addresses->user->tax_name;
        $arrayAddress['tax_code'] = $addresses->user->tax_code;

        if (!is_null($addresses->cities->aex_code)) {
            $collection = false;
            // Si la ciudad tiene aex_code, se debe calcular el costo del envio;
            $aexURL = env('AEX_SERVICE');
            $aexRequestArray = [
                "clave_publica" => env('AEX_PUBLIC_KEY'),
                "codigo_sesion" => 123,
                "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123)
            ];
            $package = [];
            foreach (Cart::content() as $content) {
                $product = Products::find($content->id);
                \Log::warning($content->qty);
                if ($content->qty > 1){
                    for ($i = 1; $i <= $content->qty; $i++){
                        $package[] = [
                            "peso" => $product->weight,  // KG
                            "largo" => $product->height, // CM
                            "alto" => $product->width,  // CM
                            "ancho" => $product->length, // CM,
                            "valor" => $product->price
                        ];
                    }
                }else{
                    $package[] = [
                        "peso" => $product->weight,  // KG
                        "largo" => $product->height, // CM
                        "alto" => $product->width,  // CM
                        "ancho" => $product->length, // CM,
                        "valor" => $product->price
                    ];
                }
            }
            \Log::warning("File: " . json_encode($package));
            try {

                $client = new Client(['base_uri' => $aexURL]);
                $response = $client->request('POST', 'autorizacion-acceso/generar', ['json' => $aexRequestArray, 'verify' => false]);
                $aexAuthRequestJson = $response->getBody()->getContents();

                $aexAuthRequest = json_decode($aexAuthRequestJson);
                \Log::info("StoreController | {$aexAuthRequestJson}");

                if ($aexAuthRequest->codigo == 0) {
                    // Calculamos dependiendo de la ciudad elegida
                    $aexCalculateRequestArray = [
                        "clave_publica" => env("AEX_PUBLIC_KEY"),
                        "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                        "origen" => "PY0000",
                        "destino" => $addresses->cities->aex_code,
                        "paquetes" => $package,
                        "codigo_tipo_carga" => "P"
                    ];
                    \Log::warning("StoreController | Calculator request -> " . json_encode($aexCalculateRequestArray));
                    $response = $client->request('POST', 'envios/calcular', ['json' => $aexCalculateRequestArray, 'verify' => false]);
                    $aexCalculateRequestJson = $response->getBody()->getContents();
                    $aexCalculateRequest = json_decode($aexCalculateRequestJson);
                    \Log::info("StoreController | {$aexCalculateRequestJson}");
                    if ($aexCalculateRequest->codigo == 0) {
                        foreach ($aexCalculateRequest->datos as $datos) {
                            if ($datos->id_tipo_servicio == 3) {
                                if (is_array($datos->adicionales)){
                                    foreach ($datos->adicionales as $adicionales){
                                        if ($adicionales->id_adicional == 3 ){
                                            $collection = true;
                                        }
                                    }
                                }
                            }
                        }

                        $arrayAddress['delivery'] = [
                            'cost' => $datos->costo_servicio,
                            'time' => $datos->tiempo_entrega . " Hs.",
                            'service' => $datos->denominacion,
                            'text' => 'Pagas tu envio al recibir tu paquete',
                            'collection' => $collection
                        ];

                    }else{
                        $arrayAddress['delivery'] = [
                            'cost' => '30000',
                            'time' => 24 . " Hs.",
                            'service' => 'Servicio Tercerizado',
                        ];
                    }
                }

            } catch (\Exception $e) {
                \Log::warning('Error while attempting to call aex');
                \Log::warning($e->getMessage());
            }
        } else {
            $arrayAddress['delivery'] = [
                'cost' => '30000',
                'time' => 24 . " Hs.",
                'service' => 'Servicio Tercerizado',
            ];
        }


        return response()->json($arrayAddress);
    }

    public function buyNow($product_id, $quantity)
    {
        \Log::info("StoreController | New Request | Buy NOW! - Product_id= {$product_id} -- Count: {$quantity}");
        $loggedUser = false;

        if (\Sentinel::getUser()){
            $loggedUser = true;
        }
        if ($products = Products::find($product_id)) {
//            Cart::destroy();
            if (Cart::add($product_id, $products->name, $quantity, $products->price)->associate('App\Models\Products') !== false) {
                \Log::debug("StoreController | Product Added");
                $error = false;
                $message = 'Producto Agregado';
            } else {
                \Log::warning("StoreController | There's was a problem adding the product to the cart");
                $error = True;
                $message = 'Error al agregar producto';
            }
        } else {
            \Log::warning("StoreController | Product not found");
            $error = true;
            $message = 'Producto no existe';
        }

        $response = [
            'error' => $error,
            'message' => $message,
            'subtotal' => Cart::subtotal(0, ',', '.'),
            'logged' => $loggedUser
        ];
        \Log::debug('Sending response. ' . json_encode($response));
        return response()->json($response);
    }
}