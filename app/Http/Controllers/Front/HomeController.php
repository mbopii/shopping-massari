<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'aboutUs', 'showContact', 'changeLanguage', 'sendContact']]);
    }

    public function index()
    {
        $categories = Categories::all();

        $newProducts = Products::orderBy('id', 'desc')->limit(8)->get(); // TODO change for most popular
        $listCategories = Categories::orderBy('id', 'asc')->limit(4)->get();
        $featuredProducts = Products::where('featured', true)->limit(4)->orderBy('id', 'desc')->get();

        // Words Section

        return view('front.welcome', compact('categories', 'newProducts', 'listCategories', 'featuredProducts'));
    }

    public function aboutUs()
    {
        $categories = Categories::all();
        return view('front.various.about', compact('categories'));
    }

    public function showContact()
    {
        $categories = Categories::all();
        return view('front.various.contact', compact('categories'));
    }

    public function sendContact(Request $request)
    {
        $input = $request->all();
        $data = ['data' => $input];
        \Log::debug("HomeController | Send new Mail: " . json_encode($data));
        \Mail::send('front.mails.contact', $data, function ($message) use ($data) {
            $message->to('massaripy@gmail.com', 'Massari')
                ->subject('Massari - Contacto')
                ->cc($data['data']['email'], $data['data']['name']);
        });

        return response()->json(['error' => false, 'message' => 'Mensaje enviado exitosamente']);
    }

    public function changeLanguage($lang)
    {
        \Session::put('applocale', $lang);
        return redirect()->back();
    }

    public function showFAQ()
    {
        $categories = Categories::all();
        return view('front.various.faq', compact('categories'));
    }
}