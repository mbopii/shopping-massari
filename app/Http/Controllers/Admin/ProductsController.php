<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsRequest;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\ComplexProductsDetails;
use App\Models\Products;
use App\Models\SubCategories;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;


class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('products')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['category_id']) && $input['category_id'] != 0) $where .= "category_id = {$input['category_id']} AND ";
            if (!is_null($input['brand_id']) && $input['brand_id'] != 0) $where .= "brand_id = {$input['brand_id']} AND ";
            if (!is_null($input['description']) && $input['description'] != '') $where .= "name ilike '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $products = Products::orderBy('id', 'desc')->paginate(20);
            } else {
                $products = Products::whereRaw($where)->orderBy('id', 'desc')->paginate(20);
            }
            // Filter data
            $category_id = isset($input['category_id']) ? $input['category_id'] : '';
            $description = isset($input['description']) ? $input['description'] : '';
            $brand_id = isset($input['brand_id']) ? $input['brand_id'] : '';
            $q = $input['q'];
        } else {
            $category_id = '';
            $description = '';
            $q = '';
            $brand_id = '';
            $products = Products::orderBy('id', 'desc')->paginate(20);
        }

        $categories = [0 => 'Seleccione..'];
        $categories += Categories::pluck('description', 'id')->toArray();

        $brands = [0 => 'Seleccione..'];
        $brands += Brands::pluck('description', 'id')->toArray();
        return view('administration.products.index', compact('products', 'categories', 'category_id', 'description', 'q', 'brands', 'brand_id'));
    }

    public function show()
    {

    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $categories = ["" => 'Seleccione ..'];
        $categories += Categories::all()->pluck('description', 'id')->toArray();
        $brands = ["" => 'Seleccione ..'];
        $brands += Brands::all()->pluck('description', 'id')->toArray();
        $productsJson = Products::all();

        return view('administration.products.create', compact('categories', 'brands', 'productsJson'));
    }

    public function store(ProductsRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();

        // If category_id has the prefix "cat" it means that the user select a category only, if not, the user select a
        // sub_category, therefor, we have the correspondent category by relationships
//        $category = explode("_", $input['category_id']);

        if (strlen($input['description']) >= 10000) {
            \Log::warning("ProductsController | Description value too long");
            return redirect()->back()->with('error', 'Valor de descripción muy alto, debe tener menos caracteres');
        }

        $arrayToSave = [];
//        if ($category[0] == 'cat') {
//            // Only a category
//            $arrayToSave['category_id'] = $category[1];
//        } else {
//            // With a sub_category
//            $subCategory = SubCategories::find($category[0]);
//            $arrayToSave['category_id'] = $subCategory->category_id;
//            $arrayToSave['subcategory_id'] = $subCategory->id;
//        }

        $arrayToSave['category_id'] = $input['category_id'];

        if (array_key_exists('featured', $input)) {
            $arrayToSave['featured'] = true;
        }

        if (array_key_exists('brand_id', $input)) {
            $arrayToSave['brand_id'] = $input['brand_id'];
        }

        if (array_key_exists('discount', $input)) {
            // Descuento
            $arrayToSave['discount'] = true;
            $arrayToSave['discount_percentage'] = $input['discount_percentage'];
            $arrayToSave['discount_quantity'] = $input['discount_quantity'];
            $arrayToSave['discount_percentage2'] = $input['discount_percentage2'];
            $arrayToSave['discount_quantity2'] = $input['discount_quantity2'];
        }

        $arrayToSave['name'] = $input['name'];
        $customDescription = str_replace('<ul>', '<ul class="spd-list">', $input['description']);
        $arrayToSave['description'] = $customDescription;
        $arrayToSave['price'] = number_format($input['price'], 0, '', '');

        $arrayToSave['weight'] = !is_null($input['weight']) && $input['weight'] != 0 ? $input['weight'] : 1;
        $arrayToSave['height'] = !is_null($input['height']) && $input['height'] != 0 ? $input['height'] : 1;
        $arrayToSave['width'] = !is_null($input['width']) && $input['width'] != 0 ? $input['width'] : 1;
        $arrayToSave['length'] = !is_null($input['length']) && $input['length'] != 0 ? $input['length'] : 1;

        $arrayToSave['bar_code'] = $input['bar_code'];

        $arrayToSave['quantity'] = 0;

        if (!$product = Products::create($arrayToSave)) {
            \Log::warning("ProductsController | Error attempting to create a new product");
            return redirect()->back()->with('error', 'Error al intentar crear el registro');
        }

        if (array_key_exists('complex', $input)) {
            for ($i = 0; $i < count($input['complex_product_id']); $i++) {
                $complex['involve_product_id'] = $input['complex_product_id'][$i];
                $complex['product_id'] = $product->id;
                $complex['quantity'] = $input['complex_quantity'][$i];
                ComplexProductsDetails::create($complex);
            }
            $product->complex_product = true;
            $product->save();
        }

        $j = 2;
        for ($i = 1; $i <= 3; $i++) {
            $indexNumber = 'product_image_' . $i;
            \Log::debug("ProductsController | Input to validate: {$indexNumber}");

            if (array_key_exists($indexNumber, $input) && !is_null($input[$indexNumber])) {
                if (!file_exists(public_path("/front/img/products/{$product->id}"))) {
                    \Log::debug("ProductsController | We create the image folder for product {$product->id}");
                    \File::makeDirectory(public_path("/front/img/products/{$product->id}"), 0777, true, true);
                }

                if (!is_null($input['answer'])) {
                    if (($input['answer'] + 1) == $i) {
                        \Log::info("ProductsController | This is the first image to display");
                        $fileName = 1 . '.jpg';
                    } else {
                        $fileName = $j . '.jpg';
                        $j++;
                    }
                } else {
                    $fileName = $i . '.jpg';
                }
                $input[$indexNumber]->move(public_path("/front/img/products/{$product->id}"), $fileName);
            }
        }


        return redirect()->route('products.index')->with('success', 'Producto creado exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($product = Products::find($id)) {
            $categories = ["" => 'Seleccione ..'];
            $categories += Categories::all()->pluck('description', 'id')->toArray();
            $brands = ["" => 'Seleccione ..'];
            $brands += Brands::all()->pluck('description', 'id')->toArray();
            $productsJson = Products::all();
            return view('administration.products.edit', compact('productsJson', 'product', 'categories', 'brands'));
        } else {
            \Log::warning("ProductsController | Product {$id} not found");
            return redirect()->back()->with('error', 'Producto no encontrado');
        }
    }

    public function update(ProductsRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        if ($product = Products::find($id)) {
            $input = $request->all();

            if (strlen($input['description']) >= 10000) {
                \Log::warning("ProductsController | Description value too long");
                return redirect()->back()->with('error', 'Valor de descripción muy alto, debe tener menos caracteres');
            }
            if (array_key_exists('active', $input)){
                $product->active = true;
            }else{
                $product->active = false;
            }
            if (array_key_exists('featured', $input)) {
                $product->featured = true;
            }else{
                $product->featured = false;
            }

            if (array_key_exists('brand_id', $input)) {
                $product->brand_id = $input['brand_id'];
            }

            if (array_key_exists('discount', $input)) {
                // Descuento
                $product->discount = true;
                $product->discount_percentage = $input['discount_percentage'];
                $product->discount_quantity = $input['discount_quantity'];
                $product->discount_percentage2 = $input['discount_percentage2'];
                $product->discount_quantity2 = $input['discount_quantity2'];
            }else{
                $product->discount = false;
                $product->discount_percentage = 0;
                $product->discount_quantity = 0;
                $product->discount_percentage2 = 0;
                $product->discount_quantity2 = 0;
            }

            $product->name = $input['name'];
            $customDescription = str_replace('<ul>', '<ul class="spd-list">', $input['description']);
            $product->description = $customDescription;
            $product->price = number_format($input['price'], 0, '', '');

            $product->weight = !is_null($input['weight']) && $input['weight'] != 0 ? $input['weight'] : 1;
            $product->height = !is_null($input['height']) && $input['height'] != 0 ? $input['height'] : 1;
            $product->width = !is_null($input['width']) && $input['width'] != 0 ? $input['width'] : 1;
            $product->length = !is_null($input['length']) && $input['length'] != 0 ? $input['length'] : 1;

            $product->bar_code = $input['bar_code'];

            $product->category_id = $input['category_id'];

            if (!$product->save()) {
                \Log::warning("ProductsController | Error attempting to update product id {$id}");
                return redirect()->back()->with('error', 'Ocurrio un error al intentar modificar el registro')->withInput();
            }

            if (array_key_exists('complex', $input)) {
                for ($i = 0; $i < count($input['complex_product_id']); $i++) {
                    $complexFlag = ComplexProductsDetails::where('product_id', '=', $product->id)->where('involve_product_id', '=', $input['complex_product_id'][$i])->first();
                    if (is_null($complexFlag)){
                        $complex['involve_product_id'] = $input['complex_product_id'][$i];
                        $complex['product_id'] = $product->id;
                        $complex['quantity'] = $input['complex_quantity'][$i];
                        ComplexProductsDetails::create($complex);
                    }else{
                        $complexFlag->quantity = $input['complex_quantity'][$i];
                        $complexFlag->save();
                    }
                }
                $product->complex_product = true;
                $product->save();
            }

            $j = 2;
            for ($i = 1; $i <= 3; $i++) {
                $indexNumber = 'product_image_' . $i;
                \Log::debug("ProductsController | Input to validate: {$indexNumber}");
                $temp = 99;
                if (array_key_exists($indexNumber, $input) && !is_null($input[$indexNumber])) {
                    if (!file_exists(public_path("/front/img/products/{$product->id}"))) {
                        \Log::debug("ProductsController | We create the image folder for product {$product->id}");
                        \File::makeDirectory(public_path("/front/img/products/{$product->id}"), 0777, true, true);
                    }


                    if (!is_null($input['answer'])) {
                        if (($input['answer']) == $i) {
                            \Log::info("ProductsController | This is the first image to display");
                            if (file_exists(public_path("front/img/products/{$product->id}/1.jpg"))) {
                                rename(public_path("front/img/products/{$product->id}/1.jpg"),
                                    public_path("front/img/products/{$product->id}/{$i}.jpg"));
                            }
                            $fileName = 1 . '.jpg';
                        } else {
                            $fileName = $j . '.jpg';
                            $j++;
                        }
                    } else {
                        $fileName = $i . '.jpg';
                    }
                    $input[$indexNumber]->move(public_path("/front/img/products/{$product->id}"), $fileName);
                } else {
                    if (($input['answer']) == $i) {
                        // First image has to be 1.jpg
                        if (file_exists(public_path("front/img/products/{$product->id}/{$i}.jpg"))) {
                            rename(public_path("front/img/products/{$product->id}/{$i}.jpg"),
                                public_path("front/img/products/{$product->id}/{$temp}.jpg"));

                            if (file_exists(public_path("front/img/products/{$product->id}/1.jpg"))) {
                                // Rename al numero reservado
                                rename(public_path("front/img/products/{$product->id}/1.jpg"),
                                    public_path("front/img/products/{$product->id}/{$i}.jpg"));
                            }

                            // El 1 ya esta disponible para asignar a la imagen elegida
                            rename(public_path("front/img/products/{$product->id}/{$temp}.jpg"),
                                public_path("front/img/products/{$product->id}/1.jpg"));
                        }
                    }
                }
            }

            return redirect()->route('products.index')->with('success', 'Registro modificado exitosamente');
        } else {
            \Log::warning("ProductsController | Product {$id} not found");
            return redirect()->back()->with('error', 'Producto no encontrado');
        }
    }

    public function destroy($id)
    {
        $error = true;
        $message = '';
        \Log::debug("PermissionsController | Delete permissions request -> {$id}");
        try {
            if (!\Sentinel::getUser()->hasAccess('permissions.destroy')) {
                \Log::error('PermissionsController | Unauthorized access attempt',
                    ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
                $message = 'No posee los permisos para esta accion';
            } else {
                if ($product = Products::find($id)) {
                    \Log::notice('ProductsController | Deleting product',
                        ['action' => 'permissions.delete', 'id' => $id]);

                    try {
                        if (Products::destroy($id)) {
                            $message = 'Producto eliminado correctamente';
                            $error = false;
                        }
                    } catch (QueryException $e) {
                        \Log::critical($e->getMessage(),
                            ['action' => 'products.delete', 'id' => $id]);
                        $message = 'Error intentando eliminar el producto, intente nuevamente';
                    }
                } else {
                    \Log::notice('ProductsController | Product doesnt exists', ['action' => 'products.delete', 'id' => $id]);
                    $message = 'Producto no encontrado';
                }
            }
            return response()->json([
                'error' => $error,
                'message' => $message,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $error,
                'message' => 'Ocurrio un error inesperado',
            ]);
        }
    }

    public function removeComplex($complex_id, $product_id)
    {
        \Log::warning("{$complex_id} | {$product_id}");
        $error = true;
        $message = '';
        \Log::debug("ProductsController | Delete complexProduct request -> {$complex_id}");
        try {

            if ($complexProduct = Products::find($complex_id)) {
                \Log::notice('ProductsController | Deleting product',
                    ['action' => 'products.complex.delete', 'id' => $complex_id]);

                try {
                    foreach ($complexProduct->complexItems as $items) {
                        if ($items->involve_product_id == $product_id) {
                            ComplexProductsDetails::destroy($items->id);
                        }
                    }

                    $error = false;
                    $message = 'Producto Eliminado Correctamente';
                    return response()->json(['error' => false, 'message' => 'Produucto Eliminado']);

                } catch (QueryException $e) {
                    \Log::critical($e->getMessage(),
                        ['action' => 'products.complex.delete', 'id' => $product_id]);
                    $message = 'Error intentando eliminar el producto, intente nuevamente';
                }
            } else {
                \Log::notice('ProductsController | Product doesnt exists', ['action' => 'products.delete', 'id' => $product_id]);
                $message = 'Producto no encontrado';
            }

            return response()->json([
                'error' => $error,
                'message' => $message,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $error,
                'message' => 'Ocurrio un error inesperado',
            ]);
        }
    }

    public function getSingleProduct($id)
    {
        if (is_null($id)) {
            \Log::warning("ProductsController | Missing id to search");
            return json_encode(['error' => true]);
        }

        $product = Products::find($id);
        if (!is_null($product)) {
            return json_encode(['error' => false, 'product' => $product]);
        } else {
            return json_encode(['error' => true, 'code' => 10]);
        }
    }

    public function destroyImage(Request $request)
    {
        $input = $request->all();

        $image = $input['image'];
        $product_id = $input['product_id'];

        // Open Folder
        if (!file_exists(public_path("/front/img/products/{$product_id}"))) {
            \Log::warning("ProductsController | Folder does not exist");
            return response()->json(['error' => 'true', 'message' => 'No existe la carpeta de imagenes para este producto']);
        }

        $file = public_path() . "/front/img/products/{$product_id}/{$image}.jpg";
        unlink($file);
        return response()->json(['error' => false, 'message' => 'Imiagen Eliminada']);
    }

}