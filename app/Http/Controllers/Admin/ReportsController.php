<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\OrdersHeaders;
use App\Models\PaymentForms;
use App\Models\Products;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
//        if (!\Sentinel::getUser()->hasAccess('reports.all')) {
//            \Log::error('Unauthorized access attempt',
//                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
//            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
//        }

        return view('administration.reports.index');
    }

    public function sales(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_end']}' AND ";
            if (!is_null($input['payment_method_id']) && $input['payment_method_id'] != 0) $where .= "payment_method_id = {$input['payment_method_id']} AND ";

            $where .= "status_id IN (3, 4) AND "; // No mostrar ordenes canceladas
            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");
            
            if ($where == '') {
                $excelDownload = OrdersHeaders::orderBy('id', 'desc')->get();
                $sales = OrdersHeaders::orderBy('id', 'desc')->paginate(20);
            } else {
                $excelDownload = OrdersHeaders::orderBy('id', 'desc')->whereRaw($where)->get();
                $sales = OrdersHeaders::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }
            if (isset($input['download'])) {
                if ($input['download'] == 'pdf'){
                    return $this->downloadPDF('administration.reports.pdf.sales', $excelDownload, 'ReporteVentas');
                }

                if ($input['download'] == 'xls'){
                    $this->downloadXLS('administration.reports.excel.sales', $excelDownload, 'ReporteVentas');
                }
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $payment_method_id = isset($input['payment_method_id']) ? $input['payment_method_id'] : '';
            $q = $input['q'];

        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $payment_method_id = '';

            $sales = OrdersHeaders::orderBy('id', 'desc')->where('status_id', '!=', 2)->paginate(20);
        }
        $paymentForms = [0 => 'Seleccione..'];
        $paymentForms += PaymentForms::all()->pluck('description', 'id')->toArray();

        return view('administration.reports.sales', compact('sales', 'date_end', 'date_start', 'q', 'payment_method_id', 'paymentForms'));
    }

    public function generalStock(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['brand_id']) && $input['brand_id'] != 0) $where .= "brand_id = {$input['brand_id']} AND ";
            if (!is_null($input['name'])) $where .= "name ILIKE '%{$input['name']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $excelDownload = Products::orderBy('id', 'desc')->get();
                $products = Products::orderBy('id', 'desc')->paginate(20);
            } else {
                $excelDownload = Products::orderBy('id', 'desc')->whereRaw($where)->get();
                $products = Products::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }
            if (isset($input['download'])) {
                if ($input['download'] == 'pdf'){
                    return $this->downloadPDF('administration.reports.pdf.stock', $excelDownload, 'ReporteStock');
                }

                if ($input['download'] == 'xls'){
                    $this->downloadXLS('administration.reports.excel.stock', $excelDownload, 'ReporteStock');
                }
            }
            $q = '';
            $brand_id = '';
            $name = isset($input['name']) ? $input['name'] : '';
        } else {
            $q = '';
            $brand_id = '';
            $name = '';
            $products = Products::orderBy('id', 'desc')->paginate(20);
        }
        $brands = [0 => 'Seleccione..'];
        $brands += Brands::all()->pluck('description', 'id')->toArray();

        return view('administration.reports.stock', compact('products', 'brands', 'brand_id', 'q', 'name'));
    }

    public function aexReports(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_end']}' AND ";
            if (!is_null($input['payment_method_id']) && $input['payment_method_id'] != 0) $where .= "payment_method_id = {$input['payment_method_id']} AND ";

            $where .= 'aex_request_id IS NOT NULL AND status_id IN (3, 4) AND ';

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $excelDownload = OrdersHeaders::orderBy('id', 'desc')->get();
                $sales = OrdersHeaders::orderBy('id', 'desc')->paginate(20);
            } else {
                $excelDownload = OrdersHeaders::orderBy('id', 'desc')->whereRaw($where)->get();
                $sales = OrdersHeaders::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }
            if (isset($input['download'])) {
                if ($input['download'] == 'pdf'){
                    return $this->downloadPDF('administration.reports.pdf.aex', $excelDownload, 'ReporteAex');
                }

                if ($input['download'] == 'xls'){
                    $this->downloadXLS('administration.reports.excel.aex', $excelDownload, 'ReporteAex');
                }
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $payment_method_id = isset($input['payment_method_id']) ? $input['payment_method_id'] : '';
            $q = $input['q'];

        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $payment_method_id = '';

            $sales = OrdersHeaders::orderBy('id', 'desc')
                ->whereRaw("aex_request_id is not null and status_id in (3, 4)")
                ->orderBy('id', 'desc')
                ->paginate(20);
        }
        $paymentForms = [0 => 'Seleccione..'];
        $paymentForms += PaymentForms::all()->pluck('description', 'id')->toArray();

        return view('administration.reports.aex', compact('sales', 'date_end', 'date_start', 'q', 'payment_method_id', 'paymentForms'));
    }

    public function payAexCollect($id)
    {
        if ($header = OrdersHeaders::find($id)){
            $header->aex_collect = true;
            $header->save();
            return redirect()->back()->with('success', 'Campo Actualizado exitosamente');
        }else{
            \Log::warning("ReportsController | Purchase not found");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }
    }

    public function collectAexPayments($id)
    {
        if ($header = OrdersHeaders::find($id)){
            $header->aex_payment = true;
            $header->save();
            return redirect()->back()->with('success', 'Campo Actualizado exitosamente');
        }else{
            \Log::warning("ReportsController | Purchase not found");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }
    }

    public function utilitiesReports(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';

            if (!is_null($input['category_id']) && $input['category_id'] != 0) $where .= "categories.id = {$input['category_id']} AND ";
            if (!is_null($input['description']) ) $where .= "products.name ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $excelDownload = \DB::table("products")
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->join('purchases_details', "products.id", '=', 'purchases_details.product_id')
                    ->selectRaw('AVG(CAST(purchases_details.single_amount as integer)), products.name, categories.description, products.id as id, products.price')
                    ->groupBy('purchases_details.single_amount', 'products.name', 'categories.description', 'products.id', 'products.price')
                    ->get();
                $products =\DB::table("products")
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->join('purchases_details', "products.id", '=', 'purchases_details.product_id')
                    ->selectRaw('AVG(CAST(purchases_details.single_amount as integer)), products.name, categories.description, products.id as id, products.price')
                    ->groupBy('purchases_details.single_amount', 'products.name', 'categories.description', 'products.id', 'products.price')
                    ->paginate(20);
            } else {
                $excelDownload =\DB::table("products")
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->join('purchases_details', "products.id", '=', 'purchases_details.product_id')
                    ->selectRaw('AVG(CAST(purchases_details.single_amount as integer)), products.name, categories.description, products.id as id, products.price')
                    ->groupBy('purchases_details.single_amount', 'products.name', 'categories.description', 'products.id', 'products.price')
                    ->whereRaw($where)
                    ->get();
                $products = \DB::table("products")
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->join('purchases_details', "products.id", '=', 'purchases_details.product_id')
                    ->selectRaw('AVG(CAST(purchases_details.single_amount as integer)), products.name, categories.description, products.id as id, products.price')
                    ->groupBy('purchases_details.single_amount', 'products.name', 'categories.description', 'products.id', 'products.price')
                    ->whereRaw($where)
                    ->paginate(20);
            }
            if (isset($input['download'])) {
                if ($input['download'] == 'pdf'){
                    return $this->downloadPDF('administration.reports.pdf.utilities', $excelDownload, 'ReporteAex');
                }

                if ($input['download'] == 'xls'){
                    $this->downloadXLS('administration.reports.excel.utilities', $excelDownload, 'ReporteAex');
                }
            }
            $category_id = isset($input['category_id']) ? $input['category_id'] : '';
            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];

        } else {

            $q = '';
            $category_id = '';
            $description = '';

            $products = \DB::table("products")
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->join('purchases_details', "products.id", '=', 'purchases_details.product_id')
                ->selectRaw('AVG(CAST(purchases_details.single_amount as integer)), products.name, categories.description, products.id as id, products.price')
                ->groupBy('purchases_details.single_amount', 'products.name', 'categories.description', 'products.id', 'products.price')
                ->paginate(20);

//            dd($products);
        }
        $categories = [0 => 'Seleccione..'];
        $categories += Categories::all()->pluck('description', 'id')->toArray();


        return view('administration.reports.utilities', compact('products', 'q', 'description', 'category_id', 'categories'));
    }

    public function monthlyUtilitiesReports()
    {

    }

    private function downloadXLS($view, $data, $fileName)
    {
       return Excel::create($fileName, function ($excel) use ($data, $view) {
            $excel->sheet('Reporte', function ($excel) use ($data, $view){
                $excel->loadView( $view, compact('data'));
            });
        })->export('xls');
    }

    private function downloadPDF($view, $data, $fileName)
    {
        return \PDF::loadView($view, compact('data'))->stream();
    }

}
