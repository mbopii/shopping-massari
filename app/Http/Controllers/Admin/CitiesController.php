<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriesRequest;
use App\Models\Cities;
use App\Models\Departments;
use App\Models\Categories;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($department_id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($department = Departments::find($department_id)) {
            $cities = Cities::where('department_id', $department_id)->paginate(20);

            return view('administration.cities.index', compact('cities', 'department'));
        } else {
            \Log::warning("CitiesController | Department {$department_id} not found");
            return redirect()->back()->with('error', 'Departamento no encontrado');
        }
    }


    public function create($department_id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        \Log::info("CitiesController | Group to find: {$department_id}");
        if ($department = Departments::find($department_id)){
            return view('administration.cities.create', compact( 'department'));
        }
        return redirect()->back()->with('error', 'Departamento no encontrado');
    }

    public function store($department_id, CategoriesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if ($department_id == 0 || is_null($department_id)){
            \Log::warning("CitiesController | City id cant be zero");
            return redirect()->back()->with('error', 'Debe asignar un departamento')->withInput();
        }

        $arrayToSave = ['description' => $input['description'], 'department_id' => $department_id];

        if (!$city = Cities::create($arrayToSave)){
            \Log::warning("CitiesController | Error while creating a new subCategory");
            return redirect()->back()->with('error', 'Error al intentar crear el registro')->withInput();
        }


        return redirect()->route('cities.index', $department_id)->with('success', 'Ciudad creada exitosamente');
    }

    public function edit($department_id, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$department = Departments::find($department_id)){
            \Log::warning("CitiesController | Group {$id} not found");
            return redirect()->back()->with('error', 'Grupo no encontado');
        }


        if (!$city = Cities::find($id)){
            \Log::warning("CitiesController | Category {$id} not found");
            return redirect()->back()->with('error', 'SCategoria no encontada');
        }

        return view('administration.cities.edit', compact('city', 'department'));
    }

    public function update($department_id, CategoriesRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($department_id == 0 || is_null($department_id)){
            \Log::warning("CitiesController | Group id cant be zero");
            return redirect()->back()->with('error', 'Debe asignar una categoria')->withInput();
        }

        if (!$department = Departments::find($department_id)){
            \Log::warning("CitiesController | Department {$id} not found");
            return redirect()->back()->with('error', 'Departamento no encontado');
        }


        if (!$city = Cities::find($id)){
            \Log::warning("CitiesController | City {$id} not found");
            return redirect()->back()->with('error', 'Ciudad no encontada');
        }

        $input = $request->all();

        if (!$city->update($input)){
            \Log::warning("CitiesController | Error trying to update city: {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro')->withInput();
        }


        return redirect()->route('cities.index', $department_id)->with('success', 'Ciudad actualizada exitosamente');
    }

    public function destroy($id, $subcategory_id)
    {
        $message = '';
        $error = '';

        \Log::info("Data: SubCategory: {$subcategory_id} | Category: {$id}");

        if (!\Sentinel::getUser()->hasAccess('categories.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($role = Categories::find($subcategory_id)) {
                try {
                    if (Categories::destroy($subcategory_id)) {
                        $message = 'SubCategoria eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("CategoriesController | Error deleting SubCategory: " . $e->getMessage());
                    $message = 'Error al intentar eliminar la subcategoria';
                    $error = true;
                }
            } else {
                \Log::warning("CategoriesController | SubCategory {$subcategory_id} not found");
                $message = 'SubCategoria no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

    private function uploadImage($input, $id)
    {
        if (array_key_exists('category_image', $input) && !is_null($input['category_image'])) {
            $fileName = $id . '.jpg';
            $image = $input['category_image'];
            $imageResize = Image::make($image->getRealPath());
//            $imageResize->resize(786, 378);
            $imageResize->save(public_path("admin/images/categories/{$fileName}"));
        }else{
            \Log::warning("CategoriesController | Image not found");
        }
    }

    public function newCityBySell(Request $request)
    {
        \Log::debug("CitiesController | New City from sales view");
        \Log::info(json_encode($request->all()));

        $input = $request->all();
        if ($city = Cities::create($input)){
            return json_encode(['error' => false, 'message' => 'success', 'data' => ['id' => $city->id]]);
        }else{
            return json_encode(['error' => true, 'message' => 'ocurrio un error al crear el registro']);

        }
    }



}
