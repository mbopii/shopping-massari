<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Models\Permissions;
use Illuminate\Database\QueryException;
use Illuminate\Support\MessageBag;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('permissions')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $permissions = Permissions::orderBy('id', 'desc')->paginate(20);

        $data = ['permissions' => $permissions];

        return view('administration.permissions.index', $data);
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('permissions.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $permission = \Session::get('permission', new Permissions());
        return view('administration.permissions.create', compact('permission'));
    }

    public function store(PermissionRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('permissions.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->except('_token');
        $input['permission'] = strtolower(trim($input['permission']));

        \Log::notice('Creating new Permission',
            ['action' => 'permissions.store', 'input' => $input]);

        try {

            $permission = new Permissions();
            $permission->permission = $request->get('permission');
            $permission->description = $request->get('description');

            $permission->save();

            return redirect()
                ->route('permissions.index')
                ->with('success', 'Permiso creado exitosamente.');


        } catch (QueryException $e) {

            if ($e->getCode() == 23505) {
                \Log::error('Unique permission violation.',
                    ['action' => 'permissions.store', 'input' => $input, 'message' => $e->getMessage()]);

                return redirect()->route('permissions.create')
                    ->with('permission', $permission)
                    ->withErrors((new MessageBag())->add('permission', 'El Permiso ya existe.'));
            }


            \Log::critical($e->getMessage(),
                ['action' => 'permissions.store', 'input' => $input]);
            return view('administration.permissions.create')
                ->with('permission', $permission)
                ->with('error', 'Ha ocurrido un error, intentelo mas tarde.');

        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('permissions.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$permission = Permissions::find($id)) {
            \Log::notice('Permission doesnt exists', ['action' => 'permissions.edit', 'id' => $id]);
            return redirect()
                ->route('permissions.edit')
                ->with('error', 'El Permiso no existe.');
        }

        return view('administration.permissions.edit', compact('permission'));
    }

    public function update(PermissionRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('permissions.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->except(['_token', '_method']);

        $input['permission'] = strtolower(trim($input['permission']));

        \Log::notice('Updating permission',
            ['action' => 'permissions.update', 'input' => $input]);

        if (!$permission = Permissions::find($id)) {
            \Log::notice('Permission doesnt exists', ['action' => 'permissions.update', 'id' => $id]);
            return redirect()
                ->route('permissions.edit')
                ->with('error', 'El Permiso no existe.');
        }

        $permission->fill($input);

        try {

            //TODO update all Roles and Users permissions
            $permission->save();

            return redirect()
                ->route('permissions.index')
                ->with('success', 'Permiso actualizado exitosamente.');


        } catch (QueryException $e) {

            if ($e->getCode() == 23505) {
                \Log::error('Unique permission violation.',
                    ['action' => 'permissions.udpate', 'input' => $input]);

                return redirect()->route('permissions.create')
                    ->with('permission', $permission)
                    ->withErrors((new MessageBag())->add('permission', 'El Permiso ya existe.'));
            }

            \Log::critical($e->getMessage(),
                ['action' => 'permissions.udpate', 'input' => $input]);
            return redirect()->route('permissions.create')
                ->with('permission', $permission)
                ->with('error', 'Ha ocurrido un error, intentelo mas tarde.');
        }
    }

    public function destroy($id)
    {
        $error = true;
        $message = '';
        \Log::debug("PermissionsController | Delete permissions request -> {$id}");
        try{
            if (!\Sentinel::getUser()->hasAccess('permissions.destroy')) {
                \Log::error('PermissionsController | Unauthorized access attempt',
                    ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
                $message = 'No posee los permisos para esta accion';
            } else {
                if ($permission = Permissions::find($id)) {
                    \Log::notice('PermissionsController | Deleting permission',
                        ['action' => 'permissions.delete', 'id' => $id]);

                    try {
                        if (Permissions::destroy($id)) {
                            $message = 'Permiso eliminado correctamente';
                            $error = false;
                        }
                    } catch (QueryException $e) {
                        \Log::critical($e->getMessage(),
                            ['action' => 'permissions.delete', 'id' => $id]);
                        $message = 'Error intentando eliminar el permiso, intente nuevamente';
                    }
                } else {
                    \Log::notice('PermissionsController | Permission doesnt exists', ['action' => 'permissions.delete', 'id' => $id]);
                    $message = 'Permiso no encontrado';
                }
            }
            return response()->json([
                'error' => $error,
                'message' => $message,
            ]);
        }catch (\Exception $e){
            return response()->json([
                'error' => $error,
                'message' => 'Ocurrio un error inesperado',
            ]);
        }
    }

}
