<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PurchasesRequest;
use App\Models\Products;
use App\Models\Providers;
use App\Models\PurchasesDetails;
use App\Models\PurchasesHeader;
use Illuminate\Http\Request;

class PurchasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_end']}' AND ";
            if (!is_null($input['invoice_number']) && $input['invoice_number'] != 0) $where .= "invoice_number = {$input['invoice_number']} AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')->paginate(20);
            } else {
                $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $invoice_number = isset($input['invoice_number']) ? $input['invoice_number'] : '';
            $q = $input['q'];
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $invoice_number = '';
            $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')->paginate(20);
        }


        return view('administration.purchases.index', compact('purchasesHeader', 'date_end',
            'date_start', 'q', 'invoice_number'));
    }


    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        // Get all providers
        $testProviders = Providers::all();

        if ($testProviders->isEmpty()) {
            \Log::warning("PurchasesController | Missing Providers");
            return redirect()->route('providers.create')->with('error', 'Debe crear al menos un proveedor para realizar compras');
        }

        $productsJson = Products::all();
        $providers = ["" => 'Selecione ..'];
        $providers += Providers::all()->pluck('description', 'id')->toArray();
        return view('administration.purchases.create', compact('productsJson', 'providers'));
    }

    public function store(PurchasesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if ($input['provider_id'] == 0) {
            \Log::warning("PurchasesController | Must select one provider");
            return redirect()->back()->with('error', 'Debe elegir un proveedor');
        }

        $headerArray = [
            'purchase_date' => $input['purchase_date'],
            'total_amount' => 0,
            'invoice_number' => $input['invoice_number'],
            'stamping' => $input['stamping'],
            'user_id' => \Sentinel::getUser()->id,
            'provider_id' => $input['provider_id']
        ];


        if (!$purchaseHeader = PurchasesHeader::create($headerArray)) {
            \Log::warning('PurchasesController | Error on create header');
            return redirect()->back()->with('error', 'Ocurrio un error al intentar crear el registro');
        }

        // Creamos el detalle
        $totalAmount = 0;
        for ($i = 0; $i < count($input['purchase_product_id']); $i++) {
            $purchaseDetail = [
                'purchase_header_id' => $purchaseHeader->id,
                'product_id' => $input['purchase_product_id'][$i],
                'quantity' => $input['purchase_quantity'][$i],
                'single_amount' => $input['purchase_unit_price'][$i],
                'total_amount' => $input['purchase_quantity'][$i] * $input['purchase_unit_price'][$i]
            ];

            $totalAmount += $input['purchase_quantity'][$i] * $input['purchase_unit_price'][$i];
            PurchasesDetails::create($purchaseDetail);

            $product = Products::find($input['purchase_product_id'][$i]);
            $product->quantity = $product->quantity + $input['purchase_quantity'][$i];
            $product->save();
        }

        $purchaseHeader->total_amount = $totalAmount;
        $purchaseHeader->save();
        return redirect()->route('purchases.index')->with('success', 'Compra creada exitosamente');
    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$purchase = PurchasesHeader::find($id)) {
            \Log::warning("PurchasesController | Purchase id {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view("administration.purchases.view", compact('purchase'));
    }

    public function destroy($id)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$purchase = PurchasesHeader::find($id)) {
            \Log::warning("PurchasesController | Purchase id {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        if ($purchase->canceled == true) {
            \Log::warning("PurchasesController | Invoice already canceled");
            return redirect()->back()->with('error', 'Factura ya anulada');
        }

        $purchase->canceled = true;
        $purchase->save();

        foreach ($purchase->purchaseDetails as $detail) {
            $product = Products::find($detail->product_id);
            $product->quantity = $product->quantity - $detail->quantity;
            $product->save();
        }

        return redirect()->route('purchases.index')->with("success", 'Factura anulada exitosamente');
    }
}