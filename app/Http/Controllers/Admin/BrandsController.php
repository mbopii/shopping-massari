<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brands;
use App\Models\Products;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('brands')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $brands = Brands::paginate(20);

        return view('administration.brands.index', compact('brands'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('administration.brands.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (is_null($input['description'])){
            \Log::warning("BrandsController | Missing description");
            return redirect()->back()->with('error', 'Debe escribir una descripción');
        }

        if (!$brand = Brands::create($input)){
            \Log::warning("BrandsController | Ocurrio un error al crear el registro");
            return redirect()->back()->with('error', 'Ocurrio un error al crear el registro');
        }

        $this->uploadImage($input, $brand->id);

        return redirect()->route('brands.index')->with('success', 'Registro creado exitosamennte');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($brand = Brands::find($id)){
            return view('administration.brands.edit', compact('brand'));
        }

        \Log::warning("BrandsController | Brand {$id} not found");

        return redirect()->back()->with('error', 'Marca no encontrada');
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($brand = Brands::find($id)){
            $input = $request->all();
            if (is_null($input['description'])){
                \Log::warning("BrandsController | Missing description");
                return redirect()->back()->with('error', 'Debe escribir una descripción');
            }

            if (!$brand->update($input)){
                \Log::warning("BrandsController | Error on update");
                return redirect()->back()->with('error', 'Ocurrio un  error al intentar modificar el registro');
            }

            $this->uploadImage($input, $brand->id);

            return redirect()->route('brands.index')->with('success', 'Registro modificado correctamente');

        }else{
            \Log::warning("BrandsController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

    }


    private function uploadImage($input, $id)
    {
        if (array_key_exists('brand_image', $input) && !is_null($input['brand_image'])) {
            $fileName = $id . '.jpg';
            $image = $input['brand_image'];
            $imageResize = Image::make($image->getRealPath());
            $imageResize->resize(786, 378);
            $imageResize->save(public_path("admin/images/brands/{$fileName}"));
        }else{
            \Log::warning("CategoriesController | Image not found");
        }
    }


    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('brands.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($brand = Brands::find($id)) {
                \Log::debug("BrandsController | Brand {$brand->description} found");
                if ($products = Products::where('brand_id', $brand->id)->first()){
                    \Log::warning(json_encode($products));
                    $message = 'No puede eliminar una marca con productos asignados a la misma';
                    $error = true;
                }else {
                    try {
                        if (Brands::destroy($id)) {
                            $message = 'Marca eliminada correctamente';
                            $error = false;
                        }
                    } catch (\Exception $e) {
                        \Log::warning("BrandsController | Error deleting Category: " . $e->getMessage());
                        $message = 'Error al intentar eliminar la marca';
                        $error = true;
                    }
                }
            } else {
                \Log::warning("BrandsController | Brand {$id} not found");
                $message = 'Marca no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }




}