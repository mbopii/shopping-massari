<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CasualClientsRequest;
use App\Models\Addresses;
use App\Models\CallClients;
use App\Models\Cities;
use Illuminate\Http\Request;

class CasualClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $input = $request->all();

        if (array_key_exists('q', $input)) {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "created_at::DATE = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date = '{$input['date_end']}' AND ";
            }

            if (!is_null($input['description']) ) {
                $where .= "description ILIKE '%{$input['description']}%' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $clients = CallClients::whereRaw($where)->orderBy('id', 'desc')->paginate(20);
            } else {
                $clients = CallClients::orderBy('id', 'desc')->paginate(20);
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        } else {
            $q = '';
            $date_start = '';
            $date_end = '';
            $description = '';
            $clients = CallClients::orderBy('id', 'desc')->paginate(20);

        }

        return view('administration.clients.index', compact('clients', 'q', 'date_start', 'date_end', 'description'));
    }

    public function show($id)
    {
        if ($client = CallClients::find($id)) {
            return view('administration.clients.view', compact('client'));
        } else {
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }

    public function edit($id)
    {
        if ($client = CallClients::find($id)) {
            $cities = ["" => 'Seleccione ..'];
            $cities += Cities::all()->pluck('description', 'id')->toArray();
            return view('administration.clients.edit', compact('client', 'cities'));
        } else {
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }

    public function update($id, CasualClientsRequest $request)
    {
        $input = $request->all();
        if ($client = CallClients::find($id)) {
            if ($client->update($input)) {
                $address = Addresses::find($client->addresses[0]->id);
                $address->update($input);
                return redirect()->route('clients.index')->with('success', 'Cliente actualizado exitosamente');
            } else {
                return redirect()->back()->with('error', 'Ocurrio un error al intentar actualizar el cliente');
            }
        } else {
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }

    public function destroy($id)
    {
        $error = true;
        $message = '';
        \Log::debug("CasualClientsController | Delete casual client request -> {$id}");
        try {
            if ($client = CallClients::find($id)) {
                \Log::notice('CasualClientsController | Deleting client',
                    ['action' => 'clients.delete', 'id' => $id]);

                try {
                    if (CallClients::destroy($id)) {
                        $message = 'Cliente eliminado correctamente';
                        $error = false;
                    }
                } catch (QueryException $e) {
                    \Log::critical($e->getMessage(),
                        ['action' => 'products.delete', 'id' => $id]);
                    $message = 'Error intentando eliminar el cliente, intente nuevamente';
                }
            }
            return response()->json([
                'error' => $error,
                'message' => $message,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error' => $error,
                'message' => 'Ocurrio un error inesperado',
            ]);
        }
    }
}
