<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubCategoriesRequest;
use App\Models\Categories;
use App\Models\SubCategories;
use Illuminate\Http\Request;

class SubCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('subcategories')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $subcategories = SubCategories::paginate(20);

        return view('administration.subcategories.index', compact('subcategories'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('subcategories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $categories = Categories::all()->pluck('description', 'id')->toArray();
        $categories = [0 => 'Seleccione .. '] + $categories;

        return view('administration.subcategories.create', compact('categories'));
    }

    public function store(SubCategoriesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('subcategories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if ($input['category_id'] == 0){
            \Log::warning("SubCategoriesController | Category id cant be zero");
            return redirect()->back()->with('error', 'Debe asignar una categoria')->withInput();
        }

        if (!SubCategories::create($input)){
            \Log::warning("SubCategoriesController | Error while creating a new subCategory");
            return redirect()->back()->with('error', 'Error al intentar crear el registro')->withInput();
        }

        return redirect()->route('subcategories.index')->with('success', 'Subcategoria creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('subcategories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$subCategory = SubCategories::find($id)){
            \Log::warning("SubCategoriesController | SubCategory {$id} not found");
            return redirect()->back()->with('error', 'SubCategoria no encontada');
        }

        $categories = Categories::all()->pluck('description', 'id')->toArray();
        $categories = [0 => 'Seleccione .. '] + $categories;

        return view('administration.subcategories.edit', compact('categories', 'subCategory'));
    }

    public function update(SubCategoriesRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('subcategories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$subCategory = SubCategories::find($id)){
            \Log::warning("SubCategoriesController | SubCategory {$id} not found");
            return redirect()->back()->with('error', 'SubCategoria no encontada');
        }

        $input = $request->all();

        if ($input['category_id'] == 0){
            \Log::warning("SubCategoriesController | Category id cant be zero");
            return redirect()->back()->with('error', 'Debe asignar una categoria')->withInput();
        }

        if (!$subCategory->update($input)){
            \Log::warning("SubCategoriesController | Error trying to update subcategory_id: {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro')->withInput();
        }

        return redirect()->route('subcategories.index')->with('success', 'SubCategoria actualizada exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('subcategories.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($role = SubCategories::find($id)) {
                try {
                    if (SubCategories::destroy($id)) {
                        $message = 'SubCategoria eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("SubCategoriesController | Error deleting SubCategory: " . $e->getMessage());
                    $message = 'Error al intentar eliminar la subcategoria';
                    $error = true;
                }
            } else {
                \Log::warning("SubCategoriesController | SubCategory {$id} not found");
                $message = 'SubCategoria no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
