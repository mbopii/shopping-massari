<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProvidersRequest;
use App\Models\Providers;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('providers')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $providers = Providers::paginate(20);

        return view('administration.providers.index', compact('providers'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('administration.providers.create');
    }

    public function store(ProvidersRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!$provider = Providers::create($input)){
            \Log::warning("CategoriesContrller | Error while trying to save new provider");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('providers.index')->with('success', 'Proveedor creado exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$provider = Providers::find($id)){
            \Log::warning("ProvidersController | Provider {$id} not found");
            return redirect()->back()->with('error', 'Proveedor no encontrada');
        }

        return view('administration.providers.edit', compact('provider'));
    }

    public function update(ProvidersRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Providers::find($id)){
            \Log::warning("ProvidersController | Provider {$id} not found");
            return redirect()->back()->with('error', 'Proveedor no encontrada');
        }

        $input = $request->all();
        if (!$category->save($input)){
            \Log::warning("ProvidersController | Error on update provider id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }
        return redirect()->route('providers.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('providers.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($role = Providers::find($id)) {
                try {
                    if (Providers::destroy($id)) {
                        $message = 'Proveedor eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("ProvidersController | Error deleting Category: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el proveedor';
                    $error = true;
                }
            } else {
                \Log::warning("ProvidersController | Category {$id} not found");
                $message = 'Proveedor no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

}