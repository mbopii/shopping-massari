<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrdersDetails;
use App\Models\OrdersHeaders;
use App\Models\Products;
use App\Models\PurchasesHeader;
use App\Models\User;

class AdminHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Products::count('*');
        $orders = OrdersHeaders::count('*');
        $purchases = PurchasesHeader::count('*');
        $users = User::count('*');

        $mostSale = OrdersDetails::selectRaw('sum(quantity) as p_quantity, product_id')
            ->join('orders_header', 'orders_header.id', '=', 'orders_details.order_header_id')
            ->whereRaw('orders_header.status_id != 2')
            ->groupBy('product_id')
            ->orderBy('p_quantity', 'desc')
            ->limit(10)
            ->get();

        if ($mostSale->isEmpty()){
            $mostSale = 'Sin ventas significativas';
        }

        $mostVisited = Products::selectRaw('name, views')
            ->orderBy('views', 'desc')
            ->limit(10)
            ->get();

        if ($mostVisited->isEmpty()){
            $mostVisited = 'Sin ventas significativas';
        }

        $mostProvider = PurchasesHeader::selectRaw('sum(total_amount::int) as p_sub_total, provider_id')
            ->where('canceled', false)
            ->groupBy('provider_id')
            ->orderBy('p_sub_total', 'desc')
            ->limit(10)
            ->get();

        if ($mostProvider->isEmpty()){
            $mostProvider = 'Sin Compras';
        }

        return view('administration.welcome', compact('products', 'orders', 'purchases', 'users',
            'mostSale', 'mostProvider', 'mostVisited'));
    }
}