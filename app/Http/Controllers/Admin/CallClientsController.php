<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Addresses;
use App\Models\CallClients;
use App\Models\Cities;
use Illuminate\Http\Request;


class CallClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',
            [
                'except' => [
                    'activate'
                ]
            ]);
    }

    public function addNewClientsForSale(Request $request)
    {
        \Log::debug("ClientsController | New Client from sales view");
        \Log::info(json_encode($request->all()));

        $input = $request->all();
        $input['user_id'] = 1;
        if ($client = CallClients::create($input)){
            $input['client_id'] = $client->id;
            // We need to save the address
            if ($address = Addresses::create($input)){
                return json_encode(['error' => false, 'message' => 'success', 'data' => ['id' => $client->id]]);
            }
        }else{
            return json_encode(['error' => true, 'message' => 'ocurrio un error al crear el registro']);
        }
    }

    public function searchClient($id)
    {
        if (is_null($id)){
            \Log::warning("ClientsControllers | Missing id to search");
            return json_encode(['error' => true]);
        }

        $client = CallClients::find($id);
        if (!is_null($client)){
            $clientAddress = Addresses::where("user_id", 1)
                ->where('client_id', $id)->first();
            $myCity = $clientAddress->cities->description;
            $clientAddressJson = json_encode($clientAddress);
            $client->addresses = $clientAddressJson;
            $client->my_city = $myCity;
            return json_encode(['client' => $client, 'error' => false]);
        }else{
            return json_encode(['error' => true, 'code' => 10]);
        }
    }
}
