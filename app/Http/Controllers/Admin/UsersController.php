<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Addresses;
use App\Models\Categories;
use App\Models\Departments;
use App\Models\OrdersHeaders;
use App\Models\Permissions;
use App\Models\Products;
use App\Models\Role;
use App\Models\User;
use App\Models\Cities;
use App\Models\UserTaxInfo;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;
use Mail;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',
            [
                'except' => [
                    'activate'
                ]
            ]);
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('users')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $users = User::with('roles')->orderBy('id', 'desc')->paginate(20);

        return view('administration.users.index', compact('users'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $roleArray = Role::all()->pluck('description', 'id')->toArray();
        $roleArray = [0 => 'Seleccione .. '] + $roleArray;

        $data = ['rolesList' => $roleArray];

        return view('administration.users.create', $data);
    }

    public function store(UserRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();

        if (User::where("email", $input['email'])->first()){
            \Log::warning("UsersController | Email already exist");
            return redirect()->back()->with("error", 'Este mail ya existe')->withInput();
        }

        if (is_null($input['password']) || is_null($input['password_confirmation'])){
            \Log::warning('UsersController | Passwords dont match');
            return redirect()->back()->with('error', 'Debe introducir una contraseña')->withInput();
        }else{
            if ($input['password'] != $input['password_confirmation']){
                \Log::warning('UsersController | Passwords dont match');
                return redirect()->back()->with('error', 'Las contraseñas no coinciden')->withInput();
            }
        }
        $credentials = [
            'username' => $input['username'],
            'email' => $input['email'],
            'description' => $input['description'],
            'password' => $input['password'],
            'telephone' => $input['telephone'],
            'address' => $input['address'],
            'birthday' => $input['birthday'],
            'idnum' => $input['idnum']
        ];
        \Log::debug("UserController | Credentials Array: " . json_encode($credentials));
        if ($user = \Sentinel::registerAndActivate($credentials)) {
            $expectedPermissions = array_pull($input, 'permissions');
            $expectedPermissions = empty($expectedPermissions) ? [] : $expectedPermissions;

            foreach ($expectedPermissions as $p => $v) {
                if ($v['inherited'] === '0') {
                    $user->addPermission($p, isset($v['state']) ? filter_var($v['state'],
                        FILTER_VALIDATE_BOOLEAN) : false);
                }
            }

            if (!$user->save()) {
                \Log::error('Cant update Users Permissions.', $input);
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', 'Problemas al actualizar registro.');
            }
        }

        $expectedRoles = $input['role_id'];
        $expectedRoles = explode(',', $expectedRoles);

        if (!empty($expectedRoles)) {
            $user->roles()->attach($expectedRoles);
            Log::info('Roles agregados a Usuario: ' . $user->username, $expectedRoles);
        }

        return redirect()->route('users.index')
            ->with('success', 'Usuario creado exitosamente.');
    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.show')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'no posee permisos para realizar esta accion.');
        }

        if ($user = User::find($id)) {
            Log::info("User {$id} find");
            return view('administration.users.show', compact('user'));
        }

        Log::warning("User not found");
        return redirect()->back()->with('error', 'Usuario no encontrado');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($user = User::with('roles')->find($id)) {

            $processedPermissions = $user->getProcessedPermissions()->all();

            $permissions = Permissions::orderBy('permission')->get();

            foreach ($permissions as $permission) {
                if (array_key_exists($permission->permission, $processedPermissions)) {
                    $permission->has = $processedPermissions[$permission->permission]['state'];
                    $permission->inherited = $processedPermissions[$permission->permission]['inherited'];
                } else {
                    $permission->has = null;
                    $permission->inherited = null;
                }
            }

            $rolesList = Role::all();
            $roleArray = [];
            $roleArray[0] = 'Seleccione ..';
            foreach ($rolesList as $role) {
                $roleArray[$role->id] = $role->description;
            }

            $data = [
                'user' => $user,
                'rolesList' => $roleArray
            ];
            return view('administration.users.edit', $data);
        }

        return redirect()->back()->with('error', 'Usuario no encontrado.');

    }

    public function update(UserRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        // Get User with Roles
        if ($user = User::with('roles')->find($id)) {
            // Get the form input
            $input = $request->all();

            // Get the form input expected permissions
            // Permissions Rules:
            //  if state is null and inherited is 0 -> revoked permission user specific
            //  if state is true and inherited is 0 -> grant permission user specific
            //  if state is true and inherited is 1 -> already granted permission role inherited
            //  if state is null and inherited is 1 -> already revoked permission role inherited
            //  if state is null and inherited is null -> permission set in neither role nor user
            $expectedPermissions = array_pull($input, 'permissions');
            $expectedPermissions = empty($expectedPermissions) ? [] : $expectedPermissions;

            foreach ($expectedPermissions as $p => $v) {
                if (!isset($v['inherited']) OR $v['inherited'] === '0') {
                    $user->updatePermission($p, isset($v['state']) ? filter_var($v['state'], FILTER_VALIDATE_BOOLEAN) : false, true);
                }
            }

            if (!$user->save()) {
                \Log::error('Cant update Users Permissions.', $input);
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', 'Problemas al actualizar registro.');
            }
            \Log::debug('Permissions Saved!. ', $input);
            $credentials = [
                'username' => $input['username'],
                'email' => $input['email'],
                'description' => $input['description'],
                'telephone' => $input['telephone'],
                'address' => $input['address'],
                'birthday' => $input['birthday'],
                'idnum' => $input['idnum']

            ];


            if (!is_null($input['password'])){
                if ($input['password'] != $input['password_confirmation']){
                    \Log::warning('UsersController | Passwords dont match');
                    return redirect()->back()->with('error', 'Las contraseñas no coinciden')->withInput();
                }
                $credentials['password'] = $input['password'];
            }
            // Get array of the User's current Roles IDs
            $currentRoles = [];

            if (!$user->roles->isEmpty())
                $currentRoles = explode(',', $user->roles->implode('id', ','));

            // Get arry of the User's expected Roles IDs
            $expectedRoles = explode(',', $request->get('role_id'));

            // Prepare array of Roles to detach from User
            if (!empty($currentRoles))
                $toDetachRoles = array_diff($currentRoles, $expectedRoles);

            if (!empty($toDetachRoles)) {
                $user->roles()->detach($toDetachRoles);
                Log::info('Roles eliminados de Usuario: ' . $user->username, $toDetachRoles);
            }

            // Prepare array of Roles to attach to User
            $toAttachRoles = array_diff($expectedRoles, $currentRoles);

            if (!empty($toAttachRoles)) {
                $user->roles()->attach($toAttachRoles);
                Log::info('Roles agregados a Usuario: ' . $user->username, $toAttachRoles);
            }


            // Update User with credentials
            //$user->update($credentials);
            $user = \Sentinel::update(\Sentinel::findById($id), $credentials);
            \Log::info("User {$user->username} updated successfully");
            return redirect()
                ->route('users.index')
                ->with('success', 'Usuario actualizado.');
        }

        return redirect()
            ->route('users.index')
            ->with('error', 'Error al actualizar el Usuario.');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('users.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        } else {
            if ($user = User::find($id)) {
                if (User::destroy($id) !== false) {
                    \Log::info('User destroy.', $user->toArray());
                    $message = 'Usuario eliminado correctamente';
                    $error = false;
                } else {
                    \Log::warning("Error while trying to destroy user: {$id}");
                    $message = 'Error al intentar eliminar el usuario';
                    $error = true;
                }
            } else {
                \Log::warning("User {$id} not found");
                $message = 'Usuario no encontrado';
                $error = true;
            }
        }
        $response = [
            'error' => $error,
            'message' => $message
        ];
        \Log::debug('Sending response. ' . json_encode($response));
        return response()->json($response);
    }

    public function activate($id, $code)
    {
        $user = \Sentinel::findUserById($id);
        \Log::info("UserController | New Activation Request");
        if (!\Activation::complete($user, $code)) {
            \Log::warning("User not activated {$user->username}");
            return redirect()->route('login.page')->withErrors('Codigo de activacion inválido o expirado');
        }

        return redirect()->route('login.page')->with('success', 'Cuenta de Usuario activada');
    }

    public function frontUserShow($id)
    {
        if ($user = User::find($id)) {
            $categories = Categories::all();
            Log::info("User {$id} find");
            // Get all cities
            $cities = Cities::all(['description', 'id']);
            $citiesJson = json_encode($cities);
            return view('front.users.show', compact('user', 'citiesJson', 'categories'));
        }

        Log::warning("UsersController | User {$id} not found");
        return redirect()->back()->with('error', 'Usuario no encontrado');
    }

    public function frontUserAddressShow($id)
    {
        if ($user = User::find($id)) {
            $categories = Categories::all();
            Log::info("User {$id} find");
            // Get all cities
            $cities = Cities::all(['description', 'id']);
            $citiesJson = json_encode($cities);
            $arrayAddress = [];
            $i = 0;

            foreach ($user->addresses as $a) {
                $arrayAddress[$i]['id'] = $a->id;
                $arrayAddress[$i]['description'] = $a->address;
                $arrayAddress[$i]['address2'] = $a->address2;
                $arrayAddress[$i]['city'] = $a->cities->description;
                $arrayAddress[$i]['primary'] = $a->primary_address;
                $arrayAddress[$i]['number'] = $a->number;
                $arrayAddress[$i]['references'] = $a->references;
                $i++;
            }
            // Traemos las ciudades
            $cities = Cities::all();
            $j = 0;
            foreach ($cities as $c) {
                $citiesArray[$j]['id'] = $c->id;
                $citiesArray[$j]['description'] = $c->description;
                $j++;
            }

            $departments = Departments::all();
            $j = 0;
            foreach ($departments as $d) {
                $departmentsArray[$j]['id'] = $d->id;
                $departmentsArray[$j]['description'] = $d->description;
                $j++;
            }


            return view('front.users.addresses', compact('user', 'citiesJson', 'arrayAddress',
                'citiesArray', 'categories', 'departmentsArray'));

        }


        Log::warning("UsersController | User {$id} not found");
        return redirect()->back()->with('error', 'Usuario no encontrado');
    }

    public function frontUserUpdate(Request $request, $id)
    {
        if ($user = User::find($id)) {
            $input = $request->all();
            if (isset($input['password'])) {
                if ($input['password'] != $input['password_confirm']) {
                    \Log::warning("UsersController | Password and password confirmation doesn't
                     match for user {$id}");
                    return redirect()->back()->with('error', 'Las contraseñas no coinciden');
                }
                if (!\Sentinel::update(\Sentinel::findById($id), $input)) {
                    \Log::warning("UsersController | User {$id} not updated");
                    return redirect()->back()->with('error', 'Usuario no actualizado');
                }
            } else {
                unset($input['password']);
                if (!$user->update($input)) {
                    \Log::warning("UsersController | User {$id} not updated");
                    return redirect()->back()->with('error', 'Usuario no actualizado');
                }
            }

            \Log::info("UsersController | User {$id} updated correctly");
            return redirect()->back()->with('success', 'Usuario modificado exitosamente');

        } else {
            \Log::warning("UsersController | Front User {$id} not found");
            return redirect()->back()->with('error', 'Usuario no encontrado');
        }

    }

    public function updateFrontUserAddress($id, Request $request)
    {
        $input = $request->all();

        $address = Addresses::find($id);

        if ($address) {
            if (array_key_exists('new_city_id', $input) && $input['new_city_id'] != 0) {
                $city = Cities::find($input['new_city_id']);
                $city_name = $city->description;
                $address->city_id = $input['new_city_id'];
            } else {
                $city_name = '---';
            }
            $address->address = $input['address'];
            $address->address2 = $input['address2'];
            $address->number = $input['number'];
            $address->references = $input['references'];

            if (!is_null($address['latitude']) && $address['latitude'] != "") {
                $address->longitude = $input['longitude'];
                $address->latitude = $input['latitude'];
            } else {
                $address->longitude = 0;
                $address->latitude = 0;
            }
            $address->update();

            return response()->json(['error' => false, 'message' => 'Success', 'city' => $city_name]);
        } else {
            return response()->json(['error' => true, 'message' => 'No existe la dirección']);
        }
    }

    public function newFrontUserAddress(Request $request)
    {
        $input = $request->all();

        if (array_key_exists('new_city_id', $input) && $input['new_city_id'] != 0) {
            if (array_key_exists('new_city_description', $input) && !is_null($input['new_city_description'])){
                // Guardamos nueva ciudad al departamento elegido
                $department = Departments::find($input['department_id']);
                $newCityArray = ['description' => $input['new_city_description'], 'department_id' => $department->id];
                $city = Cities::create($newCityArray);
            }else{
                $city = Cities::find($input['new_city_id']);
            }
            $city_name = $city->description;
            if (array_key_exists('longitude-new_map', $input)) {
                $user_id = \Sentinel::getUser()->id;
                $addressArray = [
                    "address" => $input['address'],
                    "city_id" => $city->id,
                    "number" => $input['number'],
                    "references" => $input['references'],
                    "latitude" => $input['latitude-new_map'],
                    "longitude" => $input['longitude-new_map'],
                    'user_id' => $user_id
                ];
                $address = new Addresses($addressArray);
                $address->save();
                return response()->json(['error' => false, 'message' => 'Success', 'city' => $city_name, 'id' => $address->id]);
            } else {
                return response()->json(['error' => true, 'message' => 'Debe seleccionar su ubicacion en el mapa']);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Debe elegir una ciudad']);
        }
    }

    public function destroyFrontUserAddress($id)
    {
        if (Addresses::find($id)) {
            if (Addresses::destroy($id)) {
                return response()->json(['error' => false, 'message' => 'success ' . $id]);
            } else {
                return response()->json(['error' => true, 'message' => 'Ocurrio un error al modificar el registro']);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Direccion no encontrada']);
        }
    }

    public function myPurchases($id)
    {  
        $user = User::find($id);
        $categories = Categories::all();
        $ordersHeader = OrdersHeaders::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        if ($ordersHeader->isEmpty()) {
            \Log::warning("UserController | User {$user->description} dosent have any purchases");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }

        return view('front.users.purchases', compact('ordersHeader', 'categories'));
    }

    public function pendingPayments($id)
    {
        $user = User::find($id);
        $categories = Categories::all();
        $ordersHeader = OrdersHeaders::where('user_id', $user->id)->where('status_id', 1)->get();
        $ordersCount = OrdersHeaders::where('user_id', $user->id)->where('status_id', 1)->count();

        if ($ordersHeader->isEmpty()) {
            \Log::warning("UserController | User {$user->description} dosent have any purchases");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }

        return view('front.users.pending_payments', compact('ordersHeader', 'categories', 'ordersCount'));
    }

    public function myPurchasesDetails($header_id)
    {
        $ordersHeader = OrdersHeaders::find($header_id);
        if (!$ordersHeader) {
            \Log::warning("UserController | Purchase not found");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }
        $categories = Categories::all();
        return view('front.users.purchases_details', compact('ordersHeader', 'categories'));
    }

    public function searchClient($idnum)
    {
        if (is_null($idnum)) {
            \Log::warning("ClientsControllers | Missing idnum to search");
            return json_encode(['error' => true]);
        }

        $client = User::where('idnum', $idnum)->first();
        if (!is_null($client)) {
            $addresses = $client->addresses;
            $invoices = $client->taxInfo;
            return json_encode(['error' => false, 'client' => $client, 'addresses' => $addresses, 'invoices' => $invoices]);
        } else {
            return json_encode(['error' => true, 'code' => 10]);
        }
    }

    public function saveAdminUser(Request $request)
    {

    }

    public function myPurchasesTracking($header_id)
    {
        $ordersHeader = OrdersHeaders::find($header_id);
        if (!$ordersHeader) {
            \Log::warning("UserController | Purchase not found");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }
        $categories = Categories::all();
        $aexURL = env('AEX_SERVICE');
        $aexRequestArray = [
            "clave_publica" => env('AEX_PUBLIC_KEY'),
            "codigo_sesion" => 123,
            "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123)
        ];

        try {

            $client = new Client(['base_uri' => $aexURL]);
            $response = $client->request('POST', 'autorizacion-acceso/generar',
                ['json' => $aexRequestArray, 'verify' => false]);
            $aexAuthRequestJson = $response->getBody()->getContents();

            $aexAuthRequest = json_decode($aexAuthRequestJson);
            \Log::info("OrdersController | {$aexAuthRequestJson}");

            if ($aexAuthRequest->codigo == 0) {
                $aexTrackingArray = [
                    "clave_publica" => env('AEX_PUBLIC_KEY'),
                    "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                    "numero_guia" => $ordersHeader->aex_tracking_id,
                    "codigo_operacion" => 1
                ];
                $response = $client->request('POST', 'envios/tracking',
                    ['json' => $aexTrackingArray, 'verify' => false]);
                $aexTrackingRequestJson = $response->getBody()->getContents();

                $aexTrackingRequest = json_decode($aexTrackingRequestJson);
                \Log::info("OrdersController | {$aexTrackingRequestJson}");
                if ($aexAuthRequest->codigo == 0) {
                    $trackingInfo = $aexTrackingRequest->datos;
                    return view('front.users.tracking', compact('ordersHeader', 'categories', 'trackingInfo'));

                } else {

                }
            }
        } catch (\Exception $e) {
            \Log::warning('Error while attempting to call pagando');
            \Log::warning($e->getMessage());
        }
    }

    public function uploadPurchasePicture(Request $request, $header_id)
    {
        $input = $request->all();
        $ordersHeader = OrdersHeaders::find($header_id);
        if (!$ordersHeader) {
            \Log::warning("UserController | Purchase not found");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }
        if (array_key_exists('invoice_file', $input) && !is_null($input['invoice_file'])) {
            if (!file_exists(public_path("/front/img/purchases/"))) {
                \File::makeDirectory(public_path("/front/img/purchases/"));
            }
            $fileName = $header_id . '.jpg';
            $input['invoice_file']->move(public_path("/front/img/purchases/"), $fileName);

            return redirect()->back()->with('success', 'Archivo subido exitosamente');
        }else{
            \Log::warning("UsersController | Missing Image");
            return redirect()->back()->with("error", 'Debe seleccionar una imagen');
        }

    }
}
