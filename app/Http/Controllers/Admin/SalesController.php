<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Addresses;
use App\Models\CallClients;
use App\Models\Cities;
use App\Models\Departments;
use App\Models\OrdersDetails;
use App\Models\OrdersHeaders;
use App\Models\PaymentForms;
use App\Models\Products;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "orders_header.created_at::date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "orders_header.created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "orders_header.created_at::date = '{$input['date_end']}' AND ";
            }

            if (!is_null($input['payment_method_id']) && $input['payment_method_id'] != 0) {
                $where .= "payment_method_id = {$input['payment_method_id']} AND ";
            }

            if (!is_null($input['user_description'])) {
                $where .= "users.description ILIKE '%{$input['user_description']}%' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $salesHeader = \DB::table('orders_header')
                    ->join('users', 'users.id', '=', 'orders_header.user_id')
                    ->join('payments_method', 'payments_method.id', '=', 'orders_header.payment_method_id')
                    ->join('statuses', 'statuses.id', '=', 'orders_header.status_id')
                    ->selectRaw('orders_header.id as id, orders_header.purchase_date, orders_header.order_amount,
                payments_method.description as payment_form, users.description as client, statuses.description as status,
                 orders_header.created_at
                as created_at, orders_header.client_id as client_id, orders_header.status_id, orders_header.delivery_cost')
                    ->orderBy('orders_header.id', 'desc')
                    ->paginate(20);
            } else {
                $salesHeader = \DB::table('orders_header')
                    ->join('users', 'users.id', '=', 'orders_header.user_id')
                    ->join('payments_method', 'payments_method.id', '=', 'orders_header.payment_method_id')
                    ->join('statuses', 'statuses.id', '=', 'orders_header.status_id')
                    ->selectRaw('orders_header.id as id, orders_header.purchase_date, orders_header.order_amount,
                payments_method.description as payment_form, users.description as client, statuses.description as status,
                 orders_header.created_at
                as created_at, orders_header.client_id as client_id, orders_header.status_id, orders_header.delivery_cost')
                    ->whereRaw($where)
                    ->orderBy('orders_header.id', 'desc')
                    ->paginate(20);
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $payment_method_id = isset($input['payment_method_id']) ? $input['payment_method_id'] : '';
            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $description = '';
            $payment_method_id = '';
            $salesHeader = \DB::table('orders_header')
                ->join('users', 'users.id', '=', 'orders_header.user_id')
                ->join('payments_method', 'payments_method.id', '=', 'orders_header.payment_method_id')
                ->join('statuses', 'statuses.id', '=', 'orders_header.status_id')
                ->selectRaw('orders_header.id as id, orders_header.purchase_date, orders_header.order_amount,
                payments_method.description as payment_form, users.description as client, statuses.description as status,
                 orders_header.created_at
                as created_at, orders_header.client_id as client_id, orders_header.status_id, orders_header.delivery_cost')
                ->orderBy('orders_header.id', 'desc')
                ->paginate(20);
        }

        $paymentForms = [0 => 'Seleccione..'];
        $paymentForms += PaymentForms::all()->pluck('description', 'id')->toArray();

        return view('administration.sales.index', compact('salesHeader', 'date_start', 'date_end',
            'q', 'description', 'payment_method_id', 'paymentForms'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $productsJson = Products::all();

        $clientsJson = json_encode(CallClients::selectRaw("description || ' - ' || tax_code as description, id")->get());
        $citiesJson = json_encode(Cities::selectRaw("description, id")->get());

        $paymentMethods = json_encode(PaymentForms::all(['description', 'id'])->except(2));

        $departmentsJson = json_encode(Departments::all());

        return view('administration.sales.create', compact("productsJson", 'citiesJson', 'clientsJson', 'paymentMethods', 'departmentsJson'));
    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$sale = OrdersHeaders::find($id)) {
            \Log::warning("SalesController | Sale id {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view("administration.sales.view", compact('sale'));
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        $client_user = CallClients::find($input['client_idnum']);
        $address = Addresses::where('user_id', 1)->where('client_id', $client_user->id)->first();
        $orderHeaderArray = [
            'user_id' => 1,
            'address_id' => $address->id,
            'status_id' => '1', // En proceso,
            'purchase_date' => date('Y-m-d'),
            'order_amount' => 0,
            'client_id' => $client_user->id,
            'reception_time_from' => $input['reception_time_from'],
            'reception_time_to' => $input['reception_time_to'],
            'payment_method_id' => $input['payment_form_id'],
            'reception_date' => $input['reception_date'],
        ];
        $amount = 0;
        $package = [];
        if ($orderHeader = OrdersHeaders::create($orderHeaderArray)) {
            // Guardamos detalle de factura
            for ($i = 0; $i < count($input['product_id']); $i++) {
                if ($input['new_price'][$i] == 0) {
                    $product = Products::find($input['product_id'][$i]);
                    for ($j = 1; $j <= $input['quantity'][$i]; $j++) {
                        $package[] = [
                            "peso" => $product->weight, // KG
                            "largo" => $product->height, // CM
                            "alto" => $product->width, // CM
                            "ancho" => $product->length, // CM,
                            "valor" => $product->price,
                        ];
                    }
                    $detailsArray = [
                        'order_header_id' => $orderHeader->id,
                        'product_id' => $product->id,
                        'unit_price' => $product->price,
                        'quantity' => $input['quantity'][$i],
                        'sub_total' => ($input['quantity'][$i] * $product->price),
                    ];

                    if ($input['discount'][$i] = !0 && $input['quantity'][$i] >= $product->discount_quantity && $input['quantity'][$i] < $product->discount_quantity2) {
                        $discount = $product->price * $input['quantity'][$i] * $product->discount_percentage / 100;
                        $detailsArray['discount'] = true;
                        $detailsArray['total_discount'] = $discount;
                    } else if ($input['discount'][$i] = !0 && $input['quantity'][$i] >= $product->discount_quantity2 && $product->discount_quantity2 > 0){
                        $discount = $product->price * $input['quantity'][$i] * $product->discount_percentage2 / 100;
                        $detailsArray['discount'] = true;
                        $detailsArray['total_discount'] = $discount;
                    }
                    OrdersDetails::create($detailsArray);
                    $amount += $input['quantity'][$i] * $product->price;
                } else {
                    $product = Products::find($input['product_id'][$i]);
                    for ($j = 1; $j <= $input['quantity'][$i]; $j++) {
                        $package[] = [
                            "peso" => $product->weight, // KG
                            "largo" => $product->height, // CM
                            "alto" => $product->width, // CM
                            "ancho" => $product->length, // CM,
                            "valor" => $input['new_price'][$i],
                        ];
                    }

                    $detailsArray = [
                        'order_header_id' => $orderHeader->id,
                        'product_id' => $product->id,
                        'unit_price' => $input['new_price'][$i],
                        'quantity' => $input['quantity'][$i],
                        'sub_total' => ($input['quantity'][$i] * $input['new_price'][$i]),
                    ];
                    OrdersDetails::create($detailsArray);
                    $amount += $input['quantity'][$i] * $input['new_price'][$i];
                }
            }

            $orderHeader->order_amount = $amount;
            $orderHeader->save();

            if ($input['inside_purchase'] == 0) {
                if (!is_null($address->cities->aex_code)) {
                    $aexURL = env('AEX_SERVICE');
                    $aexRequestArray = [
                        "clave_publica" => env('AEX_PUBLIC_KEY'),
                        "codigo_sesion" => 123,
                        "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
                    ];

                    try {
                        // Generamos Código Token para autenticación
                        $client = new Client(['base_uri' => $aexURL]);
                        $response = $client->request('POST', 'autorizacion-acceso/generar',
                            ['json' => $aexRequestArray, 'verify' => false]);
                        $aexAuthRequestJson = $response->getBody()->getContents();

                        $aexAuthRequest = json_decode($aexAuthRequestJson);
                        \Log::info("OrdersController | {$aexAuthRequestJson}");

                        if ($aexAuthRequest->codigo == 0) {
                            // Realizamos la solicitud
                            $aexGetServiceRequestArray = [
                                "clave_publica" => env("AEX_PUBLIC_KEY"),
                                "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                                "origen" => "PY1107",
                                "destino" => $address->cities->aex_code,
                                "codigo_operacion" => $orderHeader->id,
                                "paquetes" => $package,
                                "codigo_tipo_carga" => "P",
                            ];
                            \Log::warning("StoreController | Calculator request -> " . json_encode($aexGetServiceRequestArray));
                            // Solicitamos Servicio a AEX
                            $response = $client->request('POST', 'envios/solicitar_servicio', ['json' => $aexGetServiceRequestArray, 'verify' => false]);
                            $aexGetServiceResponseJson = $response->getBody()->getContents();
                            $aexGetServiceResponse = json_decode($aexGetServiceResponseJson);
                            \Log::info("OrdersController | {$aexGetServiceResponseJson}");
                            if ($aexGetServiceResponse->codigo == 0) {
                                // Guardamos los datos importantes para confirmar dependiendo del método de pago
                                $conditions = $aexGetServiceResponse->datos->condiciones;
                                foreach ($conditions as $condition) {
                                    // Envio estandar puerta a puerta
                                    if ($condition->id_tipo_servicio == 3) {
                                        \Log::info("OrdersController | Store AEX Data");
                                        $delivery = explode('.', $condition->costo_flete);
                                        $orderHeader->delivery_cost = $delivery[0];
                                        $orderHeader->aex_request_id = $aexGetServiceResponse->datos->id_solicitud;
                                        $orderHeader->aex_service_type = 3;
                                        // Pago en efectivo, solicitamos cobro del mismo
                                        if ($orderHeader->payment_method_id == 1) {
                                            foreach ($condition->adicionales as $service) {
                                                if ($service->id_adicional == 3) {
                                                    $cobro = explode('.', $service->costo);
                                                    $orderHeader->aex_collect_amount = $cobro[0];
                                                    break;
                                                }
                                            }
                                        }
                                        $orderHeader->save();
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        \Log::warning('Error while attempting to call pagando');
                        \Log::warning($e->getMessage());
                    }

                } else {
                    // Delivery Externo
                    $orderHeader->delivery_cost = 30000;
                    $orderHeader->save();

                }
            } else {
                // Compra Interna
                $orderHeader->delivery_cost = 0;
                $orderHeader->save();
            }

            return redirect()->route('sales.index')->with('success', 'Venta realizada exitosamente
            - Debe confirmar la orden al recibir el pago');
        } else {
            \Log::warning("OrdersController | Error while creating a new order");
            return redirect()->back()->with('error', 'Error al crear el registro');
        }
    }

    public function confirmOrder($id)
    {
        $ordersHeader = OrdersHeaders::find($id);
        if (!$ordersHeader) {
            \Log::warning("SalesController | Purchase not found");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }

        if ($ordersHeader->status_id != 1) {
            \Log::warning('SalesController | Order already closed');
            return redirect()->back()->with('error', 'La orden ya fue creada');
        }

        foreach ($ordersHeader->details as $detail) {
            $product = Products::find($detail->product_id);
            if ($detail->quantity > 1) {
                for ($i = 1; $i <= $detail->quantity; $i++) {
                    $package[] = [
                        "peso" => $product->weight, // KG
                        "largo" => $product->height, // CM
                        "alto" => $product->width, // CM
                        "ancho" => $product->length, // CM,
                        "valor" => $product->price,
                    ];
                }
            } else {
                $package[] = [
                    "peso" => $product->weight, // KG
                    "largo" => $product->height, // CM
                    "alto" => $product->width, // CM
                    "ancho" => $product->length, // CM,
                    "valor" => $product->price,
                ];
            }

            if ($product->complex_product) {
                foreach ($product->complexItems as $item) {
                    $complexItem = Products::find($item->involve_product_id);
                    $complexItem->quantity = $complexItem->quantity - ($item->quantity * $detail->quantity);
                    $complexItem->save();
                }
            } else {
                // Update Stock
                $product->quantity = $product->quantity - $detail->quantity;
                $product->save();
            }

        }

        // Enviar a AEX la solicitud para enviar el pedido
        $aexURL = env('AEX_SERVICE');
        $aexRequestArray = [
            "clave_publica" => env('AEX_PUBLIC_KEY'),
            "codigo_sesion" => 123,
            "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
        ];

        if ($ordersHeader->client_id) {
            $user = CallClients::find($ordersHeader->client_id);
        } else {
            $user = User::find($ordersHeader->user_id);
        }
        $address = Addresses::find($ordersHeader->address_id);
        if (is_null($ordersHeader->reception_date)) {
            $date = date("Y-m-d", time() + 172800);
        } else {
            $date = $ordersHeader->reception_date;
        }
        if ($address->cities->aex_code) {
            try {

                $client = new Client(['base_uri' => $aexURL]);
                $response = $client->request('POST', 'autorizacion-acceso/generar',
                    ['json' => $aexRequestArray, 'verify' => false]);
                $aexAuthRequestJson = $response->getBody()->getContents();

                $aexAuthRequest = json_decode($aexAuthRequestJson);
                \Log::info("SalesController | {$aexAuthRequestJson}");
                if ($aexAuthRequest->codigo == 0) {
                    // Realizamos la confirmacion
                    // Guardamos los datos importantes para confirmar dependiendo del método de pago

                    $aexConfirmServiceArray = [
                        "clave_publica" => env("AEX_PUBLIC_KEY"),
                        "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                        "id_solicitud" => $ordersHeader->aex_request_id,
                        'id_tipo_servicio' => $ordersHeader->aex_service_type,
                        // Direccion de retiro de mercaderías
                        "pickup" => [
                            "codigo" => "2",// COdigo direccion
                            "calle_principal" => "Valois Rivarola",
                            "numero_casa" => '951',
                            "calle_transversal_1" => "Tte. Francisco Cusmanich",
                            "codigo_ciudad" => "PY0000",
                            "telefono_movil" => '0972162339',
                            'latitud' => '-25.279472',
                            'longitud' => '-57.610626',
                            'referencias' => ''
                        ],
                        // Direccion de entrega de mercaderías
                        "entrega" => [
                            "codigo" => $address->id, // COdigo direccion
                            "calle_principal" => $address->address,
                            "numero_casa" => $address->number,
                            "calle_transversal_1" => $address->address2,
                            "codigo_ciudad" => $address->cities->aex_code,
                            "disponible_desde" => $date . " " . $ordersHeader->reception_time_from,
                            "disponible_hasta" => $date . " " . $ordersHeader->reception_time_to,
                        ],
                        "destinatario" => [
                            "codigo" => $user->id,
                            "tipo_documento" => "CIP",
                            "numero_documento" => $user->idnum,
                            "nombre" => $user->description,
                            "email" => isset($user->email) ? $user->email : 'admin@massari.com.py',
                            "telefonos" => [
                                [
                                    "numero" => $user->telephone,
                                ],
                            ],
                        ],

                    ];

                    if ($ordersHeader->payment_method_id == 1) {
                        \Log::info("SalesController | Cobramos delivery");
                        $aexConfirmServiceArray["adicionales"] = [3];
                        $aexConfirmServiceArray["codigo_forma_pago"] = "D";
                    } else {
                        \Log::info("SalesController | No Cobramos Delivery");
                        $aexConfirmServiceArray["codigo_forma_pago"] = "C";
                        $aexConfirmServiceArray["adicionales"] = [];
                    }
                    \Log::warning("SalesController | Confirmation request -> " . json_encode($aexConfirmServiceArray));
                    $response = $client->request('POST', 'envios/confirmar_servicio', ['json' => $aexConfirmServiceArray, 'verify' => false]);
                    $aexConfirmServiceResponseJson = $response->getBody()->getContents();
                    $aexConfirmServiceResponse = json_decode($aexConfirmServiceResponseJson);
                    \Log::info("SalesController | {$aexConfirmServiceResponseJson}");
                    if ($aexConfirmServiceResponse->codigo == 0) {
                        // Guardamos los datos importantes para confirmar dependiendo del método de pago
                        $ordersHeader->aex_tracking_id = $aexConfirmServiceResponse->datos->numero_guia;
                        $ordersHeader->save();
                    }

                } else {
                    // Delivery Externo
                    $ordersHeader->delivery_cost = 30000;
                    $ordersHeader->save();
                }
            } catch (\Exception $e) {
                \Log::warning('Error while attempting to call pagando');
                \Log::warning($e->getMessage());
                return redirect()->back()->with('error', 'Ocurrio un error inesperado, intente nuevamente');
            }
        }

        $data = ['orderHeader' => $ordersHeader];
        $mailList = [
            '0' => ['email' => $ordersHeader->clients->email, 'description' => $ordersHeader->clients->description],
            '1' => ['email' => 'rojalogo@gmail.com', 'description' => 'Rodrigo Lopez'],
            '2' => ['email' => 'aminmathias@gmail.com', 'description' => 'Amin Daher'],
            '3' => ['email' => 'massaripy@gmail.com', 'description' => 'MassariPY'],
        ];

        \Mail::send('front.mails.confirm', $data, function ($message) use ($mailList) {
            foreach ($mailList as $list) {
                $message->to($list['email'], $list['description'])
                    ->subject('Massari - Orden Completada');
            }

        });

        $ordersHeader->status_id = 3;
        $ordersHeader->save();

        return redirect()->route('sales.index')->with('success', 'Pedido confirmado con exito');
    }

    public function cancelOrder($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($order = OrdersHeaders::find($id)) {
            $order->status_id = 2; // canceled
            $order->save();

            return redirect()->route("sales.index")->with('success', 'Orden Cancelada Exitosamente');
        } else {
            \Log::warning("OrdersController | Order {$id} not foudn");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }

    }

    public function deliveryCost(Request $request)
    {
        $input = $request->all();

        \Log::debug(json_encode($input));
        $collection = false;
        for ($i = 0; $i < count($input['products']); $i++) {
            $product = Products::find($input['products'][$i]);
            for ($j = 1; $j <= $input['quantities'][$i]; $j++) {
                $price = 0;
                if ($product->discount_quantity2 == 0 || is_null($product->discount_quantity2)){
                    if ($input['quantities'][$i] >= $product->discount_quantity){
                        \Log::info("SalesController | Discount 1");
                        $price = $product->price - ($product->price * $product->discount_percentage / 100);
                    }else{
                        $price = $product->price;
                    }
                }elseif ($input['quantities'][$i] >= $product->discount_quantity && $input['quantities'][$i] < $product->discount_quantity2){
                    \Log::info("SalesController | Discount 1");
                    $price =  $product->price - ($product->price * $product->discount_percentage / 100);
                }elseif($input['quantities'][$i] >= $product->discount_quantity2 && $product->discount_quantity2 > 0){
                    \Log::info("SalesController | Discount 2");
                    $price =  $product->price - ($product->price * $product->discount_percentage2 / 100);
                }else{
                    $price = $product->price;
                }

                \Log::debug("SalesController | Product: {$input['products'][$i]} - Quantity: {$input['quantities'][$i]}");
                $package[] = [
                    "peso" => $product->weight, // KG
                    "largo" => $product->height, // CM
                    "alto" => $product->width, // CM
                    "ancho" => $product->length, // CM,
                    "valor" => $price,
                ];
            }
        }

        $address = Addresses::where('user_id', 1)->where('client_id', $input['client_id'])->first();

        if (!is_null($address->cities->aex_code)) {
            try {
                $aexURL = env('AEX_SERVICE');
                $aexRequestArray = [
                    "clave_publica" => env('AEX_PUBLIC_KEY'),
                    "codigo_sesion" => 123,
                    "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
                ];
                $client = new Client(['base_uri' => $aexURL]);
                $response = $client->request('POST', 'autorizacion-acceso/generar', ['json' => $aexRequestArray, 'verify' => false]);
                $aexAuthRequestJson = $response->getBody()->getContents();

                $aexAuthRequest = json_decode($aexAuthRequestJson);
                \Log::info("StoreController | {$aexAuthRequestJson}");

                if ($aexAuthRequest->codigo == 0) {
                    // Calculamos dependiendo de la ciudad elegida
                    $aexCalculateRequestArray = [
                        "clave_publica" => env("AEX_PUBLIC_KEY"),
                        "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                        "origen" => "PY0000",
                        "destino" => $address->cities->aex_code,
                        "paquetes" => $package,
                        "codigo_tipo_carga" => "P",
                    ];
                    \Log::warning("StoreController | Calculator request -> " . json_encode($aexCalculateRequestArray));
                    $response = $client->request('POST', 'envios/calcular', ['json' => $aexCalculateRequestArray, 'verify' => false]);
                    $aexCalculateRequestJson = $response->getBody()->getContents();
                    $aexCalculateRequest = json_decode($aexCalculateRequestJson);
                    \Log::info("StoreController | {$aexCalculateRequestJson}");
                    if ($aexCalculateRequest->codigo == 0) {
                        foreach ($aexCalculateRequest->datos as $datos) {
                            if ($datos->id_tipo_servicio == 3) {
                                if (is_array($datos->adicionales)) {
                                    foreach ($datos->adicionales as $adicionales) {
                                        if ($adicionales->id_adicional == 3) {
                                            $collection = true;
                                        }
                                    }
                                }

                            }
                        }
                        $arrayAddress['delivery'] = [
                            'cost' => $datos->costo_servicio,
                            'time' => $datos->tiempo_entrega . " Hs.",
                            'service' => $datos->denominacion,
                            'text' => 'Pagas tu envio al recibir tu paquete',
                            'collection' => $collection,
                        ];
                    } else {
                        $arrayAddress['delivery'] = [
                            'cost' => '30000',
                            'time' => 24 . " Hs.",
                            'service' => 'Servicio Tercerizado',
                        ];
                    }
                }

            } catch (\Exception $e) {
                \Log::warning('Error while attempting to call pagando');
                \Log::warning($e->getMessage());
            }
        } else {
            $arrayAddress['delivery'] = [
                'cost' => '30000',
                'time' => 24 . " Hs.",
                'service' => 'Servicio Tercerizado',
            ];
        }

        \Log::debug(json_encode($arrayAddress));
        return response()->json($arrayAddress);
    }

    public function cancelConfirmOrder($id)
    {
        if ($order = OrdersHeaders::find($id)) {
            $order->status_id = 2; // canceled
            $order->save();

            //Devolvemos el stock (El unico lugar donde hacemos esto)
            foreach ($order->details as $detail) {
                $product = Products::find($detail->product_id);
                if ($product->complex_product) {
                    foreach ($product->complexItems as $item) {
                        $complexItem = Products::find($item->involve_product_id);
                        $complexItem->quantity = $complexItem->quantity + ($item->quantity * $detail->qty);
                        $complexItem->save();
                    }
                } else {
                    // Update Stock
                    $product->quantity = $product->quantity + $detail->quantity;
                    $product->save();
                }

            }

            return redirect()->route("sales.index")->with('success', 'Orden Cancelada Exitosamente');
        } else {
            \Log::warning("SalesControler | Order {$id} not foudn");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }
    }

    public function acceptDepositOrder($id)
    {
        $ordersHeader = OrdersHeaders::find($id);
        if (!$ordersHeader) {
            \Log::warning("UserController | Purchase not found");
            return redirect()->back()->with('error', 'Usted aún no ha realizado ninguna compra');
        }

        $ordersHeader->status_id = 3;
        $ordersHeader->save();

        // Enviar a AEX la solicitud para enviar el pedido
        $aexURL = env('AEX_SERVICE');
        $aexRequestArray = [
            "clave_publica" => env('AEX_PUBLIC_KEY'),
            "codigo_sesion" => 123,
            "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123),
        ];

        if (is_null($ordersHeader->client_id)) {
            $user = User::find($ordersHeader->user_id);
            $destinatario = [
                "codigo" => $user->id,
                "tipo_documento" => "CIP",
                "numero_documento" => $user->idnum,
                "nombre" => $user->description,
                "email" => $user->email,
                "telefonos" => [
                    [
                        "numero" => $user->telephone,
                    ],
                ],
            ];
        } else {
            $user = CallClients::find($ordersHeader->client_id);
            $destinatario = [
                "codigo" => $user->id,
                "tipo_documento" => "CIP",
                "numero_documento" => $user->idnum,
                "nombre" => $user->description,
                "email" => 'admin@massari.com.py',
                "telefonos" => [
                    [
                        "numero" => $user->telephone,
                    ],
                ],
            ];
        }

        $address = Addresses::find($ordersHeader->address_id);

        if (!is_null($address->cities->aex_code)) {
            try {

                $client = new Client(['base_uri' => $aexURL]);
                $response = $client->request('POST', 'autorizacion-acceso/generar',
                    ['json' => $aexRequestArray, 'verify' => false]);
                $aexAuthRequestJson = $response->getBody()->getContents();

                $aexAuthRequest = json_decode($aexAuthRequestJson);
                \Log::info("OrdersController | {$aexAuthRequestJson}");

                foreach ($ordersHeader->details as $detail) {
                    $product = Products::find($detail->product_id);
                    if ($detail->quantity > 1) {
                        for ($i = 1; $i <= $detail->quantity; $i++) {
                            $package[] = [
                                "peso" => $product->weight, // KG
                                "largo" => $product->height, // CM
                                "alto" => $product->width, // CM
                                "ancho" => $product->length, // CM,
                                "valor" => $product->price,
                            ];
                        }
                    } else {
                        $package[] = [
                            "peso" => $product->weight, // KG
                            "largo" => $product->height, // CM
                            "alto" => $product->width, // CM
                            "ancho" => $product->length, // CM,
                            "valor" => $product->price,
                        ];
                    }
                }
                if ($aexAuthRequest->codigo == 0) {
                    // Realizamos la solicitud
                    $aexGetServiceRequestArray = [
                        "clave_publica" => env("AEX_PUBLIC_KEY"),
                        "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                        "origen" => "PY0000",
                        "destino" => $address->cities->aex_code,
                        "codigo_operacion" => $ordersHeader->id,
                        "paquetes" => $package,
                        "codigo_tipo_carga" => "P",
                    ];
                    \Log::warning("StoreController | Calculator request -> " . json_encode($aexGetServiceRequestArray));
                    $response = $client->request('POST', 'envios/solicitar_servicio', ['json' => $aexGetServiceRequestArray, 'verify' => false]);
                    $aexGetServiceResponseJson = $response->getBody()->getContents();
                    $aexGetServiceResponse = json_decode($aexGetServiceResponseJson);
                    \Log::info("OrdersController | {$aexGetServiceResponseJson}");
                    if ($aexGetServiceResponse->codigo == 0) {
                        // Guardamos los datos importantes para confirmar dependiendo del método de pago
                        $cost = explode(".", $aexGetServiceResponse->datos->condiciones->tipo_servicio_3->costo_flete);
                        $ordersHeader->delivery_cost = $cost[0];
                        $ordersHeader->aex_request_id = $aexGetServiceResponse->datos->id_solicitud;
                        $ordersHeader->aex_service_type = $aexGetServiceResponse->datos->condiciones->tipo_servicio_3->id_tipo_servicio;
                        $ordersHeader->save();

                        $aexConfirmServiceArray = [
                            "clave_publica" => env("AEX_PUBLIC_KEY"),
                            "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                            "id_solicitud" => $aexGetServiceResponse->datos->id_solicitud,
                            'id_tipo_servicio' => $aexGetServiceResponse->datos->condiciones->tipo_servicio_3->id_tipo_servicio,
                            'codigo_forma_pago' => 'C',
                            // Direccion de retiro de mercaderías
                            "pickup" => [
                                "codigo" => "2", // COdigo direccion
                                "calle_principal" => "Valois Rivarola",
                                "numero_casa" => '951',
                                "calle_transversal_1" => "Tte. Francisco Cusmanich",
                                "codigo_ciudad" => "PY0000",
                                "telefono_movil" => '0972162339',
                                'latitud' => '-25.279472',
                                'longitud' => '-57.610626',
                                'referencias' => '',
                            ],
                            // Direccion de entrega de mercaderías
                            "entrega" => [
                                "codigo" => $address->id, // COdigo direccion
                                "calle_principal" => $address->address,
                                "numero_casa" => $address->number,
                                "calle_transversal_1" => $address->address2,
                                "codigo_ciudad" => "PY0000",
                                "disponible_desde" => $ordersHeader->reception_time_from,
                                "disponible_hasta" => $ordersHeader->reception_time_to,
                            ],
                            "destinatario" => $destinatario,
                        ];
                        \Log::warning("StoreController | Confirmation request -> " . json_encode($aexConfirmServiceArray));
                        $response = $client->request('POST', 'envios/confirmar_servicio', ['json' => $aexConfirmServiceArray, 'verify' => false]);
                        $aexConfirmServiceResponseJson = $response->getBody()->getContents();
                        $aexConfirmServiceResponse = json_decode($aexConfirmServiceResponseJson);
                        \Log::info("OrdersController | {$aexConfirmServiceResponseJson}");
                        if ($aexConfirmServiceResponse->codigo == 0) {
                            // Guardamos los datos importantes para confirmar dependiendo del método de pago
                            $ordersHeader->aex_tracking_id = $aexConfirmServiceResponse->datos->numero_guia;
                            $ordersHeader->save();
                            return redirect()->back()->with('success', 'Orden Procesada Exitosamente');
                        }
                    }
                }
            } catch (\Exception $e) {
                \Log::warning('Error while attempting to call pagando');
                \Log::warning($e->getMessage());
            }

        }

        return redirect()->back()->with('success', 'Orden Procesada Exitosamente');
    }
}
