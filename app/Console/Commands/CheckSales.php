<?php

namespace App\Console\Commands;

use App\Models\OrdersHeaders;
use Illuminate\Console\Command;

class CheckSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if sales are finish';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = OrdersHeaders::where('payment_method_id', '<>', 3)
            ->where('status_id', '=', '1')
            ->whereRaw('client_id is NULL')
            ->get();

        if ($orders->isEmpty()){
            \Log::info("CheckSales | No changes has to be made");
            return;
        }
        foreach ($orders as $orderHeader) {
            \Log::warning("Check Sales - Order Time: {$orderHeader->created_at} | Server Time: " . date('Y-m-d H:i') );
            if (((strtotime(date('Y-m-d H:i')) - strtotime($orderHeader->created_at)) >= 1800)){
                // Pass more than 1.30hrs, rolling back
                $orderHeader->status_id = 2; // Orden Cancelada - Dejamos un histórico para reportes
                $orderHeader->save();
            }
        }
    }
}
