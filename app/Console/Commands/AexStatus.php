<?php

namespace App\Console\Commands;

use App\Models\OrdersHeaders;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class AexStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aex:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update aex order status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $aexOrders = OrdersHeaders::whereRaw('aex_tracking_id is not null and status_id = 3')->get();

        if ($aexOrders->isNotEmpty()) {
            try {
                $aexURL = env('AEX_SERVICE');
                $aexRequestArray = [
                    "clave_publica" => env('AEX_PUBLIC_KEY'),
                    "codigo_sesion" => 123,
                    "clave_privada" => md5(env('AEX_PRIVATE_KEY') . 123)
                ];

                $client = new Client(['base_uri' => $aexURL]);
                $response = $client->request('POST', 'autorizacion-acceso/generar',
                    ['json' => $aexRequestArray, 'verify' => false]);
                $aexAuthRequestJson = $response->getBody()->getContents();

                $aexAuthRequest = json_decode($aexAuthRequestJson);
                \Log::info("OrdersController | {$aexAuthRequestJson}");

                if ($aexAuthRequest->codigo == 0) {
                    foreach ($aexOrders as $order) {
                        // Consultamos con aex por el estado de un producto y actualizamos esta info en la tabla de ordenes
                        $aexTrackingArray = [
                            "clave_publica" => env('AEX_PUBLIC_KEY'),
                            "codigo_autorizacion" => $aexAuthRequest->codigo_autorizacion,
                            "numero_guia" => $order->aex_tracking_id,
                            "codigo_operacion" => 1
                        ];
                        $response = $client->request('POST', 'envios/tracking',
                            ['json' => $aexTrackingArray, 'verify' => false]);
                        $aexTrackingRequestJson = $response->getBody()->getContents();

                        $aexTrackingRequest = json_decode($aexTrackingRequestJson);
                        \Log::info("OrdersController | {$aexTrackingRequestJson}");
                        if ($aexAuthRequest->codigo == 0) {
                            if (!empty($aexTrackingRequest->datos)) {
                                for ($i = 0; $i <= count($aexTrackingRequest->datos) - 1; $i++) {
                                    if ($aexTrackingRequest->datos[$i]->codigo_estado == "E") {
                                        \Log::info("AexStatus | Order delivered");
                                        $order->status_id = 4;
                                        $order->save();
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                \Log::warning('Error while attempting to call pagando');
                \Log::warning($e->getMessage());
            }
        }
    }
}
