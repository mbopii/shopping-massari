<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('administration')->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Permission Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::resource('permissions', 'Admin\PermissionsController');

    /*
    |--------------------------------------------------------------------------
    | Roles Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::resource('roles', 'Admin\RolesController');

    /*
    |--------------------------------------------------------------------------
    | Users Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::post('users/admin/store', ['as' => 'users.admin.store', 'uses' => 'Admin\UsersController@saveAdminUser']);
    Route::get('users/search/{idnum}', 'Admin\UsersController@searchClient');
    Route::resource('users', 'Admin\UsersController');

    /*
    |--------------------------------------------------------------------------
    | Login Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::get('login', ['as' => 'admin.login.page', 'uses' => 'Auth\LoginController@adminLoginPage']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@loginAttempt']);

    /*
    |--------------------------------------------------------------------------
    | Categories Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('categories', 'Admin\CategoriesController');

    /*
    |--------------------------------------------------------------------------
    | SubCategories Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('subcategories', 'Admin\SubCategoriesController');

    /*
    |--------------------------------------------------------------------------
    | Products Routes
    |--------------------------------------------------------------------------
    */
    Route::post('products/delete_image', 'Admin\ProductsController@destroyImage');
    Route::get("products/single/{id}", ['as' => 'products.single', 'uses' => 'Admin\ProductsController@getSingleProduct']);
    Route::delete('products/complex/remove/{complex_id}/{product_id}', ['as' => 'products.complex.remove',
        'uses' => 'Admin\ProductsController@removeComplex']);
    Route::resource('products', 'Admin\ProductsController');

    /*
    |--------------------------------------------------------------------------
    | Purchases Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('purchases', 'Admin\PurchasesController');

   /*
   |--------------------------------------------------------------------------
   | Providers Routes
   |--------------------------------------------------------------------------
   */
    Route::resource('providers', 'Admin\ProvidersController');

    /*
    |--------------------------------------------------------------------------
    | Sales Routes
    |--------------------------------------------------------------------------
    */
    Route::get('sales/confirm/deposit/{id}', ['as' => 'sales.confirm.deposits', 'uses' => 'Admin\SalesController@acceptDepositOrder']);
    Route::get('sales/cancel/confirm/{id}', ['as' => 'sales.confirm.cancel', 'uses' => 'Admin\SalesController@cancelConfirmOrder']);
    Route::get('sales/confirm/{id}', ['as' => 'sales.confirm', 'uses' => 'Admin\SalesController@confirmOrder']);
    Route::post('sales/delivery/', 'Admin\SalesController@deliveryCost');
    Route::get('sales/cancel/{id}', ['as' => 'sales.cancel', 'uses' => 'Admin\SalesController@cancelOrder']);
    Route::resource('sales', 'Admin\SalesController');

    /*
    |--------------------------------------------------------------------------
    | Brands Routes
    |--------------------------------------------------------------------------
    */
    Route::get('clients/search/{id}', 'Admin\CallClientsController@searchClient');
    Route::post('clients/add_sales', 'Admin\CallClientsController@addNewClientsForSale');
    Route::resource('clients', 'Admin\CallClientsController');

    /*
    |--------------------------------------------------------------------------
    | Reports Routes
    |--------------------------------------------------------------------------
    */
    Route::get('reports/utilities/monthly', ['as' => 'reports.utilities.monthly', 'uses' => 'Admin\ReportsController@monthlyUtilitiesReports']);
    Route::get('reports/utilities', ['as' => 'reports.utilities', 'uses' => 'Admin\ReportsController@utilitiesReports']);
    Route::get('reports/aex/get_delivery/{id}', ['as' => 'reports.aex.get_delivery', 'uses' => 'Admin\ReportsController@collectAexPayments']);
    Route::get('reports/aex/pay_collect/{id}', ['as' => 'reports.aex.pay_collect', 'uses' => 'Admin\ReportsController@payAexCollect']);
    Route::get('reports/aex', ['as' => 'reports.aex', 'uses' => 'Admin\ReportsController@aexReports']);
    Route::get('reports/stock', ['as' => 'reports.stock', 'uses' => 'Admin\ReportsController@generalStock']);
    Route::get('reports/sales', ['as' => 'reports.sales', 'uses' => 'Admin\ReportsController@sales']);
    Route::resource('reports', 'Admin\ReportsController');

    /*
   |--------------------------------------------------------------------------
   | Brands Routes
   |--------------------------------------------------------------------------
   */
    Route::resource('brands', 'Admin\BrandsController');

    /*
    |--------------------------------------------------------------------------
    | Departments Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('departments', 'Admin\DepartmentsController');

    /*
    |--------------------------------------------------------------------------
    | Cities Routes
    |--------------------------------------------------------------------------
    */
    Route::post('cities/add_sales', 'Admin\CitiesController@newCityBySell');
    Route::resource('departments/{department_id}/cities', 'Admin\CitiesController');

    /*
    |--------------------------------------------------------------------------
    | Admin Home Routes
    |--------------------------------------------------------------------------
    */
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Admin\AdminHomeController@index']);

    /*
    |--------------------------------------------------------------------------
    | Casual Clients Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('casual_clients', 'Admin\CasualClientsController');

});

/*
|--------------------------------------------------------------------------
| Login & Activation Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::get('register', ['as' => 'front.register.show', 'uses' => 'Front\Auth\RegisterController@showRegisterPage']);
Route::post('register', ['as' => 'front.register.do', 'uses' => 'Front\Auth\RegisterController@doFrontRegister']);
Route::get('register/activate/{id}/{code}', ['as' => 'front.users.activate', 'uses' => 'Front\Auth\RegisterController@activateFrontUser']);
Route::get('login', ['as' => 'login.page', 'uses' => 'Auth\LoginController@loginPage']);
Route::post('login', ['as' => 'login.do', 'uses' => 'Auth\LoginController@loginAttempt']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('recovery', ['as' => 'show.recovery.password', 'uses' => 'Auth\LoginController@showResetPasswordPage']);
Route::post('recovery', ['as' => 'do.recovery.password', 'uses' => 'Auth\LoginController@resetPasswordRequest']);
Route::get('restore/{id}/{code}', ['as' => 'reset.password.page', 'uses' => 'Auth\LoginController@resetPasswordPage']);
Route::post('restore', ['as' => 'reset.password.do', 'uses' => 'Auth\LoginController@resetPassword']);


/*
|--------------------------------------------------------------------------
| Login & Activation Routes | Socialite Implementation
|--------------------------------------------------------------------------
*/
Route::get('login/facebook', ['as' => 'login.facebook', 'uses' => 'Front\FacebookController@redirectToFacebook']);
Route::get('login/facebook/callback', ['as' => 'login.facebook.callback', 'uses' => 'Front\FacebookController@handleFacebookCallback']);

/*
|--------------------------------------------------------------------------
| Front User Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::get("purchases/{id}/deposit", ['as' => 'orders.pre.deposit', 'uses' => 'Front\OrdersController@preFinishPurchase']);
Route::get('pending/{id}', ['as' => 'front.users.pending', 'uses' => 'Admin\UsersController@pendingPayments']);
Route::get('purchases/{header_id}/tracking', ['as' => 'front.users.purchases.tracking', 'uses' => 'Admin\UsersController@myPurchasesTracking']);
Route::post('purchases/{id}/upload', ['as' => 'front.users.purchases.upload', 'uses' => 'Admin\UsersController@uploadPurchasePicture']);
Route::get('purchases/{id}/details', ['as' => 'front.users.purchases.details', 'uses' => 'Admin\UsersController@myPurchasesDetails']);
Route::get('purchases/{id}', ['as' => 'front.users.purchases', 'uses' => 'Admin\UsersController@myPurchases']);
Route::get('profile/{id}', ['as' => 'front.user.show', 'uses' => 'Admin\UsersController@frontUserShow']);
Route::patch('profile/{id}/update', ['as' => 'front.user.update', 'uses' => 'Admin\UsersController@frontUserUpdate']);
Route::post('address/{id}/destroy', ['as' => 'front.address.destroy', 'uses' => 'Admin\UsersController@destroyFrontUserAddress']);
Route::post('address/create', ['as' => 'front.address.create', 'uses' => 'Admin\UsersController@newFrontUserAddress']);
Route::post('address/{id}/update', ['as' => 'front.address.update', 'uses' => 'Admin\UsersController@updateFrontUserAddress']);
Route::get('addresses/{id}', ['as' => 'front.user.addresses', 'uses' => 'Admin\UsersController@frontUserAddressShow']);
Route::post('profile/{id}/address', ['as' => 'front.user.address', 'uses' => 'Admin\UsersController@updateFrontUserAddress']);

/*
|--------------------------------------------------------------------------
| Front Various Index
|--------------------------------------------------------------------------
*/
Route::get('about', ['as' => 'front.about', 'uses' => 'Front\HomeController@aboutUs']);
Route::get('faq', ['as' => 'front.faq', 'uses' => 'Front\HomeController@showFAQ']);
Route::get('contact', ['as' => 'front.contact.show', 'uses' => 'Front\HomeController@showContact']);
Route::post('contact', ['as' => 'front.contact.send', 'uses' => 'Front\HomeController@sendContact']);



/*
|--------------------------------------------------------------------------
| Front Store Index
|--------------------------------------------------------------------------
*/
Route::get('/', ['as' => 'index', 'uses' => 'Front\HomeController@index']);
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'Front\HomeController@changeLanguage']);
/*
|--------------------------------------------------------------------------
| Cart routes
|--------------------------------------------------------------------------
*/
Route::get('cart/now/{product_id}/{quantity}', ['as' => 'cart.now', 'uses' => 'Front\StoreController@buyNow']);
Route::get('cart/getAddress/{id}', ['as' => 'cart.get.address', 'uses' => 'Front\StoreController@getAddress']);
Route::get("cart/checkout",['as' => 'cart.checkout', 'uses' => 'Front\StoreController@checkout']);
Route::get('cart/destroy/{rowId}', ['as' => 'cart.destroy', 'uses' => 'Front\StoreController@destroyOneCartItem']);
Route::get('cart/destroy', ['as' => 'cart.destroy.all', 'uses' => 'Front\StoreController@destroyAllCart']);
Route::get('cart/{id}/{count}/add', ['as' => 'cart.add', 'uses' => 'Front\StoreController@addToCart']);
Route::get('/store/cart', ['as' => 'cart.show', 'uses' => 'Front\StoreController@showCart']);
/*
|--------------------------------------------------------------------------
| Store routes
|--------------------------------------------------------------------------
*/
Route::get('store/{brand_id}/brand', ['as' => 'store.filter.brand', 'uses' => 'Front\StoreController@filterByBrand']);
Route::get('store/{category_id}/category', ['as' => 'store.filter.category', 'uses' => 'Front\StoreController@filterByCategory']);
Route::get('store/filter/{rule}', ['as' => 'store.rules', 'uses' => 'Front\StoreController@filterByRules']);
Route::get('store/search', ['as' => 'store.search', 'uses' => 'Front\StoreController@searchInStore']);
Route::resource('store', 'Front\StoreController');

/*
|--------------------------------------------------------------------------
| WishList routes
|--------------------------------------------------------------------------
*/
Route::get('wish/{product_id}/remove', ['as' => 'front.wish.remove', 'uses' => 'Front\WishListController@remove']);
Route::get('wish/{product_id}/add', ['as' => 'front.wish.add', 'uses' => 'Front\WishListController@add']);
Route::resource('wish', 'Front\WishListController');

/*
|--------------------------------------------------------------------------
| Orders routes
|--------------------------------------------------------------------------
*/
Route::get('store/cancel/{id}', ['as' => 'orders.cancel', 'uses' => 'Front\OrdersController@cancelOrder']);
Route::get('store/finish/{id}', ['as' => 'orders.finish', 'uses' => 'Front\OrdersController@finishPurchase']);
Route::resource('orders', 'Front\OrdersController');