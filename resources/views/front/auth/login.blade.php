@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li class="active"><a href="#">Inicio</a></li>
                        </ul>
                        <span class="crumb-name">Inicio de Sesion</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="personal-information">
                        <h4 class="my-account-title">Inicio de Sesión</h4>
                        <div class="personal-information-box">
                            <form action="{{ route('login') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Email</label>
                                            <input type="text" name="email">
                                        </div>
                                        <div class="pib-item">
                                            <label>Password</label>
                                            <input type="password" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 ">
                                        <div class="pib-save-change">
                                            <button type="submit">Entrar</button>
                                            <button id="facebook_button2"><i class="fa fa-facebook-square"></i> Facebook
                                            </button>

                                        </div>
                                        <div class="pib-save-change">
                                        </div>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br class="br_login">
                <div class="col-lg-8 col-md-6 col-xs-12">
                    <div class="personal-information">
                        <h4 class="my-account-title">Registro</h4>
                        <div class="personal-information-box">
                            <form action="{{ route('front.register.do') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Email</label>
                                            <input type="text" name="email" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Nombre</label>
                                            <input type="text" name="description" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Cedula de Identidad</label>
                                            <input type="text" name="idnum" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Teléfono</label>
                                            <input type="text" name="telephone" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Calle 1</label>
                                            <input type="text" name="address1" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Calle 2</label>
                                            <input type="text" name="address2" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Nro. de Casa</label>
                                            <input type="text" name="number" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label for="">Ciudad</label>
                                            <select name="city_id"
                                                    class="form-control">
                                                @foreach ($citiesArray as $c)
                                                    <option value="{{ $c['id'] }}">{{ $c['description'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>No esta tu ciudad? Agregala Aquí</label>
                                            <input type="text" name="new_city_description" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Departamento</label>
                                            <select name="department_id"
                                            class="form-control">
                                        @foreach ($departmentsArray as $d)
                                            <option value="{{ $d['id'] }}">{{ $d['description'] }}</option>
                                        @endforeach
                                    </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Password</label>
                                            <input type="password" name="password" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Confirmar Password</label>
                                            <input type="password" name="password_confirm" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pib-save-change">
                                            <button type="submit">Registrarme</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('front.js')
    <script>
        $('input[name=number]').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('input[name=telephone]').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        
        $('input[name=idnum]').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('#facebook_button2').click(function (e) {
            console.log("Facebook Login");
            e.preventDefault();
            window.location.href = "{{ route('login.facebook') }}";
        });
    </script>
@endsection