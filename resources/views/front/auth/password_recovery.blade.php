@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li class="active"><a href="#">Inicio</a></li>
                        </ul>
                        <span class="crumb-name">Inicio de Sesion</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-8 col-xs-12">
                    <div class="personal-information">
                        <h4 class="my-account-title">Recupera tu cuenta</h4>
                        <div class="personal-information-box">
                            <form action="{{ route('do.recovery.password') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Ingresa el email con el cual te registraste</label>
                                            <input type="text" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 ">
                                        <div class="pib-save-change">
                                            <button type="submit">Recuperar</button>
                                        </div>
                                        <div class="pib-save-change">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection