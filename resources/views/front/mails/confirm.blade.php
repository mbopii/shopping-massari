<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="{{"https://fonts.googleapis.com/css?family=Muli:300,400,700,900"}}" rel="stylesheet">
    <!-- Facebook sharing information tags -->
    <meta property="og:title" content="*|MC:SUBJECT|*"/>

    <title>Massari</title>
    <style type="text/css">
        /* Client-specific Styles */
        #outlook a {
            padding: 0;
        }

        /* Force Outlook to provide a "view in browser" button. */
        body {
            width: 100% !important;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        /* Force Hotmail to display emails at full width */
        body {
            -webkit-text-size-adjust: none;
        }

        /* Prevent Webkit platforms from changing default text sizes. */

        /* Reset Styles */
        body {
            margin: 0;
            padding: 0;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table td {
            border-collapse: collapse;
        }

        #backgroundTable {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        /* Template Styles */

        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */

        /**
        *




        @tab




        Page










        {{--* @section background color--}}









        *




                                                @tip




                                                Set the background color for your email. You may want to choose one that matches your company's branding.
                                                                                            *




                                                @theme




                                                page
                                                                                            */
        body, #backgroundTable {
            /*




        @editable     */
            background-color: #ededed;
        }

        /**
        *




        @tab




        Page










        {{--* @section email border--}}









        *




                                                @tip




                                                Set the border for your email.
                                                                                            */
        {{--#templateContainer{--}}
            {{--/*@editable*/ border: 1px solid #DDDDDD;--}}
        {{--}--}}











        /**
                                                                                        *




                                                @tab




                                                Page










        {{--* @section heading 1--}}









        *




                                                @tip




                                                Set the styling for all first-level headings in your emails. These should be the largest of your headings.
                                                                                            *




                                                @style




                                                heading 1
                                                                                            */
        h1, .h1 {
            /*




        @editable     */
            color: #202020;
            display: block;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 34px;
            /*




        @editable     */
            font-weight: bold;
            /*




        @editable     */
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            /*




        @editable     */
            text-align: left;
        }

        /**
        *




        @tab




        Page










        {{--* @section heading 2--}}









        *




                                                @tip




                                                Set the styling for all second-level headings in your emails.
                                                                                            *




                                                @style




                                                heading 2
                                                                                            */
        h2, .h2 {
            /*




        @editable     */
            color: #662f31;
            display: block;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 30px;
            /*




        @editable     */
            font-weight: bold;
            /*




        @editable     */
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            /*




        @editable     */
            text-align: center;
        }

        /**
        *




        @tab




        Page










        {{--* @section heading 3--}}









        *




                                                @tip




                                                Set the styling for all third-level headings in your emails.
                                                                                            *




                                                @style




                                                heading 3
                                                                                            */
        h3, .h3 {
            /*




        @editable     */
            color: #202020;
            display: block;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 26px;
            /*




        @editable     */
            font-weight: bold;
            /*




        @editable     */
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            /*




        @editable     */
            text-align: left;
        }

        /**
        *




        @tab




        Page










        {{--* @section heading 4--}}









        *




                                                @tip




                                                Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
                                                                                            *




                                                @style




                                                heading 4
                                                                                            */
        h4, .h4 {
            /*




        @editable     */
            color: #202020;
            display: block;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 22px;
            /*




        @editable     */
            font-weight: bold;
            /*




        @editable     */
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            /*




        @editable     */
            text-align: left;
        }

        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */

        /**
        *




        @tab




        Header










        {{--* @section preheader style--}}









        *




                                                @tip




                                                Set the background color for your email's preheader area.
                                                                                            *




                                                @theme




                                                page
                                                                                            */
        #templatePreheader {
            /*




        @editable     */
            background-color: #fff;
        }

        /**
        *




        @tab




        Header










        {{--* @section preheader text--}}









        *




                                                @tip




                                                Set the styling for your email's preheader text. Choose a size and color that is easy to read.
                                                                                            */
        {{--.preheaderContent div{--}}
            {{--/*@editable*/ color:#505050;--}}
            {{--/*@editable*/ font-family:Arial;--}}
            {{--/*@editable*/ font-size:10px;--}}
            {{--/*@editable*/ line-height:100%;--}}
            {{--/*@editable*/ text-align:left;--}}
        {{--}--}}











        /**
                                                                                        *




                                                @tab




                                                Header










        {{--* @section preheader link--}}









        *




                                                @tip




                                                Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
                                                                                            */
        {{--.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{--}}
            {{--/*@editable*/ color:#336699;--}}
            {{--/*@editable*/ font-weight:normal;--}}
            {{--/*@editable*/ text-decoration:underline;--}}
        {{--}--}}

                  /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */

        /**
        *




        @tab




        Header










        {{--* @section header style--}}









        *




                                                @tip




                                                Set the background color and border for your email's header area.
                                                                                            *




                                                @theme




                                                header
                                                                                            */
        #templateHeader {
            /*




        @editable     */
            background-color: #FFFFFF;
            /*




        @editable     */
            border-bottom: 0;
            /*




        @editable     */
            border-collapse: collapse;
        }

        /**
        *




        @tab




        Header










        {{--* @section header text--}}









        *




                                                @tip




                                                Set the styling for your email's header text. Choose a size and color that is easy to read.
                                                                                            */
        .headerContent {
            /*




        @editable     */
            color: #202020;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 34px;
            /*




        @editable     */
            font-weight: bold;
            /*




        @editable     */
            line-height: 100%;
            /*




        @editable     */
            padding: 0;
            /*




        @editable     */
            text-align: center;
            /*




        @editable     */
            vertical-align: middle;
        }

        /**
        *




        @tab




        Header










        {{--* @section header link--}}









        *




                                                @tip




                                                Set the styling for your email's header links. Choose a color that helps them stand out from your text.
                                                                                            */
        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */
        .headerContent a .yshortcuts /* Yahoo! Mail Override */
        {
            /*




        @editable     */
            color: #336699;
            /*




        @editable     */
            font-weight: normal;
            /*




        @editable     */
            text-decoration: underline;
        }

        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */

        /**
        *




        @tab




        Body










        {{--* @section body style--}}









        *




                                                @tip




                                                Set the background color for your email's body area.
                                                                                            */
        #templateContainer, .bodyContent {
            /*




        @editable     */
            background-color: #FFFFFF;
        }

        /**
        *




        @tab




        Body










        {{--* @section body text--}}









        *




                                                @tip




                                                Set the styling for your email's main content text. Choose a size and color that is easy to read.
                                                                                            *




                                                @theme




                                                main
                                                                                            */
        .bodyContent div {
            /*




        @editable     */
            line-height: 15px;
            color: #505050;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 14px;
            /*




        @editable     */
            /*line-height: 50%;*/
            /*




        @editable     */
            text-align: left;
        }

        /**
        *




        @tab




        Body










        {{--* @section body link--}}









        *




                                                @tip




                                                Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
                                                                                            */
        .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */
        .bodyContent div a .yshortcuts /* Yahoo! Mail Override */
        {
            /*




        @editable     */
            color: #fff;
            /*




        @editable     */
            font-weight: normal;
            /*




        @editable     */
            text-decoration: underline;
        }

        .bodyContent img {
            display: inline;
            height: auto;
        }

        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */

        /**
        *




        @tab




        Footer










        {{--* @section footer style--}}









        *




                                                @tip




                                                Set the background color and top border for your email's footer area.
                                                                                            *




                                                @theme




                                                footer
                                                                                            */
        #templateFooter {
            /*




        @editable     */
            background-color: #FFF;
            /*




        @editable     */
            border-top: 0;
        }

        /**
        *




        @tab




        Footer










        {{--* @section footer text--}}









        *




                                                @tip




                                                Set the styling for your email's footer text. Choose a size and color that is easy to read.
                                                                                            *




                                                @theme




                                                footer
                                                                                            */
        .footerContent div {
            /*




        @editable     */
            color: #707070;
            /*




        @editable     */
            font-family: Muli, sans-serif;
            /*




        @editable     */
            font-size: 12px;
            /*




        @editable     */
            line-height: 125%;
            /*




        @editable     */
            text-align: left;
        }

        .emailButton {
            background-color: #662f31;
            border-collapse: separate;
            border-radius: 4px;
        }

        .buttonContent {
            color: #fff;
            font-family: Muli, sans-serif;
            font-size: 18px;
            font-weight: bold;
            line-height: 100%;
            padding: 15px;
            text-align: center;
        }

        .buttonContent a {
            color: #fff;
            display: block;
            text-decoration: none;
        }

        /**
        *




        @tab




        Footer










        {{--* @section footer link--}}









        *




                                                @tip




                                                Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
                                                                                            */
        .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */
        .footerContent div a .yshortcuts /* Yahoo! Mail Override */
        {
            /*




        @editable     */
            color: #336699;
            /*




        @editable     */
            font-weight: normal;
            /*




        @editable     */
            text-decoration: underline;
        }

        .footerContent img {
            display: inline;
        }

        /**
        *




        @tab




        Footer










        {{--* @section social bar style--}}









        *




                                                @tip




                                                Set the background color and border for your email's footer social bar.
                                                                                            *




                                                @theme




                                                footer
                                                                                            */
        #social {
            /*




        @editable     */
            background-color: #fff;
            /*




        @editable     */
            border: 0;
        }

        /**
        *




        @tab




        Footer










        {{--* @section social bar style--}}









        *




                                                @tip




                                                Set the background color and border for your email's footer social bar.
                                                                                            */
        #social div {
            /*




        @editable     */
            text-align: center;
        }

        /**
        *




        @tab




        Footer










        {{--* @section utility bar style--}}









        *




                                                @tip




                                                Set the background color and border for your email's footer utility bar.
                                                                                            *




                                                @theme




                                                footer
                                                                                            */
        #utility {
            /*




        @editable     */
            background-color: #FFFFFF;
            /*




        @editable     */
            border: 0;
        }

        /**
        *




        @tab




        Footer










        {{--* @section utility bar style--}}









        *




                                                @tip




                                                Set the background color and border for your email's footer utility bar.
                                                                                            */
        #utility div {
            /*




        @editable     */
            text-align: center;
        }

        #monkeyRewards img {
            max-width: 190px;
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background-color: #ededed ">
<center>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
        <tr>
            <td align="center" valign="top">
                <!-- // Begin Template Preheader \\ -->
                <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
                    <tr>
                        <td valign="top" class="preheaderContent" style="padding:0 0 40px 0">

                            <!-- top bar brown -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top"
                                        style="border-spacing:0;border-collapse:collapse;height:20px;width:80%;background:#662f31;margin:0px;padding:0px;border:0">

                                    </td>
                                    <!-- top bar yellow -->
                                    <td valign="top" style="background-color: #662f31;width: 20%">

                                    </td>
                                    <!-- *|END:IF|* -->
                                </tr>
                            </table>
                            <!-- // End Module: Standard Preheader \ -->

                        </td>
                    </tr>
                </table>
                <!-- // End Template Preheader \\ -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                    <tr>
                        <td align="center" valign="top">
                            <!-- // Begin Template Header \\ -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                <tr>
                                    <td class="headerContent">

                                        <!-- // Begin Module: Standard Header Image \\ -->
                                        <img src="{{ 'http://massari.com.py/front/img/logo.png' }}"
                                             style="width: 20%; height: 7%"
                                             mc:label="header_image" mc:edit="header_image" mc:allowdesigner
                                             mc:allowtext/>
                                        <!-- // End Module: Standard Header Image \\ -->

                                    </td>
                                </tr>
                            </table>
                            <!-- // End Template Header \\ -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- // Begin Template Body \\ -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                <tr>
                                    <td valign="top" class="bodyContent">

                                        <!-- // Begin Module: Standard Content \\ -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">

                                                        <h2 class="h2" style="font-family: Muli, sans-serif">¡Gracias
                                                            por su Compra!</h2>

                                                        <p align="center" style="font-size: x-large; color: #707070">Su
                                                            pedido esta siendo procesado</p>

                                                        <br/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="border-color: #662f31; border-width: 2px !important" frame="box"
                                               cellpadding="10" cellspacing="0" width="80%" align="center">

                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Pedido:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->id !!}
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Tipo de Cliente:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        @if(isset($orderHeader->client_id)) Casual @else Normal @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Cliente:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        @if(isset($orderHeader->client_id)) {!! $orderHeader->noClient->description !!} @else {!! $orderHeader->clients->description !!} @endif

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Email:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->clients->email !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        C.I:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->clients->idnum !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        RUC:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        @if (isset($orderHeader->clients->tax_code)){!! $orderHeader->clients->tax_code !!} @else
                                                            4444444-4 @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Dirección
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->addresses->address !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Ciudad
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->addresses->cities->description !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Telefono:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        @if(isset($orderHeader->client_id))
                                                            {!! $orderHeader->noClient->telephone !!}
                                                        @else
                                                            {!! $orderHeader->clients->telephone !!}
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Forma de Pago:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->paymentForm->description !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        Hora de Entrega:
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">
                                                        {!! $orderHeader->reception_time_from !!}
                                                        / {!! $orderHeader->reception_time_to !!}
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // End Module: Standard Content \\ -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:60px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="240"
                                               style="height:30px !important;border-radius: 15px;"
                                               class="emailButton" align="left">
                                            <tr>
                                                <td align="center" valign="middle" class="buttonContent">
                                                    <a href="#" target="_blank">Detalle de productos</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="bodyContent">
                                        <table style="border-color: #662f31; border-width: 2px !important" frame="box"
                                               cellpadding="10" cellspacing="0" width="80%" align="center">
                                            <thead>
                                            <tr>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">CODIGO</div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">PRODUCTO</div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">PRECIO</div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">CANTIDAD</div>
                                                </td>
                                                <td valign="top">
                                                    <div mc:edit="std_content00">TOTAL</div>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($orderHeader->details as $detail)
                                                <tr>
                                                    <td valign="top">
                                                        <div mc:edit="std_content00">{!! $detail->id !!}</div>
                                                    </td>
                                                    <td valign="top">
                                                        <div mc:edit="std_content00">{!! $detail->product->name !!}</div>
                                                    </td>
                                                    <td valign="top">
                                                        <div mc:edit="std_content00">{!! number_format($detail->product->price, 0, ',', '.') !!} </div>
                                                    </td>
                                                    <td valign="top">
                                                        <div mc:edit="std_content00">{!! $detail->quantity !!}</div>
                                                    </td>
                                                    <td valign="top">
                                                        <div mc:edit="std_content00">{!! number_format($detail->sub_total, 0, ',', '.') !!}</div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr></tr>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                    <div mc:edit="std_content00">SUBTOTAL:
                                                        Gs. {!! number_format($orderHeader->order_amount, 0, ',', '.') !!}</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                    <div mc:edit="std_content00">DELIVERY:
                                                        Gs.{!! number_format($orderHeader->delivery_cost, 0, ',', '.') !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                    <div mc:edit="std_content00">TOTAL:
                                                        Gs. {!! number_format($orderHeader->order_amount + $orderHeader->delivery_cost, 0, ',', '.') !!}
                                                    </div>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                            </table>
                            <!-- // End Template Body \\ -->
                        </td>
                    </tr>
                    <tr style="background-color: #fff">
                        <td align="center" valign="top" style="background-color: #fff">
                            <!-- // Begin Template Footer \\ -->
                            <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                <tr>
                                    <td valign="top" class="footerContent">

                                        <!-- // Begin Module: Standard Footer \\ -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="100%">

                                            <tr>
                                                <td colspan="2" valign="middle" id="utility">
                                                    <div mc:edit="std_utility">
                                                        <p style="color: #662f31; font-size: large; font-weight: bold ">
                                                            www.massari.com.py | Tel: +595 981 592 950</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="middle" id="utility">
                                                    <div mc:edit="std_utility">
                                                        <img src="{{'http://www.massari.com.py/front/img/logo.png'}}"
                                                             width="4%" height="4%"> <span
                                                                style=" font-size: large;vertical-align: top; color: #662f31; font-family: Muli, sans-serif;font-weight: bold">massari.com.py</span>
                                                        <img src="{{'http://www.massari.com.py/front/img/facebook.png'}}"
                                                             width="3%" height="3%"> <span
                                                                style="font-size: large;vertical-align: top; color: #662f31; font-family: Muli, sans-serif;font-weight: bold">@massaripy</span>
                                                        <img src="{{'http://www.massari.com.py/front/img/instagram.png'}}"
                                                             width="3%" height="3%"> <span
                                                                style="font-size: large;vertical-align: top; color: #662f31; font-family: Muli, sans-serif;font-weight: bold">@massaripy</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // End Module: Standard Footer \\ -->

                                    </td>
                                </tr>
                            </table>
                            <!-- // End Template Footer \\ -->
                        </td>
                    </tr>
                </table>
                <br/>
            </td>
        </tr>
    </table>
</center>
</body>
</html>