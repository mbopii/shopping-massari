@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Nosotros</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- about-us-area-start -->
    <div class="about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="some-words-img">
                        <center>
                        <img src="{{ '/front/img/about-us/banner.jpg' }}" alt="">
                        </center>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="some-words-content">
                        <h2 class="about-us-title">Quienes Somos?</h2>
                        <p>Massari S.R.L es una empresa creada a fin cubrir todas las necesidades de la mujer moderna, ofreciendo productos de primera línea, originales, excelente calidad, costos inigualables, que mejoran la calidad de vida, directamente de los importadores con todas las garantías.</p>
                        <p>Con años de experiencia en los rubros de venta y distribución de artículos femeninos, fusionamos la compra con la logística, para que cada mujer, independiente de donde se encuentre, sea área rural o urbana, pueda adquirir un producto desde la comodidad de su hogar, lugar de trabajo u oficina, comprando desde su teléfono celular, computadora, o tablet y recibir directamente en propias manos en la brevedad posible.</p>
                        <p>Somos la primera tienda online del Paraguay dedicada neta y exclusivamente a la MUJER. Nuestro gran anhelo y deseo por el cual fue creada MASSARI S.R.L es la de llegar a todos los rincones del Paraguay, para satisfacer las necesidades, que todas tengan acceso a una tienda en donde puedan adquirir productos de alta calidad al mejor costo, para así verse beneficiadas en el dia a día. </p>
                        <p>Massari S.R.L tiene también responsabilidad social,colaborando, siendo parte de organizaciones de apoyo a la MUJER. </p>
                        <p>Creemos y estamos convencidos como personas y empresa que la MUJER es el PILAR que soporta a cada sociedad, nos debemos a ellas, por eso queremos aportar nuestro grano de arena para el bienestar de cada una. </p>
                        <p><b>DIRECTORIO MASSARI S.R.L. </b></p>
                    </div>
                </div>
            </div>
            {{--<div class="row">--}}
                {{--<div class="col-md-8 col-sm-12 col-xs-12">--}}
                    {{--<div class="feature-visual">--}}
                        {{--<h2 class="about-us-title">Que ofrecemos?</h2>--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>--}}
                        {{--<ul>--}}
                            {{--<li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat;</li>--}}
                            {{--<li>Nam liber tempor cum soluta nobis eleifend option;</li>--}}
                            {{--<li>Claritas est etiam processus dynamicus, qui sequitur mutationem;</li>--}}
                            {{--<li>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum;</li>--}}
                            {{--<li>Nam liber tempor cum soluta nobis eleifend.</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-12 col-xs-12">--}}
                    {{--<div class="why-our-shop">--}}
                        {{--<h2 class="about-us-title">Porque Comprar con Nosotros?</h2>--}}
                        {{--<div class="why-accordion">--}}
                            {{--<button class="accordion">Lorem ipsum dolor sit amet</button>--}}
                            {{--<div class="panel">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>--}}
                            {{--</div>--}}
                            {{--<button class="accordion">Ut wisi enim ad minim veniam, quis nostrud</button>--}}
                            {{--<div class="panel">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>--}}
                            {{--</div>--}}
                            {{--<button class="accordion">Duis autem vel eum iriure dolor</button>--}}
                            {{--<div class="panel">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>--}}
                            {{--</div>--}}
                            {{--<button class="accordion">Typi non habent claritatem insitam</button>--}}
                            {{--<div class="panel">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>--}}
                            {{--</div>--}}
                            {{--<button class="accordion">Mirum est notare quam littera gothica quam</button>--}}
                            {{--<div class="panel">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- about-us-area-end -->
@endsection