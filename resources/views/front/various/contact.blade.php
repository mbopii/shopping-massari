@extends('front.partials.layout')

@section('front_body')
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Contacto</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="contact-form-area">
                        <div class="contact-form-head">
                            <h2>Formulario de Contacto</h2>
                            <h1>Formulario de Contacto</h1>
                        </div>
                        <div class="contact-form-content">
                            <div class="cf-msg"></div>
                            <form action="{{ route('front.contact.send') }}" method="post" id="cf">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="cfc-input-box">
                                            <label>Nombre</label>
                                            <input type="text" id="fname" name="fname" style="border-color: #662B2D" required>
                                        </div>
                                        <div class="cfc-input-box">
                                            <label>Email</label>
                                            <input type="text" id="email" name="email" style="border-color: #662B2D" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="cfc-input-box">
                                            <label>Celular</label>
                                            <input type="text" id="fmobile" name="fmobile" style="border-color: #662B2D" required>
                                        </div>
                                        <div class="cfc-input-box">
                                            <label>Asunto</label>
                                            <input type="text" id="fsite" name="fsite" style="border-color: #662B2D" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="cfc-input-box">
                                            <textarea required style="border-color: #662B2D" class="contact-textarea" placeholder="Mensaje" id="msg" name="msg"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="cfc-input-box">
                                            <button id="submit" class="submit hvr-bs" name="submit">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

