@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- about-us-area-start -->
    <div class="about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="some-words-img">
                        <center>
                            <img src="{{ '/front/img/about-us/faq.jpg' }}" alt="">
                        </center>
                    </div>
                </div>
                {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="some-words-content">--}}
                        {{--<h2 class="about-us-title">Quienes Somos?</h2>--}}
                        {{--<p>Massari S.R.L es una empresa creada a fin cubrir todas las necesidades de la mujer moderna, ofreciendo productos de primera línea, originales, excelente calidad, costos inigualables, que mejoran la calidad de vida, directamente de los importadores con todas las garantías.</p>--}}
                        {{--<p>Con años de experiencia en los rubros de venta y distribución de artículos femeninos, fusionamos la compra con la logística, para que cada mujer, independiente de donde se encuentre, sea área rural o urbana, pueda adquirir un producto desde la comodidad de su hogar, lugar de trabajo u oficina, comprando desde su teléfono celular, computadora, o tablet y recibir directamente en propias manos en la brevedad posible.</p>--}}
                        {{--<p>Somos la primera tienda online del Paraguay dedicada neta y exclusivamente a la MUJER. Nuestro gran anhelo y deseo por el cual fue creada MASSARI S.R.L es la de llegar a todos los rincones del Paraguay, para satisfacer las necesidades, que todas tengan acceso a una tienda en donde puedan adquirir productos de alta calidad al mejor costo, para así verse beneficiadas en el dia a día. </p>--}}
                        {{--<p>Massari S.R.L tiene también responsabilidad social,colaborando, siendo parte de organizaciones de apoyo a la MUJER. </p>--}}
                        {{--<p>Creemos y estamos convencidos como personas y empresa que la MUJER es el PILAR que soporta a cada sociedad, nos debemos a ellas, por eso queremos aportar nuestro grano de arena para el bienestar de cada una. </p>--}}
                        {{--<p><b>DIRECTORIO MASSARI S.R.L. </b></p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="why-our-shop">
                        <h2 class="about-us-title">Preguntas Frecuentes</h2>
                        <div class="why-accordion">
                            <button class="accordion">¿Por qué comprar en MASSARI?</button>
                            <div class="panel">
                                <p>Contamos con una selección amplia de productos de calidad con precios inigualables, marcas nuevas y diseños innovadores. Además encontrarás las mejores opciones para regalar.</p>
                            </div>
                            <button class="accordion">¿Cuánto cuesta el envío?</button>
                            <div class="panel">
                                <p>El envío es calculado por la empresa que nos presta servicio, dependiendo de la localidad. </p>
                            </div>
                            <button class="accordion">¿Cuánto tarda el envío?</button>
                            <div class="panel">
                                <p>A nivel nacional entre 24 hs a 48 hs de emitido el pedido.</p>
                            </div>
                            <button class="accordion">¿Cómo puedo consultar el estado de mi pedido?</button>
                            <div class="panel">
                                <p>Al costado de su pedido usted tiene un link para ver donde se encuentra su paquete.</p>
                            </div>
                            <button class="accordion">¿Se hacen envíos a todo el país?</button>
                            <div class="panel">
                                <p>Así mismo, cubrimos todas las ciudades del país, en caso que su ciudad no se encuentre en el listado, hay un sector en donde puede cargar su ciudad.</p>
                            </div>
                            <button class="accordion">¿Tienen una atención al cliente?</button>
                            <div class="panel">
                                <p>Así mismo, tenemos atención vía redes sociales (Facebook, Instagram), también vía Whatsapp.</p>
                            </div>
                            <button class="accordion">¿Se permiten devoluciones?</button>
                            <div class="panel">
                                <p>Las devoluciones son por productos que tengan defectos de fábrica, todas las mercaderías son controladas para que estén en perfectas condiciones al enviar.</p>
                            </div>
                            <button class="accordion">¿Cómo puedo contactar ustedes?</button>
                            <div class="panel">
                                <p>Puede contactar a través del correo electrónico massaripy@gmail.com o a través de nuestro número de Whatsapp</p>
                            </div>
                            <button class="accordion">¿Qué hago si me llega el producto dañado durante el transporte?</button>
                            <div class="panel">
                                <p>En el caso de llegar en mal estado su producto, se le hará el cambio inmediato y reposición del mismo en la brevedad posible. </p>
                            </div>
                            <button class="accordion">¿Quién está detrás de la página?</button>
                            <div class="panel">
                                <p>Están personas con enormes ganas de dar su mejor esfuerzo para que otras personas tengan un mejor nivel de vida en su día a día con nuestros productos y atenciones.</p>
                            </div>
                            <button class="accordion">¿Cómo surgió la idea?</button>
                            <div class="panel">
                                <p>La idea de nuestra tienda se remonta a varios años atrás, en donde recorriendo varios puntos del país vimos la necesidad de que cada persona, independientemente de donde se encuentre sea interior o capital, tenga acceso a un comercio con productos de primera línea y a un costo muy beneficioso, adquiriendo desde la comodidad de su hogar u oficina con la mejor atención posible.</p>
                            </div>
                            <button class="accordion">¿El producto realmente es original?</button>
                            <div class="panel">
                                <p>Todos nuestros productos son originales de fábrica, cuentan con la garantía de calidad del importador.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about-us-area-end -->
@endsection