@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li><a href="{{ route('front.users.purchases', \Sentinel::getUser()->id) }}">Mis Compras</a>
                            </li>
                            <li class="active"><a href="#">Tracking</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- wishlist-table-area-start -->
    <div class="wishlist-table-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table-head">
                        <h4>Tracking de Pedido</h4>
                    </div>
                </div>
            </div>
            <link href="{{ '/front/css/progress-wizard.min.css' }}" rel="stylesheet">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <br>
                    <ul class="progress-indicator">
                        @if(count($trackingInfo) == 3)
                            <li class="completed"><span class="bubble"></span> Preparando</li>
                            <li class="completed"><span class="bubble"></span> Enviado</li>
                            <li class="completed"><span class="bubble"></span> Entregado</li>
                        @elseif (count($trackingInfo) == 2)
                            <li class="completed"><span class="bubble"></span> Preparando</li>
                            <li class="completed"><span class="bubble"></span> Enviado</li>
                            <li><span class="bubble"></span> Entregado</li>
                        @else
                            <li class="completed"><span class="bubble"></span> Preparando</li>
                            <li><span class="bubble"></span> Enviado</li>
                            <li><span class="bubble"></span> Entregado</li>
                        @endif

                    </ul>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="wishlist-table shopping-cart-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="wishlist-pro-desc">Fecha</th>
                                <th class="wishlist-pro-name table_not_show">Estado</th>
                                <th class="wishlist-pro-availabillity">Evento</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($trackingInfo as $tracking)
                                <tr class="table-row1">

                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $tracking->fecha }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value table_not_show">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $tracking->estado }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            @if ($tracking->estado == 'Pendiente')
                                                <a href="#">Pedido Recibido</a>
                                            @else
                                                <a href="#">{{ $tracking->tipo_evento }}</a>
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
