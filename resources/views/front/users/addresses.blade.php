@extends('front.partials.layout')


@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Mi Cuenta</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- my-account-area-start -->
    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my-account-welcome">
                        <h4 class="my-account-title">Hola {{ $user->description }}!</h4>
                        <ul class="profile">
                            <li><a href="{{ route('front.user.show', $user->id) }}"><i class="fa fa-user"></i> Mis Datos</a>
                            </li>
                            <li><a href="{{ route('front.user.addresses', $user->id) }}"><i class="fa fa-address-book"
                                                                                            style="color:#662B2D"></i>
                                    Mis Direcciones</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="personal-information">
                        <h4 class="my-account-title">Mis Direcciones</h4>
                        <div class="personal-information-box">

                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($arrayAddress))

                                        @foreach($arrayAddress as $address)
                                            <div class="row" id="{{ 'row-' . $address['id'] }}">
                                                <div class="col-xs-12 col-md-7 branch-info">
                                                    <p id="{{ 'p_address' . $address['id'] }}">
                                                        <label style="margin-bottom: 0px;"
                                                               id="{{ 'description-' . $address['id'] }}">{{ $address['description'] }}</label><br>
                                                        <label style="margin-bottom: 0px;"
                                                               id="{{ 'address2-' . $address['id'] }}">@isset($address['address2']){{ $address['address2'] }}@else
                                                                --- @endif</label><br>
                                                        <label style="margin-bottom: 0px;"
                                                               id="{{ 'references-' . $address['id'] }}">@if(isset($address['references'])) {{ $address['references'] }}@else
                                                                ---- @endif</label><br>
                                                        <label style="margin-bottom: 0px;"
                                                               id="{{ 'number-' . $address['id'] }}"> @if(isset($address['number']))
                                                                Nro de Casa: {{ $address['number'] }}@else
                                                                --- @endif</label> <br>
                                                        <label style="margin-bottom: 0px;"
                                                               id="{{ 'city-' . $address['id'] }}">{{ $address['city'] }}</label><br>
                                                    </p>
                                                </div>
                                                <div class="col-xs-12 col-md-5">
                                                    <div class="pib-save-change" style="margin-top: 0px !important;">
                                                        <button id="{{ 'prefer_button-' . $address['id'] }}"
                                                                @if($address['primary'])
                                                                @else
                                                                style="display:none;"@endif> Dirección Primaria
                                                        </button>
                                                        <br><br>
                                                        <button type="submit" class="form_open_button"
                                                                data-form-id="{{ $address['id'] }}">Editar
                                                        </button>
                                                        <button type="reset" class="delete_address"
                                                                data-form-id="{{ $address['id'] }}">Eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::open(['route' => ['front.address.update',':ROW_ID'], 'method' => 'post',
                                            'id' => 'address_' . $address['id'], 'style' => 'display:none', 'class' => 'address_hidden']) !!}
                                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <div class="row">
                                                {!! Form::hidden('flag', 123) !!} {{-- Solucion poco democrática--}}
                                                <input type="hidden" name="_token" id="token"
                                                       value="{{ csrf_token() }}">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group" style="margin-bottom: 0px">
                                                                <label>Calle 1</label>
                                                                {!! Form::text('address' , isset($address['description']) ? $address['description'] : null , ['class' => 'form-control', 'id'=>'direccion']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group" style="margin-bottom: 0px">
                                                                <label>Calle 2</label>
                                                                {!! Form::text('address2' , isset($address['address2']) ? $address['address2'] : null , ['class' => 'form-control', 'id'=>'direccion2']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group" style="margin-bottom: 0px">
                                                                <label>Referencias</label>
                                                                {!! Form::text('references' , isset($address['references']) ? $address['references'] : null , ['class' => 'form-control', 'id'=>'references']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label>Nro de Casa</label>
                                                                {!! Form::text('number' , isset($address['number']) ? $address['number'] : null , ['class' => 'form-control', 'id'=>'direccion']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="form-group">
                                                                <label for="">Ciudad</label>
                                                                <select name="new_city_id" id="new_city_id"
                                                                        style="height: 34px;"
                                                                        class="form-control">
                                                                    @foreach ($citiesArray as $c)
                                                                        <option value="{{ $c['id'] }}">{{ $c['description'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 edit_address_map">
                                                    <p style="padding: 10px"> Arrastre el punto con el mouse hasta la
                                                        ubicacion
                                                        de
                                                        entrega para tener una
                                                        mejor referencia</p>
                                                    <div class="row mapa">
                                                        <div class="col-md-12">
                                                            <div id='{{'map' . $address['id']}}'></div>
                                                            <pre id='{{ 'coordinates' . $address['id'] }}'
                                                                 class='coordinates'></pre>
                                                            <input type="hidden" name="{{ 'latitude' }}"
                                                                   id="{{ 'latitude-map' . $address['id'] }}">
                                                            <input type="hidden" name="{{ 'longitude' }}"
                                                                   id="{{ 'longitude-map' . $address['id'] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="pib-save-change">
                                                        <button class="confirmar update_address"
                                                                data-form-id="{{ $address['id'] }}">Guardar
                                                        </button>
                                                        <button class="confirmar cancel_address"
                                                                data-form-id="{{ $address['id'] }}">Cancelar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            {{ Form::close() }}

                                            <hr>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-7 col-md-5">
                                    <div class="pib-save-change" style="margin-top: 0px !important;">

                                        <button type="submit"
                                                id="make_new_address">Nueva Dirección
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(['route' => ['front.address.create'], 'method' => 'post',
                                                               'id' => 'new_address', 'style' => 'display:none', 'class' => 'address_hidden']) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::hidden('flag', 123) !!} {{-- Solucion poco democrática--}}
                                    <input type="hidden" name="_token" id="token"
                                           value="{{ csrf_token() }}">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-bottom: 0px">
                                                    <label>Calle 1</label>
                                                    {!! Form::text('address' , null , ['class' => 'form-control', 'id'=>'direccion']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-bottom: 0px">
                                                    <label>Calle 2</label>
                                                    {!! Form::text('address2' , null , ['class' => 'form-control', 'id'=>'direccion2']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-bottom: 0px">
                                                    <label>Referencias</label>
                                                    {!! Form::text('references' ,  null , ['class' => 'form-control', 'id'=>'references']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Nro. de Casa</label>
                                                    {!! Form::text('number' ,  null , ['class' => 'form-control', 'id'=>'direccion']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label for="">Ciudad</label>
                                                    <select name="new_city_id" id="new_city_id"
                                                            style="height: 34px;"
                                                            class="form-control">
                                                        @foreach ($citiesArray as $c)
                                                            <option value="{{ $c['id'] }}">{{ $c['description'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        No esta tu ciudad en la lista?
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Escribí tu ciudad</label>
                                                    {!! Form::text('new_city_description' ,  null , ['class' => 'form-control', 'id'=>'nueva_ciudad']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Elegí el departamento</label>
                                                    <select name="department_id" id="department_id"
                                                            style="height: 34px;"
                                                            class="form-control">
                                                        @foreach ($departmentsArray as $d)
                                                            <option value="{{ $d['id'] }}">{{ $d['description'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="pib-save-change">
                                                <button
                                                        id="save_new_address">Guardar
                                                </button>
                                                <button
                                                        id="cancel_new_address">Cancelar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="new_address_map">
                                        <p style="padding: 10px"> Arrastre el punto con el mouse hasta la
                                            ubicacion
                                            de
                                            entrega para tener una
                                            mejor referencia</p>
                                        <div class="row mapa">
                                            <div class="col-md-12">
                                                <div id='new_map'></div>
                                                <pre id='new_coordinates'
                                                     class='new_coordinates'></pre>
                                                <input type="hidden" name="latitude-new_map"
                                                       id="latitude-new_map">
                                                <input type="hidden" name="longitude-new_map"
                                                       id="longitude-new_map">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- my-account-area-end -->
    <div id="address_update_b_popup" class="b_popup_test"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Exito!</span><br><br>
        Registro actualizado correctamente<br><br>
    </div>
    <div id="address_destroy_b_popup" class="b_popup_test"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Exito!</span><br><br>
        Registro eliminado correctamente<br><br>
    </div>
    <div id="address_new_b_popup" class="b_popup_test"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Exito!</span><br><br>
        Datos modificados exitosamente<br><br>
    </div>
    <div id="address_destroy_missing" class="b_popup_test"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Error</span><br><br>
        Debe Agregar un numero de Casa<br><br>
    </div>
    <div id="address_destroy_missing2" class="b_popup_test"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Error</span><br><br>
        Debe ingresar al menos una direccion<br><br>
    </div>
    {!! Form::open(['route' => ['front.address.destroy',':ROW_ID'], 'method' => 'POST',
                                    'id' => 'form-delete']) !!}
@endsection

@section('front.js')
    <script src={{ '/front/js/mapbox.js' }}></script>
    <script src="{{"/front/js/map.js"}}"></script>
    <script>
        $('input[name=number]').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.form_open_button').click(
            function () {
                var id = $(this).data('form-id');
                $('#address_' + id).toggle();
                var myContainer = 'map' + id;
                var preCoordinates = document.getElementById('coordinates' + id);
                myMap(myContainer, preCoordinates);
                $('#' + myContainer).css('width', '100%').css('height', '270px');
            }
        );

        $('.cancel_address').click(function (evt) {
            evt.preventDefault();
            var id = $(this).data('form-id');
            console.log(id);
            var preCoordinates = document.getElementById('coordinates' + id);
            $('#address_' + id).toggle()
        });

        $('.update_address').click(function (evt) {
            evt.preventDefault();
            // We get all input data;
            var id = $(this).data('form-id');
            console.log("My id is: " + id);
            var input = $("form#address_" + id).serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            });

            console.log(input);
            var form = $("#address_" + id);
            var url = form.attr('action').replace(':ROW_ID', id);
            console.log("Mi numero de casa: " + input.number);
            if (input.number == ""){
                $('#address_destroy_missing').bPopup();
                return
            }

            if (input.address == ""){
                $('#address_destroy_missing2').bPopup();
                return
            }

            $.post(url, input, function (result) {
                if (result.error == false) {
                    console.log('This is a success');
                    $("#description-" + id).text(input.address);
                    $("#address2-" + id).text(input.address2);
                    $("#references-" + id).text(input.references);
                    $("#city-" + id).text(result.city);
                    $("#number-" + id).text("Nro de Casa: " + input.number);
                    $('#address_' + id).toggle();
                    $('#address_update_b_popup').bPopup();
                } else {
                    console.log(result);
                    alert(result.message)
                }
            });
        });

        $('#make_new_address').click(function (evt) {
            evt.preventDefault();
            $('#new_address').toggle();
            var myContainer = 'new_map';
            var preCoordinates = document.getElementById('new_coordinates');
            myMap(myContainer, preCoordinates);
            $('#' + myContainer).css('width', '100%').css('height', '270px');
        });

        $('#save_new_address').click(function (evt) {
            evt.preventDefault();
            // We get all input data;
            var input = $("form#new_address").serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            });
            console.log(input);
            var form = $("#new_address");
            var url = form.attr('action');
            console.log(url);

            if (input.number == ""){
                $('#address_destroy_missing').bPopup();
                return
            }

            if (input.address == ""){
                $('#address_destroy_missing2').bPopup();
                return
            }
            $.post(url, input, function (result) {
                if (result.error == false) {
                    console.log('This is a success');
                    $("#description").text(input.address);
                    $("#address2").text(input.address2);
                    $("#number").text("Nro de Casa: " + input.number);
                    $("#references").text(input.references);
                    $("#city").text(result.city);
                    $('#address').toggle();
                    $('#address_new_b_popup').bPopup();
                    location.reload();
                } else {
                    console.log(result);
                    alert(result.message);
                }
            })
        });

        $('#cancel_new_address').click(function (evt) {
            evt.preventDefault();
            $('#new_address').toggle();
        });

        $('.delete_address').click(function () {
            var id = $(this).data('form-id');
            console.log("My id is: " + id);
            var input = $("form#address_" + id).serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            });

            var form = $('#form-delete');
            var url = form.attr('action').replace(':ROW_ID', id);
            $.post(url, input, function (result) {
                if (result.error == false) {
                    // Ocultamos el div de esta direccion hasta el proximo refresco de pagina
                    $('#row-' + id).css('display', 'none');
                    $('#hr-' + id).css('display', 'none');
                    $('#address_destroy_b_popup').bPopup();
                } else {
                    alert('Ocurrio un error al intentar modificar el registro');
                }
            })
        });
    </script>
@endsection