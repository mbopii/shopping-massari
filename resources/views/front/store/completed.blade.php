@extends('front.partials.layout')
@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Carrito</a></li>
                        </ul>
                        <span class="crumb-name">Caja</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- shopping-head-area-start -->
    <div class="shopping-head-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shopping-head">
                        <div class="clerafix">
                            <div class="sh-menu-item">
                                <p>Mi Carrito</p>
                                <span>01</span>
                            </div>
                            <div class="sh-menu-item">
                                <p>Caja</p>
                                <span>02</span>
                            </div>
                            <div class="sh-menu-item active">
                                <p>Orden Procesada</p>
                                <span>03</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shopping-head-area-end -->
    <!-- checkout-area-start -->
    <div class="checkout-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkout-top">
                        <p>Orden Completada Exitosamente ! Estos son los datos de su orden</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="your-order-box">
                        <div class="yob-content">
                            <p class="yobc-title">Producto <span>TOTAL</span></p>
                            <ul>
                                @foreach($orderHeader->details as $item)
                                    <li class="clearfix">{{ $item->product->name }} ({{$item->quantity}})
                                        @if ($item->discount)
                                            <span>Gs. {{ number_format(($item->sub_total - $item->total_discount), 0, ',', '.') }}</span>
                                            <span><strike>Gs. {{ number_format($item->sub_total, 0, ',', '.') }} </strike>&nbsp;</span>
                                        @else
                                            <span>Gs. {{ number_format(($item->sub_total), 0, ',', '.') }}</span>
                                        @endif
                                    </li>
                                @endforeach
                                    <li class="clearfix">Delivery
                                            <span>Gs. {{ number_format($orderHeader->delivery_cost, 0, ',', '.') }}</span>
                                    </li>
                                <li class="order-total clearfix">Total:
                                    @if($orderHeader->discount)
                                        <span>Gs. {{ number_format((($orderHeader->order_amount - $orderHeader->discount_amount) + $orderHeader->delivery_cost), 0, ',', '.') }}</span>
                                        <span><strike>Gs. {{ number_format($orderHeader->order_amount + $orderHeader->delivery_cost, 0, ',', '.') }} </strike>&nbsp;</span>
                                    @else
                                        <span>Gs. {{ number_format($orderHeader->order_amount + $orderHeader->delivery_cost, 0, ',', '.') }}</span>
                                </li>
                                @endif
                            </ul>
                            <div class="create-account-checkout-method">
                                <div class="place-order">
                                    <a href="{{ route('store.index') }}">
                                        <button class="checkout-page-button hvr-bs">Finalizar!</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
