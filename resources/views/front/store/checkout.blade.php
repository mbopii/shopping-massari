@extends('front.partials.layout')
@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Carrito</a></li>
                        </ul>
                        <span class="crumb-name">Caja</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- shopping-head-area-start -->
    <div class="shopping-head-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shopping-head">
                        <div class="clerafix">
                            <div class="sh-menu-item">
                                <p>Mi Carrito</p>
                                <span>01</span>
                            </div>
                            <div class="sh-menu-item active">
                                <p>Caja</p>
                                <span>02</span>
                            </div>
                            <div class="sh-menu-item">
                                <p>Orden Procesada</p>
                                <span>03</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shopping-head-area-end -->
    <!-- checkout-area-start -->
    <div class="checkout-area">
        <div class="container">
            {{ Form::open(['route' => ['orders.store'], 'method' => 'post']) }}
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="billing-address">
                        <h4 class="checkout-box-title">Entrega y Facturación</h4>
                        <div class="checkout-billing-adress-box" id="address_form">
                            <div class="billing-address-input clearfix">
                                <label>Razon Social</label>
                                <input type="text" disabled id="tax_name">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Ruc</label>
                                <input type="text" disabled id="tax_code">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Dirección *</label>
                                <select id="select_address" name="address_id">
                                    @foreach ($preview as $p)
                                        <option value="{{ $p['id'] }}">{{ $p['description'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Dirección</label>
                                <input type="text" disabled id="checkout_address">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Numero</label>
                                <input type="text" disabled id="checkout_number">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Referencias</label>
                                <input type="text" disabled id="checkout_references">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Ciudad</label>
                                <input type="text" disabled id="checkout_city">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Tipo de Envio</label>
                                <input type="text" disabled id="delivery_description">
                            </div>
                            <div class="billing-address-input clearfix">
                                <label>Costo</label>
                                <input type="text" id="delivery_cost" disabled>
                                <input type="hidden" id="hidden_delivery_cost" name="hidden_delivery_cost">
                            </div>

                            <div class="billing-address-input clearfix">
                                <label>Tiempo Estimado</label>
                                <input type="text" disabled id="delivery_time">
                            </div>

                            {{--Opcion seleccionar hora --}}
                            <div class="billing-address-input clearfix" id="time_selection">
                                <label>Hora Desde</label>
                                <input type="text" id="reception_time_from" class="time start" name="reception_time_from" required>
                                <br><br>
                                <label>Hora Hasta</label>
                                <input type="text" id="reception_time_to" class="time end" name="reception_time_to" required>
                            </div>
                            {{--<div class="billing-address-input clearfix">--}}
                            {{--<label>Hora Hasta</label>--}}
                            {{--<input type="text" id="reception_time_to">--}}
                            {{--</div>--}}
                            {{--Fin Opcion seleccionar hora --}}


                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="your-order-box">
                        <h4 class="checkout-box-title">Su Pedido</h4>
                        <div class="yob-content">
                            <p class="yobc-title">Producto <span>TOTAL</span></p>
                            <ul>
                                @if ($cartItems)
                                    @foreach($cartItems as $item)
                                        <li class="clearfix"><img
                                                    src="{{{ "/front/img/products/{$item->model->id}/1.jpg"  }}}" alt=""
                                                    width="70px" height="70px">
                                            {{ $item->name }} ({{$item->qty}})<br>
                                            @if($item->discount == 1)
                                                <span>Gs. {{number_format($item->price * $item->qty - ($item->price * $item->qty * $item->model->discount_percentage / 100), 0, ',', '.') }}</span>
                                                <span><strike>Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</strike>&nbsp;</span>
                                            @elseif($item->discount == 2)
                                                <span>Gs. {{number_format($item->price * $item->qty - ($item->price * $item->qty * $item->model->discount_percentage2 / 100), 0, ',', '.') }}</span>
                                                <span><strike>Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</strike>&nbsp;</span>
                                            @else
                                                <span>Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</span>
                                            @endif
                                        </li>
                                    @endforeach
                                @endif
                                <li class="clearfix">
                                    Delivery <label for="" id="delivery_text"></label> <span>Gs. <label
                                                id="delivery">0</label></span>
                                </li>
                                <li class="order-total clearfix">Total:
                                    @if ($flag)
                                        <input type="hidden" name="flag" value="1">
                                        <span>Gs.  <label
                                                    id="sub_total">{{ number_format($subTotal, 0, ',', '.') }}</label></span>
                                        <span><strike>Gs.{{ number_format($subTotalDiscount, 0, ',', '.') }} </strike>&nbsp;</span>
                                        <input type="hidden" name="subtotal_discount"
                                               value="{{ $subTotal }}">
                                    @else
                                        <span>Gs. <label
                                                    id="sub_total">{{ number_format($subTotal, 0, ',', '.') }}</label></span>
                                    @endif
                                </li>
                            </ul>


                            <div class="create-account-checkout-method">
                                <div class="create-account-text">
                                    <p>Formas de Pago</p>
                                </div>
                                @foreach ($paymentsForm as $payment)
                                    <div class="select-payment-method">
                                        <input type="radio" name="payment_method_id" value="{{$payment->id}}">
                                        @if($payment->id == 1)
                                            <img src="{{'/front/img/cash.png'}}"
                                                 style="height: 19px; width: 85px"
                                                 alt="">
                                        @elseif($payment->id == 2)
                                            <img src="{{'/front/img/card.png'}}"
                                                 style="height: 19px; width: 85px"
                                                 alt="">
                                        @elseif($payment->id == 3)
                                            <img src="{{'/front/img/logobancos.jpg'}}"
                                                 style="height: 19px; width: 85px"
                                                 alt="">
                                        @endif
                                        <label>{{ $payment->description }}</label>
                                    </div>
                                @endforeach
                                <div class="select-payment-method">
                                    <input type="radio" name="payment_method_id" value="2">
                                    <img src="{{'/front/img/tigo.png'}}"
                                         style="height: 19px; width: 85px"
                                         alt="">
                                    <label>TigoMoney</label>
                                </div>
                                <div class="select-payment-method" id="cash_payment" style="display:none">
                                    <input type="radio" name="payment_method_id" value="1">
                                    <img src="{{'/front/img/cash.png'}}"
                                         style="height: 19px; width: 85px"
                                         alt="">
                                    <label>Efect/Pago en Destino</label>
                                </div>
                                <div class="place-order">
                                    <button type="submit" class="checkout-page-button hvr-bs">Comprar!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- checkout-area-end -->
    {!! Form::open(['route' => ['cart.get.address',':ROW_ID'], 'method' => 'GET',
                                          'id' => 'form-get']) !!} {!! Form::close() !!}

@endsection

@section("front.css")
    <link rel="stylesheet" href="{{ '/front/css/jquery.timepicker.min.css' }}">
    <style>
        .time{
            border: 0 none;
            border-bottom: 1px solid #e5e5e5;
            color: #646a7c;
            float: right;
            font-size: 14px;
            font-weight: 300;
            height: 40px;
            padding: 0 10px;
            width: 55%
        }
    </style>
@endsection
@section ('front.js')
    <script src="{{ '/front/js/jquery.timepicker.min.js' }}"></script>
    <script src="{{ '/front/js/datepair.min.js' }}"></script>

    <script>
        $(document).ready(function (e) {
            $('#time_selection .time').timepicker({
                minTime: '9:00am',
                maxTime: '18:00pm',
                timeFormat: 'H:i',
            });
            $('.time').on("keypress keyup blur",function (event) {
                $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
                if ((event.which < 48 || event.which > 60)) {
                    event.preventDefault();
                }
            });
            var datePairElement = document.getElementById('time_selection');
            var datepair = new Datepair(datePairElement);

            var id = $("#select_address").find("option:selected").attr("value");
            var form = $('#form-get');
            var url = form.attr('action').replace(':ROW_ID', id);
            var data = form.serialize();
            console.log(id);
            $.get(url, data, function (result) {
                console.log(result);
                if (result.error_code == false) {
                    // console.log(result);
                    $("#checkout_address").val(result.address);
                    $("#checkout_number").val(result.number);
                    $("#checkout_city").val(result.city);
                    $("#checkout_references").val(result.references);
                    $("#tax_name").val(result.tax_name);
                    $("#tax_code").val(result.tax_code);
                    if (result.delivery) {
                        $("#delivery_description").val(result.delivery.service);
                        $("#delivery_time").val(result.delivery.time);
                        $("#delivery_cost").val("Gs. " + result.delivery.cost);
                        $("#hidden_delivery_cost").val(result.delivery.cost);
                        $("#delivery").text(addCommas(result.delivery.cost));
                        if (result.delivery.collection) {
                            //     $('#delivery_text').text(" - " + result.delivery.text);
                            $("#cash_payment").css("display", '');
                            //     $("#sub_total").text(addCommas(subtotal));
                        } else {
                            $("#cash_payment").css("display", 'none')
                        }
                        $('#delivery_text').text("");

                        var newSubTotal = (parseInt(result.delivery.cost) + parseInt(subtotal));
                        console.log(subtotal);
                        console.log(newSubTotal);
                        $("#sub_total").text(addCommas(newSubTotal));
                        // }
                        // } else {
                        //     $("#delivery_description").val("---");
                        //     $("#delivery_time").val("---");
                        //     $("#delivery_cost").val("0");
                        //     $("#cash_payment").css("display", 'none');
                        //     $("#hidden_delivery_cost").val(0);
                        //     $("#sub_total").text(addCommas(subtotal));
                        //     $("#delivery").text(addCommas(result.delivery.cost));
                    }
                } else {
                    console.log('Error');
                }
            });
        });

        var subtotal = {{ $subTotal }}
        $("#select_address").change(function (e) {
            var id = $(this).find("option:selected").attr("value");
            e.preventDefault();
            var form = $('#form-get');
            var url = form.attr('action').replace(':ROW_ID', id);
            var data = form.serialize();
            console.log(id);
            $.get(url, data, function (result) {
                console.log(result);
                if (result.error_code == false) {
                    // console.log(result);
                    $("#checkout_address").val(result.address);
                    $("#checkout_number").val(result.number);
                    $("#checkout_city").val(result.city);
                    $("#checkout_references").val(result.references);
                    $("#tax_name").val(result.tax_name);
                    $("#tax_code").val(result.tax_code);
                    if (result.delivery) {
                        $("#delivery_description").val(result.delivery.service);
                        $("#delivery_time").val(result.delivery.time);
                        $("#delivery_cost").val("Gs. " + result.delivery.cost);
                        $("#hidden_delivery_cost").val(result.delivery.cost);
                        $("#delivery").text(addCommas(result.delivery.cost));
                        if (result.delivery.collection) {
                            //     $('#delivery_text').text(" - " + result.delivery.text);
                            $("#cash_payment").css("display", '');
                            //     $("#sub_total").text(addCommas(subtotal));
                        } else {
                            $("#cash_payment").css("display", 'none')
                        }
                        $('#delivery_text').text("");
                        var newSubTotal = (parseInt(result.delivery.cost) + parseInt(subtotal));
                        console.log(subtotal);
                        console.log(newSubTotal);
                        $("#sub_total").text(addCommas(newSubTotal));
                        // }
                        // } else {
                        //     $("#delivery_description").val("---");
                        //     $("#delivery_time").val("---");
                        //     $("#delivery_cost").val("0");
                        //     $("#cash_payment").css("display", 'none');
                        //     $("#hidden_delivery_cost").val(0);
                        //     $("#sub_total").text(addCommas(subtotal));
                        //     $("#delivery").text(addCommas(result.delivery.cost));
                    }
                } else {
                    console.log('Error');
                }
            });
        });


        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
    </script>
@endsection