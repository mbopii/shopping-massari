@extends('front.partials.layout')
@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="{{ route('store.index') }}">Tienda</a></li>
                        </ul>
                        <span class="crumb-name">Carrito</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- shopping-head-area-start -->
    <div class="shopping-head-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shopping-head">
                        <div class="clerafix">
                            <div class="sh-menu-item active">
                                <p>Mi carrito</p>
                                <span>01</span>
                            </div>
                            <div class="sh-menu-item">
                                <p>Caja</p>
                                <span>02</span>
                            </div>
                            <div class="sh-menu-item">
                                <p>Orden Procesada</p>
                                <span>03</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shopping-head-area-end -->
    <!-- shopping-cart-table-area-start -->
    <div class="shopping-cart-table-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shopping-cart-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="cart-img-title table_not_show">Id</th>
                                <th class="cart-pro-name table-padding">Producto</th>
                                <th class="cart-pro-price table_not_show">Precio</th>
                                <th class="cart-pro-quantity table-padding">Cant.</th>
                                <th class="cart-pro-total-price table-padding">Total</th>
                                <th class="cart-pro-remove table-padding"><a href="{{ route("cart.destroy.all") }}"><i
                                                class="fa fa-times"></i></a></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($cartItems)
                                @foreach ($cartItems as $item)
                                    <tr class="table-row1">
                                        <td class="cart_product_name_value table_not_show">
                                            <p class="cart_product_name">
                                                <a href="#">{{ $item->id  }}</a>
                                            </p>
                                        </td>
                                        <td class="cart_product_name_value table-padding">
                                            <p class="cart_product_name">
                                                <a href="{{ route('store.show', $item->id) }}">{{ $item->name }}</a>
                                            </p>
                                        </td>
                                        <td class="cart_product_price_value table_not_show">
                                            <span class="product_price">Gs. {{ number_format(($item->price), 0, ',', '.') }}</span>
                                        </td>
                                        <td class="cart_product_quantity_value table-padding">
                                            <div class="product-quantity-t">
                                                <input type="hidden" name="product_id" value="{{ $item->id }}"
                                                       class="product_id_cart">
                                                <input type="number" value="{{ $item->qty }}" name="quantity"
                                                       class="quantity_cart">
                                            </div>
                                        </td>
                                        <td class="cart_product_total_value table-padding">
                                            @if($item->discount == 1)
                                                <span class="product_price"
                                                      style="font-size: 10px;"><strike>Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</strike></span>
                                                <span class="product_price">Gs. {{ number_format($item->price * $item->qty - ($item->price * $item->qty * $item->model->discount_percentage / 100), 0, ',', '.') }}</span>
                                            @elseif($item->discount == 2)
                                                <span class="product_price"
                                                      style="font-size: 10px;"><strike>Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</strike></span>
                                                <span class="product_price">Gs. {{ number_format($item->price * $item->qty - ($item->price * $item->qty * $item->model->discount_percentage2 / 100), 0, ',', '.') }}</span>
                                            @else
                                                <span class="product_price">Gs. {{ number_format(($item->price * $item->qty), 0, ',', '.') }}</span>
                                            @endif
                                        </td>
                                        <td class="cart_product_name table-padding">
                                            <a href="{{ route("cart.destroy", $item->rowId) }}">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr class="table-row1">
                                <td class="cart_product_name table_not_show">&nbsp;</td>
                                <td class="cart_product_name table_not_show">&nbsp;</td>
                                <td class="cart_product_name_value table-padding"><p class="cart_product_name">Total</p>
                                </td>
                                <td class="cart_product_quantity_value table-padding"><span
                                            class="product-quantity-t">{{ Cart::count() }}</span></td>
                                <td class="cart_product_total_value table-padding">
                                    @if ($flag)
                                        <span
                                                class="product_price"><strike>Gs. {{ number_format($subTotal, 0, ',', '.') }}</strike></span>
                                        <span
                                                class="product_price">Gs. {{ number_format($subTotalDiscount, 0, ',', '.') }}</span>
                                    @else
                                        <span
                                                class="product_price">Gs. {{ number_format($subTotal, 0, ',', '.') }}</span>
                                    @endif
                                </td>
                                <td class="cart_product_name">&nbsp;</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shopping-cart-table-area-end -->
    <!-- shopping-cart-content-area-start -->
    <div class="shopping-cart-content-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="scca-head">
                        <a class="scb-continue shopping-cart-button hvr-bs" href="{{ route('store.index') }}">
                            {{ strtoupper(trans('words.go_to_store')) }}</a>
                        <a class="scb-update shopping-cart-button hvr-bs" href={{ route("cart.checkout") }}>
                            {{ strtoupper(trans('words.header_checkout')) }}
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- shopping-cart-content-area-end -->
    <!-- cart-page-service-area-start -->
    <div class="cart-page-service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-cart-page-box">
                        <p class="satisfaction">Satisfacción 100% Garantizada</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-cart-page-box">
                        <p class="shipping">Envio a todo el País</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-cart-page-box">
                        <p class="return">Politica de devoluciones</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-cart-page-box">
                        <p class="opening"> Abierto las 24 horas del día</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cart-page-service-area-end -->
    <!-- footer-start -->
@endsection

@section('front.js')
    <script>
        $(document).ready(function () {
            $('.quantity_cart').on('change', function () {
                var product_id = $(this).parents('tr').find('.product_id_cart').val();
                var quantity = $(this).parents('tr').find('.quantity_cart').val();

                var rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
                var finalURL = rootURL + 'cart/' + product_id + '/' + quantity + '/add';
                $.ajax({
                    url: finalURL,
                    data: '',
                    type: 'GET',
                    //dataType : 'json',
                    success: function (datos) {
                        console.log('actualizado');
                        $(this).parents('tr').find('.product_price span').text('123');
                    }
                });


            });
        });


    </script>

    <script>
        $('.quantity_cart').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    </script>
@endsection