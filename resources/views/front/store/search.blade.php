@extends('front.partials.layout')

@section('front_body')
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Tienda</a></li>
                        </ul>
                        {{--<span class="crumb-name">Shop Left Sidebar</span>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- shop-left-sidebar-area-start -->
    <div class="shop-left-sidebar-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="shop-sidebar-area">
                        <div class="sidebar-widget widget-categories">
                            <h4 class="sidebar-title">Filtros</h4>
                            <div class="sidebar-box">
                                <button class="sidebar-accordion-button" data-toggle="collapse" data-target="#men">
                                    Categorias
                                </button>
                                <ul class="sidebar-list collapse in" id="men">
                                    @foreach ($categoriesFilter as $category)
                                        <li>
                                            <a href="{{ route('store.filter.category', $category->id) }}">{{ $category->description }}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                            <div class="sidebar-box">
                                <button class="sidebar-accordion-button" data-toggle="collapse" data-target="#women">
                                    Marcas
                                </button>
                                <ul class="sidebar-list collapse in" id="women">
                                    @foreach ($brandsFilter as $brand)
                                        <li>
                                            <a href="{{ route('store.filter.brand', $brand->id) }}">{{ $brand->description }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="shop-grid">
                        <div class="shop-menu clearfix">
                            <div class="shop-tab-option">
                                <div class="deafult-sorting">
                                    <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                        @if( Request::is('store') || \Request::route()->getName() == 'store.filter.category')
                                            <option value="{{ route('store.index') }}">Ver todos</option>
                                            <option value="{{ route('store.rules', 'asc_price') }}">Menor Precio</option>
                                            <option value="{{ route('store.rules', 'desc_price') }}">Mayor Precio</option>
                                            <option value="{{ route('store.rules', 'asc') }}">Orden Ascendente</option>
                                            <option value="{{ route('store.rules', 'desc') }}">Orden Descendente</option>
                                        @elseif(Request::is('store/filter/asc_price'))
                                            <option value="{{ route('store.rules', 'asc_price') }}">Menor Precio</option>
                                            <option value="{{ route('store.rules', 'desc_price') }}">Mayor Precio</option>
                                            <option value="{{ route('store.rules', 'asc') }}">Orden Ascendente</option>
                                            <option value="{{ route('store.rules', 'desc') }}">Orden Descendente</option>
                                            <option value="{{ route('store.index') }}">Ver todos</option>
                                        @elseif(Request::is('store/filter/desc_price'))
                                            <option value="{{ route('store.rules', 'desc_price') }}">Menor Precio</option>
                                            <option value="{{ route('store.rules', 'asc_price') }}">Mayor Precio</option>
                                            <option value="{{ route('store.rules', 'asc') }}">Orden Ascendente</option>
                                            <option value="{{ route('store.rules', 'desc') }}">Orden Descendente</option>
                                            <option value="{{ route('store.index') }}">Ver todos</option>
                                        @elseif(Request::is('store/filter/asc'))
                                            <option value="{{ route('store.rules', 'asc') }}">Orden Ascendente</option>
                                            <option value="{{ route('store.rules', 'desc') }}">Orden Descendente</option>
                                            <option value="{{ route('store.rules', 'desc_price') }}">Menor Precio</option>
                                            <option value="{{ route('store.rules', 'asc_price') }}">Mayor Precio</option>
                                            <option value="{{ route('store.index') }}">Ver todos</option>
                                        @elseif(Request::is('store/filter/desc'))
                                            <option value="{{ route('store.rules', 'desc') }}">Orden Descendente</option>
                                            <option value="{{ route('store.rules', 'asc') }}">Orden Ascendente</option>
                                            <option value="{{ route('store.rules', 'desc_price') }}">Menor Precio</option>
                                            <option value="{{ route('store.rules', 'asc_price') }}">Mayor Precio</option>
                                            <option value="{{ route('store.index') }}">Ver todos</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="shop-grid-contents">
                            <div class="tab-content">
                                <div id="grid" class="tab-pane fade in active">
                                    <div class="grid-items">
                                        <div class="row">
                                            @for($i = 0; $i < count($arrayProducts['items']); $i++)
                                                @for($j = 0; $j < count($arrayProducts['items'][$i]); $j++)
                                                    <div class="col-md-4 col-sm-6 col-xs-12 prod" id="{{$arrayProducts['items'][$i][$j]->id}}">
                                                        <div class="single-featured-item">
                                                            <div class="sfi-img">
                                                                <a href="{{ route('store.show', $arrayProducts['items'][$i][$j]->id) }}"><img
                                                                            style="width: 300px !important; height: 300px !important;;"
                                                                            src="{{ "/front/img/products/{$arrayProducts['items'][$i][$j]->id}/1.jpg" }}"
                                                                            alt=""></a>

                                                            </div>
                                                            <div class="sfi-content">
                                                                <div class="sfi-name-cat">
                                                                    <a class="sfi-name"
                                                                       href="{{ route('store.show', $arrayProducts['items'][$i][$j]->id) }}">{{ $arrayProducts['items'][$i][$j]->name }}</a>
                                                                    <span class="sfi-cat">{{ $arrayProducts['items'][$i][$j]->categories->description }}</span>
                                                                </div>
                                                                <div class="sfi-price-rating">
                                                                    <p class="sfi-price">Precio:
                                                                        <span>Gs. {{ number_format($arrayProducts['items'][$i][$j]->price, 0, ',', '.') }}</span>
                                                                    </p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endfor
                                            @endfor
                                        </div>
                                        <div class="row">
                                            {{ $arrayProducts['paginate']->appends(['search' => $search])->links('front.partials.pagination') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::open(['route' => ['cart.add',':ROW_ID', ':COUNT'], 'method' => 'GET',
                                            'id' => 'form-add']) !!} {!! Form::close() !!}
@endsection

@section('front.js')
    @include('front.partials.addcart')
@endsection