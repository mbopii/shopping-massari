@extends('front.partials.layout')

@section('front_body')
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li><a href="{{ route('store.index') }}">Tienda</a></li>
                            <li class="active"><a href="#">Detalles</a></li>
                        </ul>
                        <span class="crumb-name">Detalles del Producto</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- single-product-area-start -->
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="product-tab-area">
                        <div class="clearfix">
                            <div class="product-tab-content tab-content">
                                <div id="spro1" class="tab-pane fade in active">
                                    <a href="{{ "/front/img/products/{$product->id}/1.jpg" }}" data-fancybox="images">
                                        <i class="fa fa-search-plus"></i>
                                        <img src="{{"/front/img/products/{$product->id}/1.jpg?dummy=" . rand(0, 9999) }}" alt="">
                                    </a>
                                </div>
                                @if (file_exists(public_path("/front/img/products/{$product->id}/2.jpg") ))
                                    <div id="spro2" class="tab-pane fade">
                                        <a href="{{ "/front/img/products/{$product->id}/2.jpg" }}"
                                           data-fancybox="images">
                                            <i class="fa fa-search-plus"></i>
                                            <img src="{{ "/front/img/products/{$product->id}/2.jpg?dummy=" . rand(0, 9999)  }}" alt="">
                                        </a>
                                    </div>
                                @endif
                                @if (file_exists(public_path("/front/img/products/{$product->id}/3.jpg") ))
                                    <div id="spro3" class="tab-pane fade">
                                        <a href="{{ "/front/img/products/{$product->id}/3.jpg" }}"
                                           data-fancybox="images">
                                            <i class="fa fa-search-plus"></i>
                                            <img src="{{ "/front/img/products/{$product->id}/3.jpg?dummy=" . rand(0, 9999)  }}" alt="">
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <div class="product-tab-menu">
                                <ul class="product-tab">
                                    <li class="active">
                                        <a data-toggle="tab" href="#spro1"><img
                                                    src="{{ "/front/img/products/{$product->id}/1.jpg" }}" alt=""></a>
                                    </li>
                                    @if (file_exists(public_path("/front/img/products/{$product->id}/2.jpg") ))
                                        <li>
                                            <a data-toggle="tab" href="#spro2"><img
                                                        src="{{ "/front/img/products/{$product->id}/2.jpg?dummy=" . rand(0, 9999)  }}"
                                                        alt=""></a>
                                        </li>
                                    @endif
                                    @if (file_exists(public_path("/front/img/products/{$product->id}/3.jpg" ) ))
                                        <li>
                                            <a data-toggle="tab" href="#spro3"><img
                                                        src="{{ "/front/img/products/{$product->id}/3.jpg?dummy=" . rand(0, 9999)  }}"
                                                        alt=""></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="single-product-description-area">
                        <h2 class="spd-title">{{ $product->name }}</h2>

                        <p class="spd-price">Gs. {{number_format($product->price, 0, ' ,', '.')}}
                            {{--                            <span class="spd-old-price">Gs. {{number_format($product->price, 0, ' ,', '.')}}</span>--}}
                        </p>
                        <div class="spd-meta">
                            <div class="spd-meta-type">
                                <p>Marca: <span>{{ $product->brand->description }}</span></p>
                            </div>
                            <div class="spd-meta-cat">
                                <span>Categoría:</span>
                                <ul>
                                    <li><a href="#">{{ $product->categories->description }}</a></li>
                                </ul>
                            </div>

                        </div>
                        {!! $product->description !!}
                        <ul class="spd-list">
                            <li>Nuevo en caja</li>
                            <li>Envios a todo el pais</li>
                            @if ($product->discount)
                                <li>Comprando {{ $product->discount_quantity }} o más,
                                    tenes {{ $product->discount_percentage }} % de descuento!
                                </li>
                            @endif
                            @if ($product->discount && $product->discount_percentage2 > 0)
                                <li>Comprando {{ $product->discount_quantity2 }} o más,
                                    tenes {{ $product->discount_percentage2 }} % de descuento!
                                </li>
                            @endif
                        </ul>
                        <div class="spd-add-cart">
                            <div class="row">
                                <div class="col-xs-6">

                                    <p class="quantity cart-plus-minus">
                                        <input id="count" type="text" value="1"/>
                                    </p>
                                    <input type="hidden" id="product_id" value="{{ $product->id }}">
                                    <br>
                                    <p class="spd-price">Compartir por</p>
                                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                    <div class="addthis_inline_share_toolbox"></div>
                                </div>
                                <div class="col-xs-6">
                                    <button class="spd-add-to hvr-bs"
                                            id="send-button">{{ strtoupper(trans('words.add_to_cart')) }}</button>
                                    <br><br>
                                    <button class="spd-add-to hvr-bs"
                                            id="now-button">{{ strtoupper(trans('words.buy_now')) }}
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['cart.add',':ROW_ID', ':COUNT'], 'method' => 'GET',
                                    'id' => 'form-add']) !!} {!! Form::close() !!}

    {!! Form::open(['route' => ['cart.now',':ROW_ID', ':COUNT'], 'method' => 'GET',
                                   'id' => 'form-now']) !!} {!! Form::close() !!}
@endsection

@section('front.js')
    @include('front.partials.addcart')
    @include('front.partials.now')
    <script>
        $('#count').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    </script>
@endsection