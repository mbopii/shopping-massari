<script>
    $(document).ready(function () {
        var rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var finalURL;
        $('#now-button').click(function (e) {
            e.preventDefault();
            console.log("Buy Now!");
            var quantity = $('#count').val();
            console.log(quantity);
            var product = $('#product_id').val();
            var form = $('#form-now');
            var url = form.attr('action').replace(':ROW_ID', product).replace(':COUNT', quantity);
            var data = form.serialize();
            var type = "";
            var title = "";
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                statusCode: {
                    401: function (response) {
                        console.log("Go to Login");
                        finalURL = rootURL + 'login/';
                        window.location.href = finalURL;
                    }
                },
                success: function (result) {
                    console.log("Response: " + result);
                    if (result.error == false) {
                        console.log(result);
                        finalURL = rootURL + 'cart/checkout';
                        window.location.href = finalURL;
                    } else {
                        $('#error').modal('show');
                        type = "error";
                        title = "Ocurrio un problema al intentar agregar su producto"
                    }
                },
            });

        });
    });
</script>
