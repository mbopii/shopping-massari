<link rel="stylesheet" href="{{ '/front/css/bootstrap.min.css' }}">
<link rel="stylesheet" href="{{ '/front/css/font-awesome.min.css' }}">
<link rel="stylesheet" href="{{'/front/css/owl.carousel.min.css'}}">
<link rel="stylesheet" href="{{ '/front/css/magnific-popup.css' }}">
<link rel="stylesheet" href="{{ '/front/css/animate.css' }}">
<link rel="stylesheet" href="{{ '/front/css/jquery.fancybox.min.css' }}">
<link rel="stylesheet" href="{{ '/front/css/meanmenu.css' }}">
<link rel="stylesheet" href="{{ '/front/css/nivo-slider.css' }}">
<link rel="stylesheet" href="{{ '/front/css/jquery-ui.css' }}">
<link rel="stylesheet" href="{{ '/front/css/typography.css' }}">
<link rel="stylesheet" href="{{ '/front/css/style.css' }}">
<link rel="stylesheet" href="{{ '/front/css/responsive.css' }}">


<style>
    .b_popup_test {
        background-color: #fff;
        border-radius: 10px 10px 10px 10px;
        box-shadow: 0 0 25px 5px #999;
        color: #111;
        display: none;
        min-width: 310px !important;
        max-width: 550px;
        padding: 22px;
        font-family: 'Montserrat', sans-serif;
        font-weight: normal;
    }
    .b-close{
        border-radius: 7px 7px 7px 7px;
        box-shadow: none;
        font: bold 131% sans-serif;
        padding: 0 6px 2px;
        position: absolute;
        right: -7px;
        top: -7px;
        background-color: #662B2D;
        color: #fff;
        cursor: pointer;
        display: inline-block;

    }

    .popup_logo{
        color: #662B2D;
        font: bold 325% 'Montserrat', sans-serif;
    }

    .headers_buttons {
        background: #662B2D;
        border: 1px solid #662B2D;
        border-radius: 10px;
        color: #ffffff !important;
        font-size: 14px !important;
        text-align: center;

    }

    .whatsapp_button{
        background-color:#00E676;
        width:50px;
        height:50px;
        border-radius:100%;
        border:none;
        outline:none;
        color:#FFF;
        /*bottom:100px;*/
        font-size:36px;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        transition:.3s;

        padding: 5px;
        bottom: 10px;
        right: 15px;
        z-index: 555;
        position: fixed;
        -webkit-tap-highlight-color: rgba(0,0,0,0);
    }
</style>