
@if($errors->all())
    @foreach($errors->all() as $error)
        <div id="element_to_pop_up2" class="b_popup_test" style="left: 300px; right: 300px; position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
            <span class="button b-close"><span>X</span></span>
            <span class="popup_logo">Alerta</span><br><br>
            {{ $error }}<br><br>
        </div>
    @endforeach
    <script>
        $(function() {
            $('#element_to_pop_up2').bPopup();
        });
    </script>
@endif
@if(Session::has('error'))
    <div id="element_to_pop_up2" class="b_popup_test" style="left: 300px; right: 300px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Alerta</span><br><br>
        {{ Session::get('error') }}<br><br>
    </div>
    <script>
        $(function() {
            $('#element_to_pop_up2').bPopup();
        });
    </script>
@endif

@if(Session::has('success'))
    <div id="element_to_pop_up2" class="b_popup_test" style="left: 300px; right: 300px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Exito!</span><br><br>
        {{ Session::get('success') }}<br><br>
    </div>
    <script>
        $(function() {
            $('#element_to_pop_up2').bPopup();
        });
    </script>
@endif
