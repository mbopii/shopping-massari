<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125016407-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-125016407-1');
    </script>
    <!--- Basic Page Needs  -->
    <meta charset="utf-8">
    <title>Massari</title>
    <meta charset="UTF-8">
    <meta
            name='viewport'
            content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
    />
    <meta name="description" content="Tienda online donde encontrarás todo para la Mujer Paraguaya">
    <meta name="keywords" content="ecommerce, rondina, lenceria, maquillaje, peluqueria">
    <meta name="author" content="Miguel Romero">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Sacramento|Shadows+Into+Light" rel="stylesheet">@include('front.partials.css')
    @yield('front.css')
<!-- Favicon -->
    <link rel="shortcut icon" type="/front/image/png" href="{{ '/front/img/logo.png' }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '229101781334512');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=229101781334512&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>
<!--Start of Tawk.to Script-->
{{--<script type="text/javascript">--}}
{{--var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
{{--(function(){--}}
{{--var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
{{--s1.async=true;--}}
{{--s1.src='https://embed.tawk.to/5b865602f31d0f771d843e49/default';--}}
{{--s1.charset='UTF-8';--}}
{{--s1.setAttribute('crossorigin','*');--}}
{{--s0.parentNode.insertBefore(s1,s0);--}}
{{--})();--}}
{{--</script>--}}
<!--End of Tawk.to Script-->
<body data-spy="scroll" data-target="#scroll-menu" data-offset="100">
<div id="preloader"></div>
<!-- header-start -->
@include('front.partials.header')
<!-- header-end -->
@yield('front_body')
<!-- brand-area-end -->
<!-- footer-start -->
@include('front.partials.footer')
<!-- footer-end -->
<!-- newsletter-popup-start -->
{{--<div id="wd1_nlpopup_overlay"></div>--}}
{{--<div id="wd1_nlpopup" data-expires="30" data-delay="10">--}}
{{--<span id="wd1_nlpopup_close">x</span>--}}
{{--<h2>newsletter</h2>--}}
{{--<p>Subscribe to the Motorvehikle mailing list to receive updates on new arrivals, special offers and other discount information.</p>--}}
{{--<div class="popup-subscribe-box">--}}
{{--<input type="email">--}}
{{--<input type="submit" value="submit">--}}
{{--</div>--}}
{{--<div class="pupup-dont-show">--}}
{{--<input type="checkbox">--}}
{{--<span>Don't show this popup again</span>--}}
{{--</div>--}}
{{--</div>--}}
<!-- newsletter-popup-end -->
<!-- modal-start -->
<a href="https://api.whatsapp.com/send?phone=595981592950&amp;text=Hola%21%20Estoy%20escribiendo%20desde%20la%20Web%20de%20Massari!"
   target="_blank">
    <button class="whatsapp_button"><i class="fa fa-whatsapp"></i></button>
</a>


<!-- modal-end -->
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b8e1ef4e13e91a9"></script>
</body>
@include('front.partials.js')

@yield('front.js')
@include('front.partials.alerts')
<!-- Mirrored from demo.dueza.com/uniform-html/uniform/01_Home-Page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 17 May 2018 15:18:02 GMT -->
</html>
