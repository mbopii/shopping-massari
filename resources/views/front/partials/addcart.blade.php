<script>
    $(document).ready(function () {
        $('#send-button').click(function (e) {
            console.log("click");
            e.preventDefault();
            console.log("defaut");
            var quantity = $('#count').val();
            // alert(quantity);
            console.log(quantity);
            var product = $('#product_id').val();
            var form = $('#form-add');
            var url = form.attr('action').replace(':ROW_ID', product).replace(':COUNT', quantity);
            var data = form.serialize();
            var type = "";
            var title = "";
            // alert ("This: " + product + " " + quantity);
            $.get(url, data, function (result) {
                if (result.error == false) {
                    console.log(result);
//                    var className = "cartImg" + product_id;
//                    var cartImg = document.getElementById(className);
//                    console.log(cartImg);
//                    cartImg.style.display = 'block';
                    $('#element_to_pop_up').bPopup();
                    var subtotal = $("#subtotal");
                    var subtotalquantity = $("#subtotalquantity");
                    var miniQuantity = $("#mini_count");
                    var number = parseFloat(subtotalquantity.text()) + parseFloat(quantity);
                    subtotal.text(result.subtotal);
                    subtotalquantity.text(number);
                    miniQuantity.text(number);
                } else {
                    $('#error').modal('show');
                    type = "error";
                    title = "Ocurrio un problema al intentar agregar su producto"
                }
            })

        });

        $('.send-button').click(function (e) {
            console.log("click");
            e.preventDefault();
            console.log("defaut");
            var quantity = $('#count').val();
            // alert(quantity);
            console.log(quantity);
            var p = $(this).parents('.prod');
            var product = p[0].id;
            var form = $('#form-add');
            var url = form.attr('action').replace(':ROW_ID', product).replace(':COUNT', quantity);
            var data = form.serialize();
            var type = "";
            var title = "";
            // alert ("This: " + product + " " + quantity);
            $.get(url, data, function (result) {
                if (result.error == false) {
                    console.log(result);
                    $('#element_to_pop_up').bPopup();
                    var subtotal = $("#subtotal");
                    var subtotalquantity = $("#subtotalquantity");
                    var miniQuantity = $("#mini_count");
                    var number = parseFloat(subtotalquantity.text()) + parseFloat(quantity);
                    subtotal.text(result.subtotal);
                    subtotalquantity.text(number);
                    miniQuantity.text(number);
                } else {
                    $('#error').modal('show');
                    type = "error";
                    title = "Ocurrio un problema al intentar agregar su producto"
                }
            })

        })


    });
</script>


<div id="element_to_pop_up" class="b_popup_test"
     style="left: 300px; right: 300px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
    <span class="button b-close"><span>X</span></span>
    <span class="popup_logo">Exito</span><br><br>
    Producto Agregado exitosamente!<br><br>

    <div class="spd-add-cart">
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <a href="{{ route('store.index') }}">
                    <button class="spd-add-to hvr-bs"
                            style="height: 60px;">{{ strtoupper(trans('words.keep_buying')) }}</button>
                </a>
            </div>
            <div class="col-lg-6 col-xs-6">
                <a href="{{ route('cart.checkout') }}">
                    <button class="spd-add-to hvr-bs"
                            style="height: 60px;">{{ strtoupper(trans('words.go_to_checkout')) }}</button>
                </a>
            </div>

        </div>
    </div>
</div>    <!-- shop-left-sidebar-area-end -->