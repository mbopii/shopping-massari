<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <div class="header-top-left">
                        <div class="logo">
                            <a href="{{ route('index') }}"><img src="/front/img/logo.png" alt="" width="20%"
                                                                height="20%">
                                <label
                                        class="movil">MASSARI</label>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-8 col-xs-12" id="header_search_box">
                    <div class="header-top-right">
                        <div class="htr-search">
                            <form action="{{ route('store.search') }}">
                                <div class="htr-search-content" id="search">
                                    <input name="search" type="text" placeholder="@lang('words.search_bar')"
                                           id="search_input">
                                    <button><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-8 show_user">
                    <div class="header-top-right">
                        <div class="login-or-register">
                            @if (\Sentinel::getUser())
                                <span id="navbarname">@lang('words.header_hi') {{ current(explode(' ', \Sentinel::getUser()->description))}}</span>
                            @section('front.js')
                                <script>
                                    var user_id = {!! \Sentinel::getUser()->id !!}
                                    $("#navbarname").on('click', function (e) {
                                        e.preventDefault();
                                        window.location.href = "https://massari.com.py/profile/" + user_id;
                                    });
                                </script>
                            @endsection
                            @else
                                <span id="login_register">@lang('words.header_register')</span>
                                <div class="login-register-content" id="login_register_content">
                                    <div class="lrc-login">
                                        <h4 class="lrc-title">Mi cuenta</h4>
                                        <div class="lrc-login-form">
                                            <form id="header_login" action="{{ route('login.do') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="account-name">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" placeholder="Email" name="email">
                                                </div>
                                                <div class="password">
                                                    <i class="fa fa-lock"></i>
                                                    <input type="password" placeholder="Password"
                                                           name="password">
                                                </div>
                                                <div class="lrc-submit">
                                                    <button class="viewcart headers_buttons" type="submit">Entrar
                                                    </button>
                                                    <a href="{{ route('login.facebook') }}">
                                                        <button type="button" class="viewcart headers_buttons"
                                                                onclick="facebook(this)">Entrar con <i
                                                                    class="fa fa-facebook-square"></i></button>
                                                    </a>

                                                </div>
                                                <div class="remember-help">
                                                    <a href="{{ route('show.recovery.password') }}">Olvidaste tu cuenta? Click Aquí</a>
                                                </div>
                                                @section('front.js')
                                                    <script>
                                                        function facebook() {
                                                            e.preventDefault();
                                                        }
                                                    </script>
                                                @endsection
                                                {{--<div class="lrc-submit">--}}
                                                {{--<input class="hvr-bs" type="submit" value="Facebook!">--}}
                                                {{--</div>--}}
                                                {{--<div class="viewcart-checkout">--}}
                                                {{--<button type="submit">Test</button>--}}
                                                {{--<button class="viewcart" href="" id="login-button" onclick="document.getElementById('header_login').submit()">Entrar</button>--}}
                                                {{--</div>--}}
                                                {{--<br>--}}
                                                {{--<div class="viewcart-checkout">--}}
                                                {{--<a id="facebook_button" class="viewcart" href="#"><i class="fa fa-facebook-square"></i> Entrar Con Facebook</a>--}}
                                                {{--</div>--}}
                                            </form>
                                        </div>
                                    </div>
                                    <div class="lrc-login">
                                        <h4 class="lrc-title">O Registrate</h4>
                                        <div class="lrc-login-form">
                                            <form id="header_register" action="{{ route('front.register.do') }}"
                                                  method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="account-name">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" placeholder="Tu nombre" name="description">
                                                </div>
                                                <div class="account-name">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" placeholder="Email" name="email">
                                                </div>
                                                <div class="account-name">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" placeholder="C.I" name="idnum">
                                                </div>
                                                <div class="account-name">
                                                    <i class="fa fa-phone"></i>
                                                    <input type="text" placeholder="Nro de Telef." name="telephone">
                                                </div>
                                                <div class="password" style="margin-bottom: 22px">
                                                    <i class="fa fa-lock"></i>
                                                    <input type="password" placeholder="Tu Password"
                                                           name="password">
                                                </div>
                                                <div class="password">
                                                    <i class="fa fa-lock"></i>
                                                    <input type="password" placeholder="Confirma tu Password"
                                                           name="password_confirm">
                                                </div>
                                                <div class="lrc-submit">
                                                    <button id="register_button" class="viewcart headers_buttons"
                                                            type="submit">Registrarse
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="top-cart">
                            <span id="top_cart"><i class="fa fa-shopping-basket"></i>@lang('words.header_cart') <span
                                        class="cart-number">(<label
                                            id="mini_count">{{Cart::count()}}</label>)</span></span>
                            <div class="my-cart" id="my_cart">

                                <div class="my-cart-total-cost">
                                    <span>Cantidad:</span>
                                    <span class="cost"><label id="subtotalquantity">{{ Cart::count() }} </label> item(s)</span>
                                </div>
                                <div class="my-cart-total-cost">
                                    <span>Total:</span>
                                    <span class="cost">Gs. <label
                                                id="subtotal">{{ Cart::subtotal(0, ',', '.') }}</label></span>
                                </div>
                                <div class="viewcart-checkout">
                                    <a class="viewcart"
                                       href="{{ route('cart.show') }}">@lang('words.header_cart')</a>
                                    <a class="checkout"
                                       href="{{ route('cart.checkout') }}">@lang('words.header_checkout')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="move_search_box"></div>
            </div>
        </div>
        <div class="menu-area hidden-sm hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="menu">
                            <nav>
                                <ul class="menu-list">
                                    <li @if (\Request::is('/'))class="active" @endif><a
                                                href="{{ route('index') }}">@lang("words.header")</a></li>
                                    <li @if (\Request::is('store*'))class="active" @endif><a
                                                href="{{ route('store.index') }}">@lang('words.store')</a>
                                    @foreach ($categories as $category)
                                        <li @if (\Request::is("store/{$category->id}/category"))class="active" @endif>
                                            <a href="{{ route('store.filter.category', $category->id) }}">{{ $category->description }}</a>
                                        </li>
                                    @endforeach
                                  
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="menu-area-right">
                            @if(\Sentinel::getUser())
                                <div class="my-account">
                                    <a href="#">Mi Cuenta</a>
                                    <ul class="menu-right-dropdown">
                                        <li><a href="{{ route('front.user.show', \Sentinel::getUser()->id) }}">Mis
                                                Datos</a>
                                        </li>
                                        <li><a href="{{ route('wish.index') }}">Mi Lista de Deseos</a></li>
                                        @if (\Sentinel::getUser() && \Sentinel::getUser()->hasAccess('users'))
                                            <li><a href="{{ route('admin.index') }}">Administración</a></li>@endif
                                        <li>
                                            <a href="{{ route('front.users.purchases',  \Sentinel::getUser()->id) }}">Mis
                                                Compras</a></li>
                                        <li><a href="{{ route('front.users.pending',  \Sentinel::getUser()->id) }}">Mis
                                                Pagos Pendientes</a></li>
                                        <li><a href="{{ route('logout') }}">Salir</a></li>
                                    </ul>
                                </div>
                            @endif
                            <div class="language">
                                @if(App::isLocale('es'))
                                    <a href="{{ route('lang.switch', 'es') }}"><img
                                                src="{{ '/front/img/flag/spain.jpg' }}" width="16"
                                                height="11"
                                                alt="">Español</a>
                                @else
                                    <a href="{{ route('lang.switch', 'gn') }}"><img
                                                src="{{ '/front/img/flag/paraguay.jpg' }}" width="16"
                                                height="11"
                                                alt="">Guarani</a>
                                @endif
                                <ul class="menu-right-dropdown">
                                    @if(App::isLocale('es'))
                                        <li>
                                            <a href="{{ route('lang.switch', 'gn') }}"><img
                                                        src="{{ '/front/img/flag/paraguay.jpg' }}" width="16"
                                                        height="11" alt="">Guaraní</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ route('lang.switch', 'es') }}"><img
                                                        src="{{ '/front/img/flag/spain.jpg' }}" width="16"
                                                        height="11" alt="">Español</a>
                                        </li>

                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-start -->
        <div class="mobile-menu-area visible-xs visible-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile_menu">
                            <nav id="mobile_menu_active">
                                <ul class="menu-list">
                                    <li><a href="{{ route('index') }}">Inicio</a>
                                    </li>
                                    <li><a href="{{ route('store.index') }}">Tienda</a></li>
                                    <li><a href="#" class="nav_bar_new">Categorías</a>
                                        <ul>
                                            @foreach ($categories as $category)
                                                <li>
                                                    <a href="{{ route('store.filter.category', $category->id) }}">{{ $category->description }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li><a href="#" class="nav_bar_new">Idiomas</a>
                                        <ul>
                                            <li>
                                                <a href="{{ route('lang.switch', 'es') }}">Español</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('lang.switch', 'gn') }}">Guarani</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @if (\Sentinel::getUser())
                                        <li><a href="{{ route('front.user.show', \Sentinel::getUser()->id) }}">Mi
                                                Perfil</a>
                                        </li>
                                        <li><a href="{{ route('wish.index') }}">Mi Lista de Deseos</a></li>
                                        <li><a href="{{ route('front.users.purchases',  \Sentinel::getUser()->id) }}">Mis
                                                Compras</a></li>
                                        <li><a href="{{ route('front.users.pending',  \Sentinel::getUser()->id) }}">Mis
                                                Pagos Pendientes</a></li>
                                        @if (\Sentinel::getUser() && \Sentinel::getUser()->hasAccess('users'))
                                            <li><a href="{{ route('admin.index') }}">Administración</a></li>@endif
                                        <li><a href="{{ route('logout') }}">Salir</a></li>
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-end -->
    </div>
</header>
@section('front.js')
    <script>
        $('#facebook_button').click(function (e) {
            console.log("Facebook Login");
            e.preventDefault();
            window.location.href = "{{ route('login.facebook') }}";
        });
    </script>


@endsection