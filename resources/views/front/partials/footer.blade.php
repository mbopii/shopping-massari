<footer>
    <div class="footer-top-area">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-desc">
                            <img src="/front/img/logo.png" alt="" width="20%", height="20%">
                            <p>MASSARI S.R.L</p>
                            <div class="footer-desc-contact">
                                <h6>@lang('words.contact_title'): </h6>
                                <ul>
                                    <li><i class="fa fa-whatsapp"></i>+595 981 592 950</li>
                                    <li><i class="fa fa-envelope-o"></i><span>email: </span> massaripy@gmail.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-links">
                            <h4 class="footer-title">Enlaces de Interes</h4>
                            <ul class="clearfix">
                                <li><a href="{{ route('index') }}">Inicio</a></li>
                                <li><a href="{{ route('front.about') }}">Nosotros</a></li>
                                <li><a href="{{ route('store.index') }}">Productos</a></li>
                                <li><a href="{{ route('login.page') }}">Inicio de sesion</a></li>
                                <li><a href="{{ route('login.page') }}">Registro</a></li>
                                <li><a href="{{ route('front.contact.show') }}">Contacto</a></li>
                                <li><a href="{{ route('front.faq') }}">Preguntas Frecuentes</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="popular-tag">
                            <h4 class="footer-title">Busquedas Populares</h4>
                            <ul class="footer-tags">
                                <li><a href="{{ route('store.search', ['search' => 'perfume']) }}">Perfume</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'rondina']) }}">Rondina</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'cartera']) }}">Carteras</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'calvin']) }}">Calvin Klein</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'madres']) }}">Mamás</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'peine'])}}">Peluquería</a></li>
                                <li><a href="#">Maquillaje</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright">
                            <p>No encontraste algun producto ?<a href="https://api.whatsapp.com/send?phone=595981592950&amp;text=No%20encontre%20el%20producto%20que%20buscaba%21" target="_blank"> Contactanos!</a></p>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="footer-social">
                            <ul>
                                <li><a href="https://www.facebook.com/massariparaguay/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/massariparaguay/"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://api.whatsapp.com/send?phone=595981592950&amp;text=Hola%21%20Estoy%20escribiendo%20desde%20la%20Web%20de%20Massari!"
                                       target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="payment-method">
                            <ul>
                                <li><img src="{{ '/front/img/payment-method/american.png' }}" alt=""></li>
                                <li><img src="{{ '/front/img/payment-method/mastercard.png' }}" alt=""></li>
                                <li><img src="{{ '/front/img/payment-method/visa.png' }}" alt=""></li>
                                <li><img src="{{ '/front/img/payment-method/cabal.png' }}" alt="" style="height: 23px; width: 45px"></li>

                                <li><img src="{{ '/front/img/tigo.png' }}" alt="" style="height: 23px; width: 50px"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright">
                            <p>© 2018 Todos los Derechos Reservados. <a href="#" target="_blank">MASSARI S.R.L</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>