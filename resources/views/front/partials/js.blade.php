<!-- Scripts -->
<script src="/front/js/jquery-3.2.0.min.js"></script>
<script src="/front/js/jquery.bpopup.min.js"></script>
<script src="/front/js/owl.carousel.min.js"></script>
<script src="/front/js/jquery.counterup.min.js"></script>
<script src="/front/js/countdown.js"></script>
<script src="/front/js/jquery.magnific-popup.min.js"></script>
<script src="/front/js/jquery.fancybox.min.js"></script>
<script src="/front/js/jquery.meanmenu.js"></script>
<script src="/front/js/jquery.nivo.slider.pack.js"></script>
<script src="/front/js/jquery.scrollUp.js"></script>
<script src="/front/js/jquery.mixitup.min.js"></script>
<script src="/front/js/jquery.waypoints.min.js"></script>
<script src="/front/js/jquery-ui.js"></script>
<script src="/front/js/bootstrap.min.js"></script>
<script src="/front/js/theme.js"></script>

<script>
    // Mobile devices only
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        // some code..
        $(".login-or-register").css('margin-right', '3px');
        $('.top-cart > span i').css("margin-right", '5px');
        $("#header_search_box").prependTo($("#move_search_box"));
        $('#search').css('width', '200px');
        $(".htr-search").css({
            width: '300px',
            'margin-right': '-60px',

        });
        $('#search_input').css({
            width: '345px',
            height: '40px'
        });
        $('#send-button').css('height', '70px');
        $('#now-button').css('height', '70px');
        $('#new_address_map').css('display', 'none');
        $('.edit_address_map').css('display', 'none');
        $('#scrollUp').css({
            bottom: '75px',
            right: '20px'
        });
        $('.htr-search-content').css('right', '53px');
        $('.htr-search .htr-search-content button').css('right', '-140px');
        $('.login-register-content').css({
            left: '-35px',
            width: '265px'
        })
        ;
        $('.table_not_show').css('display', 'none');
        $('.table-padding').css({
            'padding-left': '5px',
            'padding-right': '5px'
        });
        $('.show_user').css({
            'padding-left' : '0px',
            'padding-right': '0px'
        });

        $('.owl-prev').css('margin-left', '-5px');
        $('.owl-next').css('margin-right', '10px');

        $('.shop-sidebar-area').css('display', 'none');
    } else {
        $('.br_login').css('display', 'none');
    }
</script>

<style>
    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        margin-left: -20px;
        display: block !IMPORTANT;
        border: 0px solid black;
    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -25px;
        display: block !IMPORTANT;
        border: 0px solid black;
    }

    .owl-prev i, .owl-next i {
        transform: scale(1, 6);
        color: #ccc;
    }

    /*.owl-prev {*/
    /*display: none;*/
    /*}*/
    /*.owl-next {*/
    /*display: none;*/
    /*}*/
    /*.disabled {*/
    /*display: none !important;*/
    /*}*/
</style>