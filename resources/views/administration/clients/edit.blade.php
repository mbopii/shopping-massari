@extends('administration.templates.layout') 
@section('content')
<!-- #END# Vertical Layout -->
<!-- Vertical Layout | With Floating Label -->
{!! Form::model($client, ['route' => ['clients.update', $client->id], 'method' => 'patch']) !!}

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Editar Cliente Casual {{--
                    <small>With floating label</small>--}}
                </h2>
            </div>
            <div class="body">
    @include('administration.clients.partials.fields')
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Dirección {{--
                    <small>With floating label</small>--}}
                </h2>
            </div>
            <div class="body">
                <div class="form-group form-float">
                    <div class="form-line">
                        {{ Form::text('address', isset($client->addresses[0]->address) ? $client->addresses[0]->address : null, ['class' => 'form-control'])
                        }}
                        <label class="form-label">Calle 1</label>
                    </div>
                </div>

                <div class="form-group form-float">
                    <div class="form-line">
                        {{ Form::text('address2', isset($client->addresses[0]->address2) ? $client->addresses[0]->address2 : null, ['class' => 'form-control'])
                        }}
                        <label class="form-label">Calle2</label>
                    </div>
                </div>

                <div class="form-group form-float">
                    <div class="form-line">
                        {{ Form::text('number', isset($client->addresses[0]->number) ? $client->addresses[0]->number : null, ['class' => 'form-control'])
                        }}
                        <label class="form-label">Numero de casa</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        {{ Form::text('telephone', isset($client->addresses[0]->telephone) ? $client->addresses[0]->telephone : null, ['class' => 'form-control'])
                        }}
                        <label class="form-label">Telefono</label>
                    </div>
                </div>
                <div class="form-line">
                    <label id="brand_id">Ciudad</label> {!! Form::select('city_id' , $cities , isset($client->addresses[0]->city_id)
                    ? $client->addresses[0]->city_id : null, ['class' => 'form-control show-tick', 'required' => 'required'])
                    !!}
                </div>
                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>

            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<!-- Vertical Layout | With Floating Label -->
@endsection
 
@section('css')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}" rel="stylesheet"
/>

<!-- Wait Me Css -->
<link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet" />
@endsection
 
@section('js')
<!-- Moment Plugin Js -->
<script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

<script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
@endsection