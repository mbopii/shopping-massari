@extends('administration.templates.layout') 
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Cliente Casual
                    <small>Nro: {{ $client->id}}</small>
                </h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-2">
                        <p>
                            <b>Fecha de Alta</b>
                        </p>
                        <p>{{ $client->created_at->format('Y-m-d') }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Descripción:</b>
                        </p>
                        <p>{{$client->description }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Cedula:</b>
                        </p>
                        <p>{{ $client->idnum }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Ruc:</b>
                        </p>
                        <p>{{$client->tax_name }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Razon Social:</b>
                        </p>
                        <p>{{$client->tax_code }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Telefono:</b>
                        </p>
                        <p>{{$client->telephone }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Fecha de Nacimiento:</b>
                        </p>
                        <p>{{$client->birthday }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Dirección:</b>
                        </p>
                        <p>{{$client->addresses[0]->address }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Dirección:</b>
                        </p>
                        <p>{{$client->addresses[0]->address2 }}</p>
                    </div>
                    <div class="col-md-2">
                        <p>
                            <b>Ciudad:</b>
                        </p>
                        <p>{{$client->addresses[0]->cities->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Compras
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha de Compra</th>
                            <th>Estado</th>
                            <th>Monto</th>
                            <th>Forma de Pago</th>
                            <th>Aex</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($client->purchases as $s)
                        <tr>
                            <th>{{ $s->id }}</th>
                            <th>{{ $s->purchase_date }}</th>
                            <th>{{ $s->statuses->description }}</th>
                            <th>{{ number_format($s->order_amount, 0, ',', '.') }}</th>
                            <th>{{ $s->paymentForm->description}}</th>
                            <th>{{ $s->aex_tracking_id }}</th>
                            <th>
                                <div class="icon-button-demo">
                                    <a href="{{ route('sales.show', $s->id) }}">
                                <button class="btn btn-info waves-effect" type="button">
                                    <i class="material-icons">zoom_in</i>
                                </button>
                            </a>
                                </div>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection