@extends('administration.templates.layout') 
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Clientes Casuales
                    <small>Filtros de Busqueda</small>
                </h2>
            </div>
            <div class="body">
                <form action="{{ route('casual_clients.index') }}">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    {!! Form::text('date_start', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                    <label class="form-label">Desde</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    {!! Form::text('date_end', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                    <label class="form-label">Hasta</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
                                    <label class="form-label">Descripción</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <input type="hidden" name="q" value="q">
                            <button type="submit" class="btn btn-info">Filtrar</button> {{--
                            <button class="btn btn-success" name="download" value="xls">Excel</button>--}}
                            <a href="{{ route('casual_clients.index') }}" class="btn btn-warning">Ver Todos</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Teléfono</th>
                            <th>Creado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $c)
                        <tr class="even pointer" data-id="{{ $c->id }}">
                            <th scope="row">{{ $c->id }}</th>
                            <td>{{ $c->description }}</td>
                            <td>{{ $c->idnum }}</td>
                            <td>{{ $c->telephone }}</td>
                            <td>{{ $c->created_at }}</td>
                            <td>
                                <div class="icon-button-demo">
                                    <a href="{{ route('casual_clients.show', $c->id) }}">
                                        <button class="btn btn-info waves-effect" type="button">
                                            <i class="material-icons">zoom_in</i>
                                        </button>
                                    </a>

                                    <a href="{{ route('casual_clients.edit', $c->id) }}">
                                        <button class="btn btn-warning waves-effect" type="button">
                                            <i class="material-icons">border_color</i>
                                        </button>
                                    </a>

                                    <button class="btn btn-danger waves-effect btn-delete" type="button" title="Cancelar Orden">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $clients->appends(['date_start' => $date_start, 'q' => $q, 'date_end' => $date_end, 'description' => $description])->links()
                }}
            </div>
        </div>
    </div>
</div>
{!! Form::open(['route' => ['casual_clients.destroy',':ROW_ID'], 'method' => 'DELETE',
'id' => 'form-delete']) !!}
<!-- #END# Basic Table -->
@endsection
 
@section('css')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}" rel="stylesheet"
/>

<!-- Wait Me Css -->
<link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet" />
@endsection
 
@section('js')
<!-- Sweetalert Css -->
<link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet" />
<!-- Moment Plugin Js -->
<script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

<script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

<script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
@include('administration.templates.partials.delete_row')

@endsection