<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('description', null, ['class' => 'form-control']) }}
        <label class="form-label">Descripción</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('idnum', null, ['class' => 'form-control']) }}
        <label class="form-label">Cedula de Identidad</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('tax_name', null, ['class' => 'form-control']) }}
        <label class="form-label">Razon Social</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('tax_code', null, ['class' => 'form-control']) }}
        <label class="form-label">Ruc</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('birthday', null, ['class' => 'datepicker form-control']) }}
        <label class="form-label">Fecha de Nacimiento</label>
    </div>
</div>