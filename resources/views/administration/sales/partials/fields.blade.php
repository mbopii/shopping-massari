<div class="row clearfix">
    <div class="col-md-3 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <label for="cc-payment" class="control-label mb-1">Cliente</label>
                {!! Form::text('client_idnum', null, ['class' => 'form-control', 'id' => 'client_idnum']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-1">
        <br>
        <div class="form-group">
            <button class="btn btn-primary m-t-15 waves-effect" id="add_client">Nuevo!</button>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                <label for="cc-payment" class="control-label mb-1">Formas de Pago</label>
                {!! Form::text('payment_form_id', null, ['class' => 'form-control', 'id' => 'select_payments_forms']) !!}
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                <label for="cc-payment" class="control-label mb-1">Compra Interna</label>
                <select id="inside_purchase" class=" form-control" name="inside_purchase" required>
                    <option value="0">No</option>
                    <option value="1">Si</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="form-line">
                <label for="cc-payment" class="control-label mb-1">Fecha de Entrega</label>
                {!! Form::text('reception_date', null, ['class' => 'sales_datepicker form-control', 'id' => 'description']) !!}
            </div>
        </div>
    </div>

    <div id="time_selection">
        <div class="col-md-4">
            <div class="form-group">
                <div class="form-line">
                    <label for="cc-payment" class="control-label mb-1">Desde</label>
                    <input type="text" id="reception_time_from" class="time start form-control" name="reception_time_from" required>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="form-line">
                    <label for="cc-payment" class="control-label mb-1">Hasta</label>
                    <input type="text" id="reception_time_to" class="time end form-control" name="reception_time_to" required>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="client_details">
    <div class="col-lg-2 col-md-3 col-xs-4">
        <label for="client_name">Nombre:</label>
        <label for="client_name" id="client_client_name"></label>
        <br>
        <label for="client_name">Cedula:</label>
        <label for="client_name" id="client_client_idnum"></label>
    </div>
    <div class="col-lg-2 col-md-3 col-xs-4">
        <label for="client_name">Razon Social:</label>
        <label for="client_name" id="client_tax_name"></label>
        <br>
        <label for="client_name">Ruc:</label>
        <label for="client_name" id="client_tax_code"></label>
    </div>

    <div class="col-lg-3 col-md-3 col-xs-4">
        <label for="client_name">Calle 1:</label>
        <label for="client_name" id="client_address_1"></label>
        <br>
        <label for="client_name">Calle 2:</label>
        <label for="client_name" id="client_address_2"></label>
    </div>
    <div class="col-lg-2 col-md-3 col-xs-4">
        <label for="client_name">Referencias:</label>
        <label for="client_name" id="client_references"></label>
        <br>
        <label for="client_name">Numero de casa:</label>
        <label for="client_name" id="client_house_number"></label>
    </div>
    <div class="col-lg-2  col-md-3 col-xs-4">
        <label for="client_name">Ciudad:</label>
        <label for="client_name" id="client_city"></label>
    </div>
</div>