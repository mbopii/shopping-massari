<div class="row clearfix">

    <div class="col-md-12">

        <table id="complex_table" class="table table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Cantidad</td>
                <td>Precio de Venta</td>
                <td>Precio con descuento</td>
                <td>Precio Especial</td>
                <td>Descuento</td>
                <td>Subtotal</td>
                <td>Acciones</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>


    <br>

    <div class="modal fade" id="productsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agregar Producto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <p>
                                <b>Elija un producto</b>
                            </p>
                            <div class="row">
                                <select id="select_product" name="select_product" class="form-control show-tick"
                                        data-live-search="true">
                                    <option value="0">Seleccione</option>
                                    @foreach($productsJson as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label id="role_id">Cantidad</label>
                                            {!! Form::text('select_quantity' , null , ['class' => 'form-control', 'id' => 'select_quantity']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-float">--}}
                                        {{--<div class="form-line">--}}
                                            {{--<label id="role_id">Precio de Compra</label>--}}
                                            {{--{!! Form::text('purchase_price' , null , ['class' => 'form-control', 'id' => 'purchase_price']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="add_complex" class="btn btn-link waves-effect">Agregar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
