@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Venta
                        <small>Filtros de Busqueda</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('sales.index') }}">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_start', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Desde</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_end', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Hasta</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('user_description', null, ['class' => 'form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Cliente</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-line">
                                    <label id="role_id">Medio de Pago</label>
                                    {!! Form::select('payment_method_id' , $paymentForms , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="hidden" name="q" value="q">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                                {{--<button class="btn btn-success" name="download" value="xls">Excel</button>--}}
                                <a href="{{ route('sales.index') }}" class="btn btn-warning">Ver Todos</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Ventas
                        <small>Cargadas en el sistema</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha de Venta</th>
                            <th>Monto</th>
                            <th>Cliente</th>
                            <th>Metodo de Pago</th>
                            <th>Estado</th>
                            <th>Creado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($salesHeader as $s)
                            <tr class="even pointer" data-id="{{ $s->id }}">
                                <th scope="row">{{ $s->id }}</th>
                                <td>{{ $s->purchase_date }}</td>
                                <td>{{ number_format($s->order_amount + $s->delivery_cost, 0, ',', '.') }}</td>
                                @if (!is_null($s->client_id))
                                    <td>Venta Rápida</td>
                                @else
                                    <td>{{ $s->client }}</td>
                                @endif
                                <td>{{ $s->payment_form }}</td>
                                <td>{{ $s->status }}</td>
                                <td>{{ $s->created_at }}</td>
                                <td>
                                    <div class="icon-button-demo">
                                        <a href="{{ route('sales.show', $s->id) }}">
                                            <button class="btn btn-info waves-effect" type="button">
                                                <i class="material-icons">zoom_in</i>
                                            </button>
                                        </a>
                                    </div>
                                    @if(!is_null($s->client_id) && $s->status_id == 1)
                                        <div class="icon-button-demo">
                                            <a href="{{ route('sales.confirm', $s->id) }}">
                                                <button class="btn btn-warning waves-effect" type="button">
                                                    <i class="material-icons">done</i>
                                                </button>
                                            </a>
                                        </div>
                                    @endif
                                    @if($s->status_id == 1)
                                        <div class="icon-button-demo">
                                            <a href="{{ route('sales.cancel', $s->id) }}">
                                                <button class="btn btn-danger waves-effect" type="button">
                                                    <i class="material-icons">delete_forever</i>
                                                </button>
                                            </a>
                                        </div>
                                    @endif
                                    @if($s->status_id == 3)
                                    <div class="icon-button-demo">
                                        <a href="{{ route('sales.confirm.cancel', $s->id) }}">
                                            <button class="btn btn-danger waves-effect" type="button" title="Cancelar Orden">
                                                <i class="material-icons">delete_forever</i>
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $salesHeader->appends(['date_start' => $date_start, 'q' => $q, 'date_end' => $date_end, 'description' => $description])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
@endsection


@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
@endsection

@section('js')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
@endsection
