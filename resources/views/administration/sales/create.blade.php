@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    {!! Form::open(['route' => ['sales.store'], 'method' => 'post', 'files' => 'true', 'id' => 'saleForm']) !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nueva Venta
                        <small>Para Clientes no registrados en la plataforma</small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.sales.partials.fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalles
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <button type="button" data-toggle="modal" data-target="#productsModal"
                                        class="btn btn-warning m-t-15 waves-effect">Agregar Producto
                                </button>
                                <button class="btn btn-success m-t-15 waves-effect delivery">Calcular Delivery</button>

                                <button type="submit" class="btn btn-primary m-t-15 waves-effect finish_sell">Guardar
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                Gs. <label class="sub_total">0</label>
                                Delivery: Gs. <label class="sub_delivery">0</label>
                                Proveedor Delivery: <label class="delivery_provider">--</label>
                                Cobranza AEX: <label class="delivery_answer">--</label>
                                Total: Gs. <label class="all_total">0</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.sales.partials.fields_table')
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    {{--Modal para Nuevo Cliente--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="new-client-modal" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="new_client" class="modal-content form-horizontal" action="{{ route('clients.store') }}"
                      method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Nuevo Cliente</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <input type="hidden" name="sales" value="sales">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nombre</label>
                                {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'new_client_description', 'required ']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Cedula</label>
                                {!! Form::text('idnum' , null , ['class' => 'form-control', 'id' => 'new_client_idnum']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Teléfono</label>
                                {!! Form::text('telephone' , null , ['class' => 'form-control', 'id' => 'new_client_telephone']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Razon Social</label>
                                {!! Form::text('tax_name' , null , ['class' => 'form-control', 'id' => 'new_client_tax_name']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Ruc</label>
                                {!! Form::text('tax_code' , null , ['class' => 'form-control', 'id' => 'new_client_tax_code']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Calle 1</label>
                                {!! Form::text('address' , null , ['class' => 'form-control', 'id' => 'new_client_address']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Calle 2</label>
                                {!! Form::text('address2' , null , ['class' => 'form-control', 'id' => 'new_client_address2']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nro de Casa</label>
                                {!! Form::text('number' , null , ['class' => 'form-control', 'id' => 'new_client_number']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Referencias</label>
                                {!! Form::text('references' , null , ['class' => 'form-control', 'id' => 'new_client_references']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="role_id">Ciudad</label>
                            {!! Form::text('city_id' , null , ['class' => 'form-control', 'id' => 'select_city']) !!}
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success show_new_city_modal">Agregar Ciudad</button>
                        <button class="btn btn-primary add_new_client">Agregar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->

    <div class="modal fade" tabindex="-1" role="dialog" id="new-city-modal" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="new_client" class="modal-content form-horizontal" action="{{ route('clients.store') }}"
                      method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Nueva Ciudad</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <input type="hidden" name="sales" value="sales">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nombre</label>
                                {!! Form::text('new_city_description' , null , ['class' => 'form-control', 'id' => 'new_city_description']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Departamento</label>
                                {!! Form::text('department_id' , null , ['class' => 'form-control', 'id' => 'select_departments', 'required ']) !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary add_new_city">Agregar</button>
                            <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ '/front/css/jquery.timepicker.min.css' }}">
    <link rel="stylesheet" href="{{ '/admin/css/selectize/selectize.css' }}">
    <style>
        .option {
            background-color: white;
        }
    </style>
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ '/admin/plugins/data-table/css/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/admin/plugins/data-table/css/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/admin/plugins/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>

@endsection

@section('js')
    <script src="{{ '/front/js/jquery.timepicker.min.js' }}"></script>
    <script type="text/javascript" src="{{ '/admin/js/selectize/selectize.min.js' }}"></script>
    <script>

        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var xhr;
        $('#time_selection .time').timepicker({
            minTime: '9:00am',
            maxTime: '18:00pm',
            timeFormat: 'H:i',
        });
        $('.time').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
            if ((event.which < 48 || event.which > 60)) {
                event.preventDefault();
            }
        });
        $('#new_client_number').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('#new_client_telephone').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $(".show_new_city_modal").click(function (e) {
            e.preventDefault();
            $('#new_city_description').val("");
            $('#select_departments').val("");
            $("#new-city-modal").modal()

        });

        $('#add_client').click(function (e) {
            e.preventDefault();

            $('#new_client_description').val('');
            $('#new_client_idnum').val('');
            $('#new_client_telephone').val('');
            $('#new_client_birthday').val('');
            $('#new_client_tax_name').val('Sin Nombre');
            $('#new_client_tax_code').val('0');

            $('#new_client_address').val('');
            $('#new_client_address2').val('');
            $('#new_client_references').val('');
            $('#new_client_number').val('');
            $('#select_city').val('');

            $("#new-client-modal").modal()

        });

        $('#select_payments_forms').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },

            options: {!! $paymentMethods !!}
        });

        $('#select_departments').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },

            options: {!! $departmentsJson !!}
        });
        var client_list = $('#client_idnum').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            onChange(value) {
                $("#preloader").show();
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: rootURL + 'administration/clients/search/' + value,
                    method: "GET",
                    success: function (result) {
                        var response = JSON.parse(result);
                        if (response.error == false) {
                            $('#client_tax_code').text(response.client.tax_code);
                            $('#client_tax_name').text(response.client.tax_name);
                            $('#client_client_idnum').text(response.client.idnum);
                            $('#client_client_name').text(response.client.description);

                            var address = JSON.parse(response.client.addresses);
                            $('#client_address_1').text(address.address);
                            $('#client_address_2').text(address.address2);
                            $('#client_references').text(address.references);
                            $('#client_house_number').text(address.number);
                            $('#client_city').text(response.client.my_city);


                        }
                    }
                });
            },
            options: {!! $clientsJson !!}
        });

        var cities_list = $('#select_city').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $citiesJson !!}
        });

        $(".add_new_city").on('click', function (e) {
            e.preventDefault();
            console.log('Add new City');
            var city_description = $('#new_city_description').val();
            var department_id = $('#select_departments').val();

            if (city_description == "") {
                alert("Debe agregar un nombre a la nueva ciudad");
                return;
            }

            if (department_id == "") {
                alert("Debe delegir un departamento");
                return;
            }

            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + 'administration/cities/add_sales',
                method: "POST",
                data: {
                    description: city_description,
                    department_id: department_id,
                    _token: "{{ csrf_token() }}",
                },
                success: function (result) {
                    console.log(result);
                    var response = JSON.parse(result);
                    if (response.error == false) {
                        cities_list[0].selectize.addOption({
                            id: response.data.id,
                            description: city_description
                        });
                        cities_list[0].selectize.refreshOptions(false);
                        cities_list[0].selectize.setValue(response.data.id);
                        $("#new-city-modal").modal('toggle');
                    }
                },
                error: function (result) {
                }

            });

        });

        $('.add_new_client').on('click', function (e) {
            e.preventDefault();
            console.log('Add new Client');

            var client_description = $('#new_client_description').val();
            var client_id_num = $('#new_client_idnum').val();
            var client_telephone = $('#new_client_telephone').val();
            // var client_birthday = $('#new_client_birthday').val();
            var client_tax_name = $('#new_client_tax_name').val();
            var client_tax_code = $('#new_client_tax_code').val();

            var client_address = $('#new_client_address').val();
            var client_address_2 = $('#new_client_address2').val();
            var client_references = $('#new_client_references').val();
            var client_house_number = $('#new_client_number').val();
            var client_city_id = $('#select_city').val();

            console.log(client_description);
            if (client_description == "") {
                alert("Debe agregar un nombre al cliente");
                return;
            }

            if (client_id_num == "") {
                alert("Debe agregar un numero de cedula al cliente");
                return;
            }

            if (client_telephone == "") {
                alert("Debe agregar un numero de telefono al cliente");
                return;
            }


            if (client_tax_name == "") {
                alert("Debe agregar una razon social para el cliente");
                return;
            }

            if (client_tax_code == "") {
                alert("Debe agregar un ruc para el cliente");
                return;
            }

            if (client_address == "") {
                alert("Debe agregar un nombre de calle para la direccion");
                return;
            }

            if (client_address_2 == "") {
                alert("Debe agregar un nombre de calle para la direccion");
                return;
            }

            if (client_house_number == "") {
                alert("Debe agregar una numero de casa para esta direccion");
                return;
            }

            if (client_city_id == "") {
                alert("Debe seleccionar una ciudad");
                return;
            }

            if (client_references == "") {
                client_references = '';
            }

            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + 'administration/clients/add_sales',
                method: "POST",
                data: {
                    description: client_description,
                    idnum: client_id_num,
                    telephone: client_telephone,
                    tax_name: client_tax_name,
                    tax_code: client_tax_code,
                    address: client_address,
                    address2: client_address_2,
                    number: client_house_number,
                    city_id: client_city_id,
                    references: client_references,
                    _token: "{{ csrf_token() }}",
                },
                success: function (result) {
                    console.log(result);
                    var response = JSON.parse(result);
                    if (response.error == false) {
                        client_list[0].selectize.addOption({
                            id: response.data.id,
                            description: client_description
                        });
                        client_list[0].selectize.refreshOptions(false);
                        client_list[0].selectize.setValue(response.data.id);
                        $("#new-client-modal").modal('toggle');
                    }
                },
                error: function (result) {
                }

            });
        });

        $('.delivery').on('click', function (e) {
            e.preventDefault();
            var product = [];
            var quantity = [];
            var i = 0;
            var j = 0;

            $('input[name^="product_id"]').each(function () {
                product[i] = $(this).val();
                i++;
            });

            $('input[name^="quantity"]').each(function () {
                quantity[j] = $(this).val();
                j++;
            });
            var client_id = $("#client_idnum").val();

            if (client_id == "") {
                alert("Debe seleccionar un cliente");
                return;
            }
            xhr = $.ajax({
                url: rootURL + 'administration/sales/delivery',
                method: "POST",
                data: {
                    products: product,
                    quantities: quantity,
                    client_id: client_id,
                    _token: "{{ csrf_token() }}",
                },
                success: function (result) {
                    console.log(result);
                    $(".sub_delivery").text(result.delivery.cost);
                    $(".all_total").text(result.delivery.cost + parseInt($(".sub_total").text()));
                    if (result.delivery.collection == true) {
                        $(".delivery_answer").text("Si");
                    } else {
                        $(".delivery_answer").text("NO");
                    }
                    if (result.delivery.text) {
                        $(".delivery_provider").text("AEX");
                    } else {
                        $(".delivery_provider").text("Transportadora Tercerizada");
                    }
                },
                error: function (result) {
                }

            });
        });

        $(".finish_sell").on('click', function (e) {
            e.preventDefault();
            if ($("#select_payments_forms").val() == "") {
                alert("Debe elegir una forma de pago");
                return;
            }
            
            if ($(".sub_total").text() == "0") {
                alert("Debe agregar productos para poder vender");
                return;
            }

            if ($(".time").val() == "") {
                alert("Debe seleccionar un rango de horario para entregar");
                return;
            }

            if ($(".sales_datepicker").val() == "") {
                alert("Debe seleccionar una fecha de entrega");
                return;
            }

            if ($('#inside_purchase').val() == 0){
                if ($(".sub_delivery").text() == '0') {
                    alert("Debe calcular el costo del delivery antes de enviar el formulario");
                    return;
                }
            }

            $("#saleForm").submit();

        });
    </script>
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script type="text/javascript" src="{{ '/admin/plugins/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/admin/plugins/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/admin/plugins/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        $('#select_quantity').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


        $(document).ready(function () {
            var table = $('#complex_table').DataTable({
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin resultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                },
                pageLength: 50,
                // keys: true,
                columns: [
                    {data: "id"},
                    {data: 'product'},
                    {data: 'quantity', className: 'products_quantity'},
                    {data: 'amount'},
                    {data: 'discount_price', className: "discount_price"},
                    {data: "new_price", className: "alter_price"},
                    {data: 'discount'},
                    {data: 'subtotal'},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<button class="btn btn-danger waves-effect btn-delete remove_product" type="button"><i class="material-icons">delete_forever</i></button>';
                        }
                    }
                ]
            });

            $('#add_complex').on("click", function (e) {
                e.preventDefault();
                // $("#preloader").show();

                var product_id = $('#select_product').val();
                var product_name = $("#select_product option:selected").text();
                var quantity = $('#select_quantity').val();

                if (!$('#select_quantity').val()) {
                    alert('Debe asignar una cantidad al producto');
                    return;
                }

                // Traemos el precio de la mercaderia
                xhr = $.ajax({
                    url: rootURL + 'administration/products/single/' + product_id,
                    method: "GET",
                    success: function (result) {
                        var response = JSON.parse(result);
                        var product = response.product;
                        $('#productsModal').modal('toggle');
                        var selling_price = product.price;
                        let currentSubTotal = $('.sub_total');
                        var subtotal = 0;
                        var discount = 0;
                        var productDiscount = 0;
                        console.log(product.discount_quantity2);
                        if (product.discount_quantity2 == 0 || !product.discount_quantity2){
                            console.log("Discount quantity2 is null or 0");
                            if (product.discount && parseInt(quantity) >= parseInt(product.discount_quantity)  ) {
                                console.log("Discount 1");
                                productDiscount = selling_price - ((selling_price * product.discount_percentage) /100);
                                discount = (selling_price * quantity) * (product.discount_percentage / 100);
                                subtotal = selling_price * quantity - discount;
                            }else{
                                console.log("No Discount");
                                subtotal = selling_price * quantity;
                                discount = 0;
                                productDiscount  = 0;
                            }
                        } else if (product.discount && parseInt(quantity) >= parseInt(product.discount_quantity) && parseInt(quantity) < product.discount_quantity2) {
                            console.log("Discount 1");
                            productDiscount = selling_price - ((selling_price * product.discount_percentage) /100);
                            discount = (selling_price * quantity) * (product.discount_percentage / 100);
                            subtotal = selling_price * quantity - discount;                       
                        } else if (parseInt(quantity) >= product.discount_quantity2 && parseInt(quantity) && product.discount_quantity2 > 0) {
                            console.log("Discount 2");
                            productDiscount = selling_price - ((selling_price * product.discount_percentage2) /100);
                            discount = (selling_price * quantity) * (product.discount_percentage2 / 100);
                            subtotal = selling_price * quantity - discount;
                        } else {
                            console.log("No Discount");
                            subtotal = selling_price * quantity;
                            discount = 0;
                            productDiscount  = 0;
                        }

                        currentSubTotal.text(parseInt(currentSubTotal.text()));
                        currentSubTotal.text(parseInt(currentSubTotal.text()) + parseInt(subtotal));

                        table.row.add({
                                id: product_id,
                                product: product_name + '<input type="hidden" name="product_id[]" value="' + product_id + '">' +
                                    '<input type="hidden" class="quantity" name="quantity[]" value="' + quantity + '">' +
                                    '<input type="hidden" class="price" name="price[]" value="' + selling_price + '">' +
                                    '<input type="hidden" class="new_price" name="new_price[]" value="0">',
                                quantity: quantity,
                                amount: selling_price,
                                discount_price: productDiscount,
                                new_price: "",
                                discount: discount,
                                subtotal: subtotal,
                            }
                        ).draw(false);

                        $('#select_quantity').val('');
                        $('#select_product').val(0);
                    },
                    error: function (result) {

                    },
                });

            });

            $('#complex_table tbody').on('click', '.remove_product', function () {
                let data = table.row($(this).parents('tr')).data();
                table.row($(this).parents("tr")).remove().draw();
                $('.sub_total').text(parseInt($('.sub_total').text()) - parseInt(data.subtotal));
            });

            function myCallBackFunction(updatedCell, updatedRow, oldValue) {
                console.log("The new value for the cell is: " + updatedCell.data());
                console.log("The values for each cell in that row are: " + updatedRow.data());
            }


            $(document).on('change', '.products_quantity', function () {
                let row = table.row($(this).parents('tr'));
                let product_id = table.cell(row, 0).data();

                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
                console.log("Change quantity");
                updateQuantity(rootURL + 'administration/products/single/' + product_id, row, table, $(this))

            });

             $(document).on('change', '.alter_price', function () {
                 let row = table.row($(this).parents('tr'));
                 let product_id = table.cell(row, 0).data();
            
                 $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                     event.preventDefault();
                 }
                 console.log("Change Price");
                 // let customPrice = $(this).
            
                 updatePrice(rootURL + 'administration/products/single/' + product_id, row, table, $(this));
            
             });
            
             function updatePrice(url, row, table, cell){
                 // Primero ponemos los descuentos a 0
                 table.cell(row, 6).data(0).draw();
                 // Traemos el nuevo monto
                 let newQuantity = table.cell(row, 2).data();
                 $("#preloader").show();
                 xhr = $.ajax({
                     url: url,
                     method: "GET",
                     success: function (result) {
                        var subtotal = 0;
                        let newPrice = table.cell(row, 5).data();
                        if ((parseInt(newPrice) == 0) || newPrice == ""){
                            // Debemos devolver a los valores por defecto.
                            // Verificamos si hay descuento
                            var discount = table.cell(row, 4).data();
                            console.log(discount);
                            if (parseInt(discount) > 0){
                                newPrice = table.cell(row, 4).data();
                                let customPrice = cell.parents('tr').find('.new_price');
                                customPrice.val(newPrice);
                            } else {
                                newPrice = table.cell(row, 3).data();
                                // Ceramos la variable oculta para no joder con los descuentos
                                let customPrice = cell.parents('tr').find('.new_price');
                                customPrice.val(0);
                            }
                            console.log(newPrice);
                            // Actualizamos el subTotal
                            subtotal = newPrice * newQuantity; 
                            
                        }else{                           
                            // Seteamos el valor hidden new_price con el nuevo precio
                            let customPrice = cell.parents('tr').find('.new_price');
                            customPrice.val(newPrice);
                            // Actualizamos el subTotal
                            subtotal = newPrice * newQuantity; 
                            
                        }
                        // Actualizamos subTotal de la tabla o total del producto
                        let oldSubTotal = table.cell(row, 7).data();
                        let subTotal = $('.sub_total');
                        let s = parseInt(subTotal.text()) - parseInt(oldSubTotal);
                        subTotal.text(parseInt(s + (subtotal)));

                        //Actualizamos el subTotal de la fila
                        table.cell(row, 7).data(parseInt(subtotal)).draw();

                        // Eliminamos variable del deliery para poder recalcular
                        var oldDeliveryValue = $(".sub_delivery").text();
                        $(".all_total").text("0");
                        $(".delivery_answer").text("--");
                        $(".delivery_provider").text("--");
                        $(".sub_delivery").text(0);
                     }
                 });
             }

            function updateQuantity(url, row, table, cell) {

                xhr && xhr.abort();
                $("#preloader").show();
                xhr = $.ajax({
                    url: url,
                    method: "GET",
                    success: function (result) {
                        var response = JSON.parse(result);
                        var product = response.product;
                        let customPrice = table.cell(row, 5).data();

                        let newQuantity = table.cell(row, 2).data();
                        let productPrice = product.price;

                        var discount = 0;
                        var subtotal = 0;
                        var productDiscount = 0;
                        
                        // Verificamos el valor de la columna de precio especial
                        if (customPrice == ""){
                            if (product.discount_quantity2 == 0){
                                if (product.discount && parseInt(newQuantity) >= parseInt(product.discount_quantity)  ) {
                                    console.log("Discount 1");
                                    productDiscount = productPrice - ((productPrice * product.discount_percentage) /100);
                                    discount = (productPrice * newQuantity) * (product.discount_percentage / 100);
                                    subtotal = productPrice * newQuantity - discount;
                                } else{
                                    console.log("No Discount");
                                    subtotal = productPrice * newQuantity;
                                    discount = 0;
                                    productDiscount  = 0;
                                }
                            } else if (product.discount && parseInt(newQuantity) >= parseInt(product.discount_quantity) && parseInt(newQuantity) && parseInt(newQuantity) < product.discount_quantity2) {
                                console.log("Discount 1");
                                productDiscount = productPrice - ((productPrice * product.discount_percentage) /100);
                                discount = (productPrice * newQuantity) * (product.discount_percentage / 100);
                                subtotal = productPrice * newQuantity - discount;                       
                            } else if (parseInt(newQuantity) >= product.discount_quantity2 && parseInt(newQuantity) && product.discount_quantity2 > 0) {
                                console.log("Discount 2");
                                productDiscount = productPrice - ((productPrice * product.discount_percentage2) /100);
                                discount = (productPrice * newQuantity) * (product.discount_percentage2 / 100);
                                subtotal = productPrice * newQuantity - discount;
                            } else {
                                console.log("No Discount");
                                subtotal = productPrice * newQuantity;
                                discount = 0;
                                productDiscount  = 0;
                            }
                        }else{
                            subtotal = customPrice * newQuantity;
                        }
                
                        // Actualizamos columna de descuentos
                        table.cell(row, 6).data(parseInt(discount)).draw();
                        table.cell(row, 4).data(parseInt(productDiscount)).draw();

                        let oldSubTotal = table.cell(row, 7).data();
                        // Actualizamos subTotal de la fila
                        table.cell(row, 7).data(parseInt(subtotal)).draw();

                        // Actualizamos subTotal de la tabla o total del producto
                        let subTotal = $('.sub_total');
                        let s = parseInt(subTotal.text()) - parseInt(oldSubTotal);
                        subTotal.text(parseInt(s + (subtotal)));

                        // Actualizamos los valores ocultos
                        cell.parents('tr').find('.quantity').val(newQuantity);
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        })

                        // Eliminamos variable del deliery para poder recalcular
                        var oldDeliveryValue = $(".sub_delivery").text();
                        $(".all_total").text("0");
                        $(".delivery_answer").text("--");
                        $(".delivery_provider").text("--");
                        $(".sub_delivery").text(0);

                    },
                    error: function () {
                        alert("this is not working");
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                    }
                });
            }

            table.MakeCellsEditable({
                "onUpdate": myCallBackFunction,
                "inputCss": 'form-control',
                "columns": [2, 5],
                "allowNulls": {
                    "columns": [2, 5],
                    "errorClass": 'error'
                },
                "inputTypes": [
                    {
                        "column": 2,
                        "type": "text",
                        "options": null
                    },
                    {
                        "column": 5,
                        "type": "number",
                        "options": null
                    }
                ]
            });
        });

        $('.sales_datepicker').bootstrapMaterialDatePicker({
            disabledDays: [7],
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false,
            lang: 'es',
            minDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
        });
    </script>
@endsection