@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Venta
                        <small>Nro: {{ $sale->id}}</small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-2">
                            <p>
                                <b>Fecha de Venta</b>
                            </p>
                            <p>{{ $sale->purchase_date }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Usuario:</b>
                            </p>
                            <p>{{$sale->clients->description }}</p>
                        </div>
                        @if (isset($sale->client_id))
                            <div class="col-md-2">
                                <p>
                                    <b>Cliente:</b>
                                </p>
                                <p>{{$sale->noClient->description }}</p>
                            </div>
                        @endif
                        <div class="col-md-2">
                            <p>
                                <b>Cedula de Identidad:</b>
                            </p>
                            <p>@if(isset($sale->client_id)){{ $sale->noClient->idnum }}@else {{ $sale->clients->idnum }}@endif</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Orden:</b>
                            </p>
                            <p>Gs. {{ number_format($sale->order_amount, 0, ',', '.') }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Delivery:</b>
                            </p>
                            <p>Gs. {{ number_format($sale->delivery_cost, 0, ',', '.') }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Total:</b>
                            </p>
                            <p>Gs. {{ number_format($sale->order_amount + $sale->delivery_cost, 0, ',', '.') }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Proveedor de servicio de delivery:</b>
                            </p>
                            <p>@if ($sale->aex_request_id) AEX @elseif($sale->delivery_cost > 0) Transportadora @else Compra Interna @endif</p>
                        </div>
                        @if ($sale->aex_tracking_id)
                        <div class="col-md-2">
                            <p>
                                <b>Guia AEX:</b>
                            </p>
                            <p>{{ $sale->aex_tracking_id}}</p>
                        </div>
                        @endif
                        <div class="col-md-2">
                            <p>
                                <b>Metodo de Pago:</b>
                            </p>
                            <p>{{ $sale->paymentForm->description}}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Ciudad</b>
                            <p>@if (isset($sale->client_id)){{ $sale->noClient->addresses[0]->cities->description }} @else{{ $sale->addresses->cities->description }}@endif</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Dirección de Entrega</b>
                            <p>@if (isset($sale->client_id)){{ $sale->noClient->addresses[0]->address }} @else{{ $sale->addresses->address }}@endif</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Horario de Entrega</b>
                            <p>{!! $sale->reception_time_from !!} a {!! $sale->reception_time_to !!}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Estado:</b>
                            </p>
                            <p>{{ $sale->statuses->description}}</p>
                        </div>
                        @if ($sale->payment_method_id == 3)
                            <div class="col-md-2">
                                <p>
                                    <b>Boleta de Deposito</b>
                                </p>

                                <div class="icon-button-demo">
                                    <a href="#">
                                        <button class="btn btn-info waves-effect" data-toggle="modal"
                                                data-target="#myModal" type="button">
                                            <i class="material-icons">camera_alt</i>
                                        </button>
                                    </a>
                                    @if ($sale->status_id == 1)
                                        <a href="{{ route('sales.confirm', $sale->id) }}">
                                            <button class="btn btn-success waves-effect" type="button">
                                                <i class="material-icons">done</i>
                                            </button>
                                        </a>
                                        <a href="{{ route('sales.cancel', $sale->id) }}">
                                            <button class="btn btn-danger waves-effect" type="button">
                                                <i class="material-icons">delete_forever</i>
                                            </button>
                                        </a>
                                    @endif

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 850px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Foto</h4>
                </div>
                <div class="modal-body">
                    @if(file_exists(public_path("/front/img/purchases/{$sale->id}.jpg")))
                        <img src="{{ "/front/img/purchases/{$sale->id}.jpg" }}" alt="Factura"
                             style="width: 800px; height:600px;">
                    @else
                        <h2>Sin Foto Cargada</h2>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalle
                    </h2>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Categoría</th>
                            <th>Marca</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Descuento</th>
                            <th>Total Descuento</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sale->details as $s)
                            <tr class="even pointer" data-id="{{ $s->id }}">
                                <th scope="row">{{ $s->id }}</th>
                                <td>{{ $s->product->name }}</td>
                                <td>{{ $s->product->categories->description }}</td>
                                <td>{{ $s->product->brand->description }}</td>
                                <td>{{ $s->quantity }}</td>
                                <td>{{ number_format($s->unit_price, 0, ',', '.') }}</td>
                                <td>@if($s->discount)Si @else No @endif</td>
                                <td>{{ number_format($s->total_discount, 0, ',', '.') }}</td>
                                <td>{{ number_format($s->sub_total, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection