@extends('administration.templates.layout')
@section('content')
    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('products.index') }}">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">PRODUCTOS</div>
                        <div class="number count-to" data-from="0" data-to="{{ $products }}" data-speed="15"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('sales.index') }}">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">ORDENES</div>
                        <div class="number count-to" data-from="0" data-to="{{ $orders }}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('purchases.index') }}">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">COMPRAS</div>
                        <div class="number count-to" data-from="0" data-to="{{ $purchases }}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('users.index') }}">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">USUARIOS</div>
                        <div class="number count-to" data-from="0" data-to="{{ $users }}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    {{--<!-- #END# Widgets -->--}}
    <div class="row clearfix">
        <!-- Visitors -->

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-blue">
                    <div class="font-bold m-b--35">Productos Mas Visitados</div>
                    <ul class="dashboard-stat-list">
                        @if (is_object($mostVisited))
                            @foreach ($mostVisited as $visit)
                                <li>
                                    {{ $visit->name }}
                                    <span class="pull-right"><b>{{ $visit->views }}</b> </span>
                                </li>
                            @endforeach
                        @else
                            <li>
                                {{ $mostVisited }}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-teal">
                    <div class="font-bold m-b--35">Productos Mas Vendidos</div>
                    <ul class="dashboard-stat-list">
                        @if (is_object($mostSale))
                            @foreach ($mostSale as $sale)
                                <li>
                                    {{ $sale->product->name }}
                                    <span class="pull-right"><b>{{ $sale->p_quantity }}</b> </span>
                                </li>
                            @endforeach
                        @else
                            <li>
                                {{ $mostSale }}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-blue-grey">
                    <div class="font-bold m-b--35">Ranking por Proveedores</div>
                    <ul class="dashboard-stat-list">
                        @if (is_object($mostProvider))
                            @foreach ($mostProvider as $most)
                                <li>
                                    {{$most->providers->description}}
                                    <span class="pull-right"><small>Gs. </small><b>{{ number_format($most->p_sub_total, 0, ',', '.') }}</b> </span>
                                </li>
                            @endforeach
                        @else
                            <li>
                                {{ $mostProvider }}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    {{--<!-- #END# Visitors -->--}}
    {{--<!-- Latest Social Trends -->--}}
    {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
    {{--<div class="card">--}}
    {{--<div class="body bg-cyan">--}}
    {{--<div class="m-b--35 font-bold">LATEST SOCIAL TRENDS</div>--}}
    {{--<ul class="dashboard-stat-list">--}}
    {{--<li>--}}
    {{--#socialtrends--}}
    {{--<span class="pull-right">--}}
    {{--<i class="material-icons">trending_up</i>--}}
    {{--</span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--#materialdesign--}}
    {{--<span class="pull-right">--}}
    {{--<i class="material-icons">trending_up</i>--}}
    {{--</span>--}}
    {{--</li>--}}
    {{--<li>#adminbsb</li>--}}
    {{--<li>#freeadmintemplate</li>--}}
    {{--<li>#bootstraptemplate</li>--}}
    {{--<li>--}}
    {{--... #freehtmltemplate--}}
    {{--<span class="pull-right">--}}
    {{--<i class="material-icons">trending_up</i>--}}
    {{--</span>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Latest Social Trends -->--}}
    {{--<!-- Answered Tickets -->--}}
    {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
    {{--<div class="card">--}}
    {{--<div class="body bg-teal">--}}
    {{--<div class="font-bold m-b--35">ANSWERED TICKETS</div>--}}
    {{--<ul class="dashboard-stat-list">--}}
    {{--<li>--}}
    {{--TODAY--}}
    {{--<span class="pull-right"><b>12</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--YESTERDAY--}}
    {{--<span class="pull-right"><b>15</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--LAST WEEK--}}
    {{--<span class="pull-right"><b>90</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--LAST MONTH--}}
    {{--<span class="pull-right"><b>342</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--LAST YEAR--}}
    {{--<span class="pull-right"><b>4 225</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--ALL--}}
    {{--<span class="pull-right"><b>8 752</b> <small>TICKETS</small></span>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Answered Tickets -->--}}
    {{--</div>--}}

    {{--<div class="row clearfix">--}}
    {{--<!-- Task Info -->--}}
    {{--<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">--}}
    {{--<div class="card">--}}
    {{--<div class="header">--}}
    {{--<h2>TASK INFOS</h2>--}}
    {{--<ul class="header-dropdown m-r--5">--}}
    {{--<li class="dropdown">--}}
    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--<i class="material-icons">more_vert</i>--}}
    {{--</a>--}}
    {{--<ul class="dropdown-menu pull-right">--}}
    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="body">--}}
    {{--<div class="table-responsive">--}}
    {{--<table class="table table-hover dashboard-task-infos">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th>#</th>--}}
    {{--<th>Task</th>--}}
    {{--<th>Status</th>--}}
    {{--<th>Manager</th>--}}
    {{--<th>Progress</th>--}}
    {{--</tr>--}}
    {{--</thead>--}}
    {{--<tbody>--}}
    {{--<tr>--}}
    {{--<td>1</td>--}}
    {{--<td>Task A</td>--}}
    {{--<td><span class="label bg-green">Doing</span></td>--}}
    {{--<td>John Doe</td>--}}
    {{--<td>--}}
    {{--<div class="progress">--}}
    {{--<div class="progress-bar bg-green" role="progressbar" aria-valuenow="62"--}}
    {{--aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>--}}
    {{--</div>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>2</td>--}}
    {{--<td>Task B</td>--}}
    {{--<td><span class="label bg-blue">To Do</span></td>--}}
    {{--<td>John Doe</td>--}}
    {{--<td>--}}
    {{--<div class="progress">--}}
    {{--<div class="progress-bar bg-blue" role="progressbar" aria-valuenow="40"--}}
    {{--aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>--}}
    {{--</div>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>3</td>--}}
    {{--<td>Task C</td>--}}
    {{--<td><span class="label bg-light-blue">On Hold</span></td>--}}
    {{--<td>John Doe</td>--}}
    {{--<td>--}}
    {{--<div class="progress">--}}
    {{--<div class="progress-bar bg-light-blue" role="progressbar" aria-valuenow="72"--}}
    {{--aria-valuemin="0" aria-valuemax="100" style="width: 72%"></div>--}}
    {{--</div>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>4</td>--}}
    {{--<td>Task D</td>--}}
    {{--<td><span class="label bg-orange">Wait Approvel</span></td>--}}
    {{--<td>John Doe</td>--}}
    {{--<td>--}}
    {{--<div class="progress">--}}
    {{--<div class="progress-bar bg-orange" role="progressbar" aria-valuenow="95"--}}
    {{--aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>--}}
    {{--</div>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>5</td>--}}
    {{--<td>Task E</td>--}}
    {{--<td>--}}
    {{--<span class="label bg-red">Suspended</span>--}}
    {{--</td>--}}
    {{--<td>John Doe</td>--}}
    {{--<td>--}}
    {{--<div class="progress">--}}
    {{--<div class="progress-bar bg-red" role="progressbar" aria-valuenow="87"--}}
    {{--aria-valuemin="0" aria-valuemax="100" style="width: 87%"></div>--}}
    {{--</div>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--</tbody>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Task Info -->--}}
    {{--<!-- Browser Usage -->--}}
    {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
    {{--<div class="card">--}}
    {{--<div class="header">--}}
    {{--<h2>BROWSER USAGE</h2>--}}
    {{--<ul class="header-dropdown m-r--5">--}}
    {{--<li class="dropdown">--}}
    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--<i class="material-icons">more_vert</i>--}}
    {{--</a>--}}
    {{--<ul class="dropdown-menu pull-right">--}}
    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="body">--}}
    {{--<div id="donut_chart" class="dashboard-donut-chart"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Browser Usage -->--}}
    {{--</div>--}}
    @endsection

    @section('css')
        <!-- Morris Chart Css-->
            <link href="{{ '/admin/plugins/morrisjs/morris.css' }}" rel="stylesheet"/>
    @endsection

    @section('js')
        <!-- Jquery CountTo Plugin Js -->
            <script src="{{ '/admin/plugins/jquery-countto/jquery.countTo.js' }}"></script>
            <!-- Morris Plugin Js -->
            <script src="{{ '/admin/plugins/raphael/raphael.min.js' }}"></script>
            <script src="{{ '/admin/plugins/morrisjs/morris.js' }}"></script>
            <!-- ChartJs -->
            <script src="{{ '/admin/plugins/chartjs/Chart.bundle.js' }}"></script>
            <!-- Flot Charts Plugin Js -->
            <script src="{{ '/admin/plugins/flot-charts/jquery.flot.js' }}"></script>
            <script src="{{ '/admin/plugins/flot-charts/jquery.flot.resize.js' }}"></script>
            <script src="{{ '/admin/plugins/flot-charts/jquery.flot.pie.js' }}"></script>
            <script src="{{ '/admin/plugins/flot-charts/jquery.flot.categories.js' }}"></script>
            <script src="{{ '/admin/plugins/flot-charts/jquery.flot.time.js' }}"></script>
            <!-- Sparkline Chart Plugin Js -->
            <script src="{{ '/admin/plugins/jquery-sparkline/jquery.sparkline.js' }}"></script>

            <script src="{{ '/admin/js/pages/index.js' }}"></script>
@endsection
