@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    {!! Form::open(['route' => ['purchases.store'], 'method' => 'post', 'files' => 'true']) !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nueva Compra
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.purchases.partials.fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalles
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-float">
                                <button type="button" data-toggle="modal" data-target="#productsModal"
                                        class="btn btn-primary m-t-15 waves-effect">Agregar Producto
                                </button>
                            </div>
                        </div>
                        <div class="col-md-offset-4 col-md-4">
                            <div class="form-group form-float">
                            Gs. <label class="sub_total">0</label>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-md-12">
                            @include('administration.purchases.partials.fields_table')
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ '/admin/plugins/data-table/css/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/admin/plugins/data-table/css/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/admin/plugins/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>

@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script type="text/javascript" src="{{ '/admin/plugins/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/admin/plugins/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/admin/plugins/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        $('#select_quantity').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('#purchase_price').on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var xhr;
        $(document).ready(function () {
            var table = $('#complex_table').DataTable({
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin resultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                },
                pageLength: 50,
                // keys: true,
                columns: [
                    {data: "id"},
                    {data: 'product'},
                    {data: 'quantity',  className: 'products_quantity'},
                    {data: 'amount'},
                    {data: 'subtotal'},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<button class="btn btn-danger waves-effect btn-delete remove_product" type="button"><i class="material-icons">delete_forever</i></button>';
                        }
                    }
                ]
            });

            $('#add_complex').on("click", function (e) {
                e.preventDefault();
                // $("#preloader").show();

                var product_id = $('#select_product').val();
                var product_name = $("#select_product option:selected").text();
                var quantity = $('#select_quantity').val();
                var purchase_price = $('#purchase_price').val();
                if (product_id == 0){
                    alert('Debe seleccionar un producto');
                    return;
                }

                if (!$('#select_quantity').val()){
                    alert('Debe asignar una cantidad al producto');
                    return;
                }

                if (!$('#purchase_price').val()){
                    alert('Debe asignar un precio de compra');
                    return;
                }

                $('#productsModal').modal('toggle');

                let currentSubTotal = $('.sub_total');
                currentSubTotal.text(parseInt(currentSubTotal.text()));
                currentSubTotal.text(parseInt(currentSubTotal.text()) + parseInt(quantity * purchase_price));

                table.row.add({
                        id: product_id,
                        product: product_name + '<input type="hidden" name="purchase_product_id[]" value="' + product_id + '">' +
                            '<input type="hidden" class="purchase_quantity" name="purchase_quantity[]" value="' + quantity + '">' +
                            '<input type="hidden" class="purchase_price" name="purchase_unit_price[]" value="' + purchase_price + '">',
                        amount: purchase_price,
                        quantity: quantity,
                        subtotal: quantity * purchase_price,
                    }
                ).draw(false);

                $('#select_quantity').val('');
                $('#select_product').val('');
                $('#purchase_price').val('');
            });

            $('#complex_table tbody').on('click', '.remove_product', function () {
                let data = table.row($(this).parents('tr')).data();
                table.row($(this).parents("tr")).remove().draw();
                $('.sub_total').text(parseInt($('.sub_total').text()) - parseInt(data.subtotal));
            });

            function myCallBackFunction(updatedCell, updatedRow, oldValue) {
                console.log("The new value for the cell is: " + updatedCell.data());
                console.log("The values for each cell in that row are: " + updatedRow.data());
                console.log(oldValue);
            }


            $(document).on('change', '.products_quantity', function () {

                let row = table.row($(this).parents('tr'));
                let hiddenQuantity = $(this).parents('tr').find('.purchase_quantity');
                let quantity = table.cell(row, 2).data(); // 5ta columna cantidad

                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
                console.log("Change quantity");
                updateQuantity(row, table, $(this))

            });

            function updateQuantity(row, table, cell) {
                xhr && xhr.abort();
                $("#preloader").show();
                xhr = $.ajax({
                    url: rootURL + 'administration/products/single/' + 1,
                    method: "GET",
                    success: function (result) {
                        let newQuantity = table.cell(row, 2).data();
                        let productAmount = table.cell(row, 3).data();
                        let oldSubTotal = table.cell(row, 4).data();
                        // Actualizamos subTotal de la fila
                        table.cell(row, 4).data(parseInt(productAmount * newQuantity)).draw();

                        // Actualizamos subTotal de la tabla o total del producto
                        let subTotal = $('.sub_total');
                        let s = parseInt(subTotal.text()) - parseInt(oldSubTotal);
                        subTotal.text(parseInt(s + (productAmount * newQuantity)));

                        // Actualizamos los valores ocultos
                        cell.parents('tr').find('.purchase_quantity').val(newQuantity);
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        })
                    },
                    error: function () {
                        alert("this is not working");
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                    }
                });
            }

                $('#select_quantity').on("keypress keyup blur",function (event) {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            table.MakeCellsEditable({
                "onUpdate": myCallBackFunction,
                "inputCss": 'form-control',
                "columns": [2],
                "allowNulls": {
                    "columns": [2],
                    "errorClass": 'error'
                },
                "inputTypes": [
                    {
                        "column": 2,
                        "type": "text",
                        "options": null
                    }
                ]
            });
        });
    </script>
@endsection