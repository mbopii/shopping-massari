<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label id="role_id">Proveedor</label><br>
                {!! Form::select('provider_id' , $providers , null, ['class' => 'form-control show-tick', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label id="role_id">Fecha de Compra</label>
                {!! Form::text('purchase_date', null, ['class' => 'datepicker form-control','required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label id="role_id">Numero de Factura</label>
                {!! Form::text('invoice_number' , null , ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label id="role_id">Timbrado</label>
                {!! Form::text('stamping' , null , ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
    </div>
</div>

