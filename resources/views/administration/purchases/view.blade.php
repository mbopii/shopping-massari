@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Compras
                        <small>Factura Nro: {{ $purchase->invoice_number }}</small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-2">
                            <p>
                                <b>Fecha de Compra</b>
                            </p>
                            <p>{{ $purchase->purchase_date }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Total:</b>
                            </p>
                            <p>{{ number_format($purchase->total_amount, 0, ',', '.') }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Factura Nro:</b>
                            </p>
                            <p>{{ $purchase->invoice_number }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Timbrado</b>
                            </p>
                            <p>{{ $purchase->stamping }}</p>
                        </div>
                        <div class="col-md-2">
                            <p>
                                <b>Usuario:</b>
                            </p>
                            <p>{{ $purchase->user->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalle
                    </h2>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio Unitario</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($purchase->purchaseDetails as $p)
                            <tr class="even pointer" data-id="{{ $p->id }}">
                                <th scope="row">{{ $p->id }}</th>
                                <td>{{ $p->product->name }}</td>
                                <td>{{ $p->quantity }}</td>
                                <td>{{ number_format($p->single_amount, 0, ',', '.') }}</td>
                                <td>{{ number_format($p->total_amount, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection