@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Usuarios
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('users.create') }}">Nuevo</a></li>
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th>Email</th>
                            <th>Nombre de Usuario</th>
                            <th>Creado</th>
                            <th>Modificado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $u)
                            <tr class="even pointer" data-id="{{ $u->id }}">
                                <th scope="row">{{ $u->id }}</th>
                                <td>{{ $u->description }}</td>
                                <td>{{ $u->email }}</td>
                                <td>----</td>
                                <td>{{ $u->created_at }}</td>
                                <td>{{ $u->updated_at }}</td>
                                <td>
                                    <div class="icon-button-demo">
                                        <a href="{{ route('users.edit', $u->id) }}">
                                            <button class="btn btn-info waves-effect" type="button">
                                                <i class="material-icons">border_color</i>
                                            </button>
                                        </a>
                                        <button class="btn btn-danger waves-effect btn-delete" type="button">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->

    {!! Form::open(['route' => ['users.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet" />
@endsection
@section('js')
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection
