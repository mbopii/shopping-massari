@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Usuario
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                    @include('administration.users.partials.fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet" />
@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
@endsection