<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Datos Personales</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('idnum' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Cedula</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('email' , null , ['class' => 'form-control email', 'id' => 'description']) !!}
        <label class="form-label">Email</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('username' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Nombre de Usuario</label>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::password('password', ['class' => 'form-control', 'id' => 'description']) !!}
                <label class="form-label">Contraseña</label>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'description']) !!}
                <label class="form-label">Confirmar Contraseña</label>
            </div>
        </div>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('address' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Dirección</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('telephone' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Telefono</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('birthday' , null , ['class' => 'datepicker form-control', 'id' => 'description']) !!}
        <label class="form-label">Fecha de Nacimiento</label>
    </div>
</div>

{{--<div class="form-group form-float">--}}
    <div class="form-line">
        <label id="role_id">Roles</label>
        {!! Form::select('role_id' , $rolesList , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
    </div>
{{--</div>--}}
{{--<input type="checkbox" id="remember_me_2" class="filled-in">--}}
{{--<label for="remember_me_2">Remember Me</label>--}}
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>