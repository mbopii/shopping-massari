@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Reportes
                        <small></small>
                    </h2>
                </div>
                <div class="body">
                    <ul>
                        <li><a href="{{ route('reports.sales') }}">Ventas</a></li>
                        <li><a href="{{ route('reports.stock') }}">Stock</a></li>
                        <li><a href="{{ route('reports.aex') }}">Aex</a></li>
                        <li><a href="{{ route('reports.utilities') }}">Utilidad</a></li>
                        {{--<li><a href="{{ route('reports.utilities.monthly') }}">Utilidad mensual</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

