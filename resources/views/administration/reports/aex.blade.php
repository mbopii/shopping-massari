@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Aex
                        <small>Criterios de Búsqueda</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('reports.aex') }}">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_start', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Desde</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_end', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Hasta</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-line">
                                    <label id="role_id">Medio de Pago</label>
                                    {!! Form::select('payment_method_id' , $paymentForms , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="hidden" name="q" value="q">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                                <button class="btn btn-group" name="download" value="pdf">PDF</button>
                                <button class="btn btn-success" name="download" value="xls">Excel</button>
                                <a href="{{ route('reports.aex') }}" class="btn btn-warning">Ver Todos</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                {{--<div class="header">--}}
                {{--<h2>--}}
                {{--Detalles--}}
                {{--<small>de la factura seleccionada</small>--}}
                {{--</h2>--}}
                {{--</div>--}}
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Fecha de Compra</th>
                            <th>Monto</th>
                            <th>Delivery</th>
                            <th>Guia AEX</th>
                            <th>Pago 3%</th>
                            <th>Estado</th>
                            <th>Forma de Pago</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales as $sale)
                            <tr class="even pointer" data-id="{{ $sale->id }}">
                                <th scope="row">{{ $sale->id }}</th>
                                <td>{{ $sale->clients->description }}</td>
                                <td>{{ $sale->purchase_date }}</td>
                                <td>{{ number_format($sale->order_amount, 0, ',', '.') }}</td>
                                <td>{{ number_format($sale->delivery_cost, 0, ',', '.') }}</td>
                                <td>{{ $sale->aex_tracking_id }}</td>
                                <td>{{ number_format($sale->aex_collect_amount, 0, ',', '.') }}</td>
                                <td>{{ $sale->statuses->description }}</td>
                                <td>{{ $sale->paymentForm->description }}</td>
                                <td>
                                    <div class="icon-button-demo">
                                        <a href="{{ route('reports.aex.get_delivery', $sale->id) }}">
                                            <button class="btn btn-success waves-effect" type="button" title="Cobrar Monto AEX AEX" @if ($sale->aex_payment) disabled @endif>
                                                <i class="material-icons">payment</i>
                                            </button>
                                        </a>
                                        <a href="{{ route('reports.aex.pay_collect', $sale->id) }}">
                                            <button class="btn btn-warning waves-effect" type="button" title="Pagar Cobro pedidos AEX" @if ($sale->aex_collect) disabled @endif>
                                                <i class="material-icons">done</i>
                                            </button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $sales->appends(['date_start' => $date_start, 'date_end' => $date_end,
                     'payment_method_id' => $payment_method_id, 'q' => $q])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 850px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Foto</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
@endsection