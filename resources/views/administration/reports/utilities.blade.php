@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Utilidades
                        <small>Criterios de Búsqueda</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('reports.utilities') }}">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Producto</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-line">
                                    <label id="role_id">Categorías</label>
                                    {!! Form::select('category_id' , $categories , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="hidden" name="q" value="q">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                                <button class="btn btn-group" name="download" value="pdf">PDF</button>
                                <button class="btn btn-success" name="download" value="xls">Excel</button>
                                <a href="{{ route('reports.utilities') }}" class="btn btn-warning">Ver Todos</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                {{--<div class="header">--}}
                {{--<h2>--}}
                {{--Detalles--}}
                {{--<small>de la factura seleccionada</small>--}}
                {{--</h2>--}}
                {{--</div>--}}
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Producto</th>
                            <th>Categoría</th>
                            <th>Costo Promedio</th>
                            <th>Precio de Venta</th>
                            <th>Ganancia</th>
                            <th>Ganancia (%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr class="even pointer" data-id="{{ $product->id }}">
                                <th scope="row">{{ $product->id }}</th>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ number_format($product->avg, 0, ',', '.') }}</td>
                                <td>{{ number_format($product->price, 0, ',', '.') }}</td>
                                <td>{{ number_format($product->price - $product->avg, 0, ',', '.') }}</td>
                                <td>{{ number_format((($product->price - $product->avg)/$product->price) * 100, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->appends(['description' => $description, 'category_id' => $category_id,
                      'q' => $q])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 850px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Foto</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
@endsection