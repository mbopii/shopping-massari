@extends('administration.reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Producto</th>
            <th>Marca</th>
            <th>Tipo de Producto</th>
            <th>Disponible</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $product)
            <tr class="even pointer" data-id="{{ $product->id }}">
                <th scope="row">{{ $product->id }}</th>
                <td>{{ $product->name }}</td>
                <td>{{ $product->brand->description }}</td>
                <td>{{ ($product->commplex_product) ? 'Compuesto' : 'Simple' }}</td>
                <td>{{ $product->quantity }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection