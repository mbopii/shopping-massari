@extends('administration.reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Producto</th>
            <th>Categoría</th>
            <th>Costo Promedio</th>
            <th>Precio de Venta</th>
            <th>Ganancia</th>
            <th>Ganancia (%)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $product)
            <tr class="even pointer" data-id="{{ $product->id }}">
                <th scope="row">{{ $product->id }}</th>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ number_format($product->avg, 0, ',', '.') }}</td>
                <td>{{ number_format($product->price, 0, ',', '.') }}</td>
                <td>{{ number_format($product->price - $product->avg, 0, ',', '.') }}</td>
                <td>{{ number_format((($product->price - $product->avg)/$product->price) * 100, 2, ',', '.') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection