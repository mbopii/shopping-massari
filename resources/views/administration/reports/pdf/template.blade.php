<html>
<head>
    <title>REPORTE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ public_path('/admin/css/custom-bootstrap-reportes.min.css') }}" rel="stylesheet">
    @section('styles')
        <style>
            td
            {
                text-align: center;
                font-size:0.6em !important;
            }
            th
            {
                text-align: center;
                font-size:0.7em !important;
            }
        </style>
    @show
</head>

<body>
<div class="container-fluid">

    @section('header')
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th style="vertical-align: top;">{{ isset($reportName) ? $reportName : "Reporte" }} <br> {{ date("d/m/Y H:i") }} Hs.</th>
                <th>
                    <img src="{{ public_path('/front/img//logo.png') }}" width="50px" height="30px">
                    <br>
                    Massari S.R.L
                </th>
            </tr>
            </thead>
        </table>
    @show

    @yield('content')

</div>


<script type="text/php">
            if ( isset($pdf) )
            {
                $x = 750;
                $y = 18;
                $text = "pagina {PAGE_NUM} de {PAGE_COUNT}";
                $font = $fontMetrics->get_font("helvetica", "bold");
                $size = 6;
                $color = array(0,0,0);
                $word_space = 0.0;  //  default
                $char_space = 0.0;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
            }
		</script>
@section('scripts')
@show

</body>
</html>