@extends('administration.reports.excel.template')

@section('content')
    <tr>
        <th>ID</th>
        <th>Producto</th>
        <th>Categoría</th>
        <th>Costo Promedio</th>
        <th>Precio de Venta</th>
        <th>Ganancia</th>
        <th>Ganancia (%)</th>
    </tr>
    @foreach($products as $product)
        <tr class="even pointer" data-id="{{ $product->id }}">
            <th scope="row">{{ $product->id }}</th>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->avg }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->price - $product->avg}}</td>
            <td>{{ (($product->price - $product->avg)/$product->price) * 100 }}</td>
        </tr>
    @endforeach
@endsection