@extends('administration.reports.excel.template')

@section('content')
    <tr>
        <th>#</th>
        <th>Cliente</th>
        <th>Fecha de Compra</th>
        <th>Monto</th>
        <th>Forma de Pago</th>
        <th>AEX</th>
        <th>Marca</th>
        <th>Categoría</th>
        <th>Producto(s)</th>
    </tr>
    @foreach($data as $sale)
        @php ($first = true)
        @foreach($sale->details as $detail)
            <tr class="even pointer" data-id="{{ $sale->id }}">
                @if ($first)
                    <th rowspan="{{ $sale->details->count() }}">{{ $sale->id }}</th>
                    <td rowspan="{{ $sale->details->count() }}"> @if (isset($sale->client_id)) {{$sale->noClient->description }} @else{{ $sale->clients->description }}@endif</td>
                    <td rowspan="{{ $sale->details->count() }}">{{ $sale->purchase_date }}</td>
                    <td rowspan="{{ $sale->details->count() }}">{{ $sale->order_amount + $sale->delivery_cost }}</td>
                    <td rowspan="{{ $sale->details->count() }}">{{ $sale->paymentForm->description }}</td>
                    <td rowspan="{{ $sale->details->count() }}">@if(isset($sale->aex_tracking_id)) {{ $sale->aex_tracking_id }} @else
                            --- @endif</td>
                    @php ($first = false)
                @endif
                <td>{{ $detail->product->brand->description }}</td>
                <td>{{ $detail->product->categories->description }}</td>
                <td>
                    {{ $detail->product->name }}
                </td>
            </tr>
        @endforeach
    @endforeach
@endsection