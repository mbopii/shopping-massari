@extends('administration.reports.excel.template')

@section('content')
    <tr>
        <th>#</th>
        <th>Producto</th>
        <th>Marca</th>
        <th>Tipo de Producto</th>
        <th>Disponible</th>
    </tr>
    @foreach($data as $product)
        <tr class="even pointer" data-id="{{ $product->id }}">
            <th scope="row">{{ $product->id }}</th>
            <td>{{ $product->name }}</td>
            <td>{{ $product->brand->description }}</td>
            <td>{{ ($product->commplex_product) ? 'Compuesto' : 'Simple' }}</td>
            <td>{{ $product->quantity }}</td>
        </tr>
    @endforeach
@endsection