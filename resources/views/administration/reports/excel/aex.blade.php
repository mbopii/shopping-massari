@extends('administration.reports.excel.template')

@section('content')
    <tr>
        <th>#</th>
        <th>Cliente</th>
        <th>Fecha de Compra</th>
        <th>Monto</th>
        <th>Delivery</th>
        <th>Guia AEX</th>
        <th>Pago 3%</th>
        <th>Estado</th>
        <th>Forma de Pago</th>
    </tr>
    <tbody>
    @foreach($data as $sale)
        <tr class="even pointer" data-id="{{ $sale->id }}">
            <th scope="row">{{ $sale->id }}</th>
            <td>{{ $sale->clients->description }}</td>
            <td>{{ $sale->purchase_date }}</td>
            <td>{{ number_format($sale->order_amount, 0, ',', '.') }}</td>
            <td>{{ number_format($sale->delivery_cost, 0, ',', '.') }}</td>
            <td>{{ $sale->aex_tracking_id }}</td>
            <td>{{ number_format($sale->aex_collect_amount, 0, ',', '.') }}</td>
            <td>{{ $sale->statuses->description }}</td>
            <td>{{ $sale->paymentForm->description }}</td>
        </tr>
    @endforeach
@endsection