<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
        <label class="form-label">Nombre</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('slug', null, ['class' => 'form-control']) }}
        <label class="form-label">Identificador</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {{ Form::text('description', null, ['class' => 'form-control']) }}
        <label class="form-label">Descripción</label>
    </div>
</div>

<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
