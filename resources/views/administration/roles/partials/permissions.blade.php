{{--<div class="row clearfix">--}}
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Permisos
                    <small>Seleccione los permisos a asignar al rol
                    </small>
                </h2>
            </div>
            <div class="body">
                <h2 class="card-inside-title"></h2>
                <div class="demo-checkbox">
                    <table>
                        <tr>
                            @for($i = 0; $i < $permissions->count(); $i++)
                                <td style="padding-right: 15px">
                                    <input type="checkbox" name="permissions[]" id="{{ $permissions[$i]->id }}"
                                           class="filled-in" @if($permissions[$i]->has) checked
                                           @endif value="{{ $permissions[$i]->permission }}"/>
                                    <label for="{{ $permissions[$i]->id }}"> {{ $permissions[$i]->description }} </label>
                                </td>
                                @if(($i+1) % 4 == 0)
                        </tr>
                        <tr>
                            @endif
                            @endfor
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
{{--</div>--}}
