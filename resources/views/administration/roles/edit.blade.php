@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Rol
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    @include('administration.roles.partials.fields')
                </div>
            </div>
        </div>

        @include('administration.roles.partials.permissions')
    </div>
    {{ Form::close() }}
    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet" />
@endsection

@section('js')
    <!-- Autosize Plugin Js -->
    <script src="{{ '/admin/plugins/autosize/autosize.js' }}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>
@endsection