<!-- Jquery Core Js -->
<script src="{{ '/admin/plugins/jquery/jquery.min.js' }}"></script>
<!-- Bootstrap Core Js -->
<script src="{{ '/admin/plugins/bootstrap/js/bootstrap.js' }}"></script>
<!-- Select Plugin Js -->
<script src="{{ '/admin/plugins/bootstrap-select/js/bootstrap-select.js' }}"></script>
<!-- Slimscroll Plugin Js -->
<script src="{{ '/admin/plugins/jquery-slimscroll/jquery.slimscroll.js' }}"></script>
<!-- Waves Effect Plugin Js -->
<script src="{{ '/admin/plugins/node-waves/waves.js' }}"></script>
<!-- Autosize Plugin Js -->
<script src="{{ '/admin/plugins/autosize/autosize.js' }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ '/admin/plugins/bootstrap-notify/bootstrap-notify.js' }}"></script>

<!-- Custom Js -->
<script src="{{ '/admin/js/admin.js' }}"></script>
<script src="{{ '/admin/js/pages/ui/dialogs.js' }}"></script>
{{--<!-- Demo Js -->--}}
<script src="{{ '/admin/js/demo.js' }}"></script>