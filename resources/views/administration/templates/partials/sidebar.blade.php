<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ '/admin/images/user.png' }}" width="48" height="48" alt="Usuario"/>
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true"
                 aria-expanded="false">@if (\Sentinel::getUser()){{ \Sentinel::getUser()->description }} @endif</div>
            <div class="email">@if (\Sentinel::getUser()){{ \Sentinel::getUser()->email }}@endif</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
{{--                    @if (\Sentinel::getUser())--}}
{{--                        <li><a href="{{ route('users.show', \Sentinel::getUser()->id) }}"><i class="material-icons">person</i>Perfil</a>--}}
                        {{--</li>@endif--}}
                    {{--<li role="seperator" class="divider"></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>--}}
                    <li role="seperator" class="divider"></li>
                    <li><a href="{{ url('/') }}"><i class="material-icons">shopping_cart</i>Tienda</a></li>
                    <li><a href="{{ route('logout') }}"><i class="material-icons">input</i>Salir</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">Menu Principal</li>
            <li class="{{ Request::is('administration') ? 'active' : '' }}">
                <a href="{{ route('admin.index') }}">
                    <i class="material-icons">pie_chart</i>
                    <span>Inicio</span>
                </a>
            </li>

            <li @if( Request::is('administration/categories*'))
                class="active" @endif>
                <a href="{{ route('categories.index') }}">
                    <i class="material-icons">bookmark</i>
                    <span>Categorías</span>
                </a>
            </li>
            <li @if( Request::is('administration/brands*'))
                class="active" @endif>
                <a href="{{ route('brands.index') }}">
                    <i class="material-icons">branding_watermark</i>
                    <span>Marcas</span>
                </a>
            </li>
            <li @if( Request::is('administration/purchases*'))
                class="active" @endif>
                <a href="{{ route('purchases.index') }}">
                    <i class="material-icons">money</i>
                    <span>Compras</span>
                </a>
            </li>
            <li @if( Request::is('administration/casual_clients*'))
            class="active" @endif>
            <a href="{{ route('casual_clients.index') }}">
                <i class="material-icons">airline_seat_recline_extra</i>
                <span>Cliente Casual</span>
            </a>
        </li>
            <li @if( Request::is('administration/sales*'))
                class="active" @endif>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">attach_money</i>
                    <span>Ventas</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ route('sales.index') }}">Lista</a>
                    </li>
                    <li>
                        <a href="{{ route('sales.create') }}">Nueva Venta</a>
                    </li>

                </ul>
            </li>
            <li class="{{ Request::is('administration/providers*') ? 'active' : '' }}">
                <a href="{{ route('providers.index') }}">
                    <i class="material-icons">airport_shuttle</i>
                    <span>Proveedores</span>
                </a>
            </li>
            <li class="{{ Request::is('administration/products*') ? 'active' : '' }}">
                <a href="{{ route('products.index') }}">
                    <i class="material-icons">local_mall</i>
                    <span>Productos</span>
                </a>
            </li>
            <li @if( Request::is('administration/departments*') )
                class="active" @endif>
                <a href="{{ route('departments.index') }}">
                    <i class="material-icons">add_location</i>
                    <span>Departamentos & Ciudades</span>
                </a>
            </li>
            <li class="{{ Request::is('administration/reports*') ? 'active' : '' }}">
                <a href="{{ route('reports.index') }}">
                    <i class="material-icons">announcement</i>
                    <span>Reportes</span>
                </a>
            </li>
            <li @if( Request::is('administration/users*') || Request::is('administration/roles*') || Request::is('administration/permissions*'))
                class="active" @endif>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">build</i>
                    <span>Configuracion</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ Request::is('administration/users*') ? 'active' : '' }}">
                        <a href="{{ route('users.index') }}">Usuarios</a>
                    </li>
                    <li class="{{ Request::is('administration/roles*') ? 'active' : '' }}">
                        <a href="{{ route('roles.index') }}">Roles</a>
                    </li>
                    <li class="{{ Request::is('administration/permissions*') ? 'active' : '' }}">
                        <a href="{{ route('permissions.index') }}">Permisos</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2018 <a href="javascript:void(0);">Massari</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.6
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->