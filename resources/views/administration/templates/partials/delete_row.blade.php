<script type="text/javascript">
    // $(document).ready(function () {
    $('.btn-delete').click(function (e) {
        console.log("Delete a row - This can be fun");
        e.preventDefault();
        var row = $(this).parents('tr');
        var id = row.data('id');
        swal({
                title: "Atención!",
                text: "Está a punto de borrar el registro, está seguro?.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar!",
                cancelButtonText: "No, cancelar!",
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                closeOnCancel: true
            },

            function (isConfirm) {
                if (isConfirm) {
                    var form = $('#form-delete');
                    var url = form.attr('action').replace(':ROW_ID', id);
                    var data = form.serialize();
                    var type = "";
                    var title = "";
                    $.post(url, data, function (result) {

                        console.log(result.message);
                        console.log(result.error);
                        if (result.error === false) {
                            row.fadeOut();
                            type = "success";
                            title = "Operación realizada!";
                        } else if (result.error === true) {
                            type = "error";
                            title = result.message;
                        }else{
                            type = "error";
                            title = "No se pudo realizar la operacion";
                        }
                        swal({title: title, text: result.message, type: type, confirmButtonText: "Aceptar"});
                    }).fail(function () {
                        swal('No se pudo realizar la petición.');
                    });
                }
            });
    });

    // });
</script>
