<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{'/assets/img/apple-icon.png'}}"/>
    <link rel="icon" type="image/png" href="{{ '/front/img/logo.png' }}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>BackEnd | {{ env('APP_NAME') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    @include('administration.templates.partials.css')
    @yield('css')
</head>

<body class="theme-blue">
<!-- Page Loader -->
{{--@include('templates.partials.loader')--}}
<!-- #END# Page Loader -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
@include('administration.templates.partials.navbar')
<section>
    @include('administration.templates.partials.sidebar')
</section>

<section class="content">
    <div class="container-fluid">
        @include('administration.templates.partials.alerts')
        <div class="block-header">
            <h2>@yield('title')</h2>
        </div>
        @yield('content')
    </div>

</section>
</body>
@include('administration.templates.partials.js')
@yield('js')