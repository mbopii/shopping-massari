<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('permission' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Permiso</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Descripción</label>
    </div>
</div>

{{--<input type="checkbox" id="remember_me_2" class="filled-in">--}}
{{--<label for="remember_me_2">Remember Me</label>--}}
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>