<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Nombre</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tax_name' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Razon Social</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tax_code' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Ruc</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('address' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Dirección</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('telephone' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Teléfono</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('fax_number' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Fax</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('email' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Email</label>
    </div>
</div>


<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>