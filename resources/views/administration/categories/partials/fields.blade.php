<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Categoria</label>
    </div>
</div>
@if (isset($category))
    @if (file_exists(public_path("/front/img/categories/{$category->id}.jpg")))
        <img src="{{ "/front/img/categories/{$category->id}.jpg?dummy=" . rand(0, 9999)  }}" alt="Categoria" width="200px" height="200px">
    @endif
@endif
<div class="form-group">
    <h2 class="card-inside-title">Imagen de Referencia</h2>
    <div class="form-line">
        {!! Form::file('category_image' , ['class' => 'form-control', 'id' => 'product_image']) !!}
    </div>
</div>
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>