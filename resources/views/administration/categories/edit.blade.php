@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Categoria
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'patch',
                    'files' => 'true']) !!}
                    @include('administration.categories.partials.fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

