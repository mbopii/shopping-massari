@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Marca
                    </h2>
                </div>
                <div class="body">
                    {!! Form::model($brand, ['route' => ['brands.update', $brand->id], 'method' => 'patch', 'files' => 'true']) !!}
                    @include('administration.brands.partials.fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

