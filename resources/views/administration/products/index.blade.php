@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Productos
                        <small>Filtros de Busqueda</small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <form action="{{ route('products.index') }}">

                            <div class="col-lg-4">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Descripcion</label>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-4">
                                <div class="form-line">
                                    <label id="role_id">Categoria</label>
                                    {!! Form::select('category_id' , $categories , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-line">
                                    <label id="role_id">Marcas</label>
                                    {!! Form::select('brand_id' , $brands, ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <input type="hidden" name="q" value="q">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                                <a href="{{ route('products.index') }}" class="btn btn-success">Ver Todos</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Productos
                        <small>Disponibles para la venta en la tienda</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('products.create') }}">Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th>Categoria</th>
                            <th>Marca</th>
                            <th>Foto</th>
                            <th>Creado</th>
                            <th>Modificado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $p)
                            <tr class="even pointer" data-id="{{ $p->id }}">
                                <th scope="row">{{ $p->id }}</th>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->categories->description }}</td>
                                <td>@if (isset($p->brand)){{ $p->brand->description }} @else -- @endif</td>
                                <td>@if (file_exists(public_path("admin/images/products/{$p->id}.jpg"))) <i class="material-icons">check_circle</i> @else <i class="material-icons">clear</i>@endif</td>
                                <td>{{ $p->created_at }}</td>
                                <td>{{ $p->updated_at }}</td>
                                <td>
                                    <div class="icon-button-demo">
                                        <a href="{{ route('products.edit', $p->id) }}">
                                            <button class="btn btn-info waves-effect" type="button">
                                                <i class="material-icons">border_color</i>
                                            </button>
                                        </a>
                                        <button class="btn btn-danger waves-effect btn-delete" type="button">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->appends(['category_id' => $category_id, 'q' => $q, 'description' => $description, 'brand_id' => $brand_id])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->

    {!! Form::open(['route' => ['products.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection


@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
@endsection

@section('js')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet" />
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection
