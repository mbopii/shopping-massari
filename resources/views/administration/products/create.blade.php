@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nuevo Producto
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    {!! Form::open(['route' => ['products.store'], 'method' => 'post', 'files' => 'true']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.products.partials.fields')
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_editor.min.css' rel='stylesheet'
          type='text/css'/>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_style.min.css' rel='stylesheet'
          type='text/css'/>

    <!-- Include JS file. -->
    <style>
        img {
            padding: 5px;
            border: 3px solid silver;
        }

        img:hover, img.selected {
            border: 3px solid green;
        }

        .img-wrap {
            position: relative;
            width: 240px;
            height: 250px;
            max-height: 310px;
            max-width: 290px;
        }

        .img-wrap .close {
            position: absolute;
            top: 2px;
            right: 2px;
            z-index: 100;
            color: red;
        }

        .close {
            font-size: 40px;
        }
    </style>
@endsection

@section('js')

    <script>
        $(".uploader").change(function () {
            var number = $(this).data("id");
            console.log("preview");
            var fileList = this.files;
            var anyWindow = window.URL || window.webkitURL;
            var imagePreview = $('#imagePreview_' + number);
            if ((imagePreview).children('.img-wrap').length) {
                $(imagePreview).children('div.img-wrap').remove();
            }
            for (var i = 0; i < fileList.length; i++) {
                console.log(fileList[i]);

                var objectUrl = anyWindow.createObjectURL(fileList[i]);
                $(imagePreview).append('<div class="col-md-4 img-wrap"><span class="close remove_image">&times;</span>' +
                    '<img class="image_preview" style="width: 250px; height: 250px" src="' + objectUrl + '" />' +
                    '</div>'
                );
                window.URL.revokeObjectURL(fileList[i]);
            }
        });


        $('.image_preview').on('click', 'img', function () {
            var images = $('.image_preview img').removeClass('selected');
            img = $(this).addClass('selected');
            $('#answer').val(images.index(img));
        });

        $(document).on('click', 'span.remove_image', function (e) {
            e.preventDefault();
            var data_id = $(this).parent('div.img-wrap').parent('div.image_preview').data('id');
            $(this).parent('div.img-wrap').remove();
            var imageInput = $("#product_image_" + data_id);
            imageInput.val('');
        });


    </script>
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>


    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5rg2hp0sb7xrx6usv2626gj82u7xgrkpeagib19qq3jhvmt3"></script>
    <script>
        $(function () {
            tinymce.init({
                selector: 'textarea',
                height: 500,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                ]
            });
        });
    </script>
    <script>
        $('#product_price').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    </script>
    <script>
        $('#add_complex').click(function () {
            var product_id = $('#select_product').val();
            var product_name = $("#select_product option:selected").text();
            var quantity = $('#select_quantity').val();
            console.log(quantity);
            if (!$('#select_quantity').val()) {
                alert('Debe asignar una cantidad al producto');
                return;
            }
            $("#complex_table tbody").append('' +
                '<tr id="' + product_id + '">' +
                '<td> ' + product_id + ' <input type="hidden" name=complex_product_id[] value="' + product_id + '"></td>' +
                '<td> ' + product_name + ' </td>' +
                '<td> ' + quantity + ' <input type="hidden" name=complex_quantity[] value="' + quantity + '"></td>' +
                '<td>' +
                '<button class="btn btn-danger waves-effect btn-delete-complex" type="button">' +
                '<i class="material-icons">delete_forever</i></button>' +
                '</td>' +
                '</tr> ');
            $('#select_product').val('');
            $("#select_product option:selected").text('');
            $('#select_quantity').val('');

            $('#complex_table tbody tr td').on('click', 'button.btn-delete-complex', function (e) {
                console.log("Delete complex product - Binding new event");
                e.preventDefault();
                var row = $(this).parents('tr');
                var id = row[0].id;
                console.log(id);
                swal({
                        title: "Atención!",
                        text: "Está a punto de borrar el registro, está seguro?.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        cancelButtonText: "No, cancelar!",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        closeOnCancel: true
                    },

                    function (isConfirm) {
                        if (isConfirm) {
                            row.fadeOut();
                        }
                    });
            });
            $('#productsModal').modal('toggle');
        });

    </script>
@endsection