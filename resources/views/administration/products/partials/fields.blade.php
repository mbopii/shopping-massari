<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('name' , null , ['class' => 'form-control', 'id' => 'permission', 'required']) !!}
        <label class="form-label">Nombre</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        <label>Descripción</label>
        {!! Form::textarea('description' , null , ['class' => 'form-control', 'id' => 'froala_editor', 'rows' => '20']) !!}
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('bar_code' , null , ['class' => 'form-control', 'id' => 'permission', 'required']) !!}
        <label class="form-label">Codigo de Barras</label>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('quantity' , null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                <label class="form-label">Stock Disponible</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('price' , null , ['class' => 'form-control', 'id' => 'product_price', 'required']) !!}
                <label class="form-label">Precio</label>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-line">
            <label id="brand_id">Marca</label>
            {!! Form::select('brand_id' , $brands , isset($product->brand_id) ? $product->brand_id : null, ['class' => 'form-control show-tick', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label id="brand_id">Categoría</label>
            {!! Form::select('category_id' , $categories, isset($product->category_id) ? $product->category_id : null, ['class' => 'form-control show-tick', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="active" name="active"
                       @if(isset($product) &&  $product->active) checked @endif/>
                <label for="active">Activo</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="featured" name="featured"
                       @if(isset($product) &&  $product->featured) checked @endif/>
                <label for="featured">Destacado</label>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="discount" name="discount"
                       @if(isset($product) && $product->discount) checked @endif/>
                <label for="discount">Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_percentage' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">% de Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_quantity' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Cantidad de Items</label>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-offset-4 col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_percentage2' , null , ['class' => 'form-control', 'id' => 'discount_percentage2']) !!}
                <label class="form-label">% de Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_quantity2' , null , ['class' => 'form-control', 'id' => 'discount_quantity2']) !!}
                <label class="form-label">Cantidad de Items</label>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="complex" name="complex"
                       @if(isset($product) && $product->complex_product) checked @endif/>
                <label for="complex">Producto Compuesto</label>
            </div>
        </div>
    </div>
    <div class="col-md-8">

        <table id="complex_table" class="table table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Cantidad</td>
                <td>Acciones</td>
            </tr>
            </thead>
            <tbody>
            @isset($product)
                @foreach($product->complexItems as $item)
                    <tr id="{{ $item->product->id }}">
                        <td>{{ $item->product->id }}<input type="hidden" name=complex_product_id[]
                                                           value="{{ $item->product->id }}"></td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->quantity }}<input type="hidden" name=complex_quantity[]
                                                        value="{{ $item->quantity }}"></td>
                        <td>
                            <button class="btn btn-danger waves-effect btn-delete-complex" type="button">
                                <i class="material-icons">delete_forever</i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <div class="form-group form-float">
            <button type="button" data-toggle="modal" data-target="#productsModal"
                    class="btn btn-primary m-t-15 waves-effect">Agregar Producto
            </button>
        </div>
        <br>
        <div id="complex_products">

        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('weight' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">Peso</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('height' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">Altura</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('width' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Ancho</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('length' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Largo</label>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <h2 class="card-inside-title">Imagen de Referencia - Seleccione una imagen para que sea la predeterminada en la
        tienda</h2>
    <input type="hidden" name="answer" id="answer"/>
    <div class="row">
        <div class="col-md-4">
            <div class="form-line">
                    {!! Form::file('product_image_1' , ['class' => 'form-control uploader', 'id' => 'product_image_1', 'accept' => 'image/x-png,image/gif,image/jpeg', 'data-id' => '1']) !!}
            </div>
            <div id="imagePreview_1" class="image_preview" data-id="1">
                @if (isset($product))
                    @if (file_exists(public_path("/front/img/products/{$product->id}/1.jpg")))
                        <div class="img-wrap">
                            <span class="close remove_image">&times;</span>
                            <img class="image_preview" style="width: 250px; height: 250px"
                                 src="{{ "/front/img/products/{$product->id}/1.jpg?dummy=" . rand(1, 9999) }}"/>
                        </div>
                    @endif
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-line">
                {!! Form::file('product_image_2' , ['class' => 'form-control uploader', 'id' => 'product_image_2', 'accept' => 'image/x-png,image/gif,image/jpeg', 'data-id' => '2']) !!}
            </div>
            <div id="imagePreview_2" class="image_preview" data-id="2">
                @if (isset($product))
                    @if (file_exists(public_path("/front/img/products/{$product->id}/2.jpg")))
                        <div class="img-wrap">
                            <span class="close remove_image">&times;</span>
                            <img class="image_preview" style="width: 250px; height: 250px"
                                 src="{{ "/front/img/products/{$product->id}/2.jpg?dummy=" . rand(1, 9999) }}"/>
                        </div>
                    @endif
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-line">
                {!! Form::file('product_image_3' , ['class' => 'form-control uploader', 'id' => 'product_image_3', 'accept' => 'image/x-png,image/gif,image/jpeg', 'data-id' => '3']) !!}
            </div>
            <div id="imagePreview_3" class="image_preview" data-id="3">
                @if (isset($product))
                    @if (file_exists(public_path("/front/img/products/{$product->id}/3.jpg")))
                        <div class="img-wrap">
                            <span class="close remove_image">&times;</span>
                            <img class="image_preview" style="width: 250px; height: 250px"
                                 src="{{ "/front/img/products/{$product->id}/3.jpg?dummy=" . rand(1, 9999) }}"/>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>


<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
<div class="modal fade" id="productsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar Producto</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Elija un producto</b>
                        </p>
                        <div class="row">
                            <select id="select_product" name="select_product" class="form-control show-tick"
                                    data-live-search="true">
                                <option value="0">Seleccione</option>
                                @foreach($productsJson as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-line">
                                <div class="form-group">
                                    {!! Form::text('select_quantity' , null , ['class' => 'form-control', 'id' => 'select_quantity']) !!}
                                    <label class="form-label">Cantidad</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="add_complex" class="btn btn-link waves-effect">Agregar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>